#include <mav_path_trajectory/trajectory_tf_transform.h>

TrajectoryTransformer::TrajectoryTransformer()
{
  nhPrivate = ros::NodeHandle("~");
  transformed_trajectory = nhPrivate.advertise<trajectory_msgs::MultiDOFJointTrajectory>("/euroc3/command/trajectory_transformed", 10);
  transformedPosePub = nhPrivate.advertise<geometry_msgs::PoseStamped>("/euroc3/command/pose_transformed", 10);
  //subscribers
  trajectorySub = nhPrivate.subscribe("/euroc3/command/trajectory", 1000, &TrajectoryTransformer::trajectoryCallback,this);
  transformSub = nhPrivate.subscribe("/euroc3/soft/vicon_to_odom0", 1000, &TrajectoryTransformer::transformCallback,this);
  poseSub = nhPrivate.subscribe("/euroc3/command/pose", 1000, &TrajectoryTransformer::poseCallback, this);
  //odom2viconYaw = 0;
}

void TrajectoryTransformer::transformCallback(const geometry_msgs::TransformStamped::ConstPtr& msg)
{
    tf::Transform vicon2odom;
    tf::Vector3 msgOrigin(msg->transform.translation.x,msg->transform.translation.y,msg->transform.translation.z);
    vicon2odom.setOrigin(msgOrigin);
    tf::Quaternion msgRotation(msg->transform.rotation.x,msg->transform.rotation.y,msg->transform.rotation.z,msg->transform.rotation.w);
    vicon2odom.setRotation(msgRotation);
    transform = vicon2odom.inverse();//We want odom2vicon which is now contained in transform
    //Now we want to take only yaw axis of the coordinate systems since we assume odom is aligned with g
    //double roll,pitch,yaw;
    odom2viconYaw = transform.getRotation();
    //tf::Matrix3x3(msgRotation).getRPY(roll, pitch, yaw);
    //odom2viconYaw = tf::createQuaternionFromRPY(0.0,0.0,yaw);
    //ROS_INFO("Angle is %f",yaw);
    transform_received = true;
}

void TrajectoryTransformer::trajectoryCallback(const trajectory_msgs::MultiDOFJointTrajectory::ConstPtr& msg)
{
  if(transform_received)
  {

      //ROS_INFO("Received trajectory, transforming it to odom");
      trajectory_msgs::MultiDOFJointTrajectory transformedTrajectory;
      trajectory_msgs::MultiDOFJointTrajectoryPoint transformedTrajectoryPoint;
      geometry_msgs::Transform tempTransform;
      geometry_msgs::Twist tempTwist;
      transformedTrajectoryPoint.transforms.push_back(tempTransform);
      transformedTrajectoryPoint.velocities.push_back(tempTwist);
      transformedTrajectoryPoint.accelerations.push_back(tempTwist);
      
      //ROS_INFO("printing %d ", msg->points.size());
      for(int i=0; i<msg->points.size(); i++)
      {
      	
      	//Here we transform poses
      	tf::Vector3 _translation(msg->points[i].transforms[0].translation.x,msg->points[i].transforms[0].translation.y,msg->points[i].transforms[0].translation.z);
      	tf::Vector3 _transformed_translation = transform(_translation);

      	transformedTrajectoryPoint.transforms[0].translation.x = _transformed_translation.getX();//+transform.getOrigin().getX();
      	transformedTrajectoryPoint.transforms[0].translation.y = _transformed_translation.getY();//+transform.getOrigin().getY();
      	transformedTrajectoryPoint.transforms[0].translation.z = _transformed_translation.getZ();//+transform.getOrigin().getZ();
      	//transformedTrajectoryPoint.transforms[0].rotation = msg->points[i].transforms[0].rotation;
      	//transformedTrajectoryPoint.velocities[0].linear = msg->points[i].velocities[0].linear;
      	tf::Quaternion _rotation(msg->points[i].transforms[0].rotation.x,msg->points[i].transforms[0].rotation.y,msg->points[i].transforms[0].rotation.z,msg->points[i].transforms[0].rotation.w);
	tf::Quaternion _transformed_rotation = _rotation * odom2viconYaw;
	tf:: quaternionTFToMsg(_transformed_rotation,transformedTrajectoryPoint.transforms[0].rotation);
      	//transformedTrajectoryPoint.transforms[0].rotation.x = _transformed_rotation.getX();
	//transformedTrajectoryPoint.transforms[0].rotation.y = _transformed_rotation.getY();
	//transformedTrajectoryPoint.transforms[0].rotation.z = _transformed_rotation.getZ();
	//transformedTrajectoryPoint.transforms[0].rotation.w = _transformed_rotation.getW();
	
	
      	tf::Vector3 zeroOrigin(0,0,0);
      	//zeroOrigin.x()= 0;zeroOrigin.y = 0;zeroOrigin.z = 0; 
      	double x = transform.getOrigin().getX();
      	double y = transform.getOrigin().getY();
      	double z = transform.getOrigin().getZ();
      	transform.setOrigin(zeroOrigin);
      	tf::Vector3 _velocity(msg->points[i].velocities[0].linear.x,msg->points[i].velocities[0].linear.y,msg->points[i].velocities[0].linear.z);
      	tf::Vector3 _transformed_velocity = transform(_velocity);
      	tf::vector3TFToMsg(_transformed_velocity,transformedTrajectoryPoint.velocities[0].linear);
      	
      	transformedTrajectoryPoint.velocities[0].angular = msg->points[i].velocities[0].angular;
      	//transformedTrajectoryPoint.accelerations[0].linear = msg->points[i].accelerations[0].linear;
      	tf::Vector3 _acceleration(msg->points[i].accelerations[0].linear.x,msg->points[i].accelerations[0].linear.y,msg->points[i].accelerations[0].linear.z);
      	tf::Vector3 _transformed_acceleration = transform(_acceleration);
      	tf::vector3TFToMsg(_transformed_acceleration,transformedTrajectoryPoint.accelerations[0].linear);
      	transformedTrajectoryPoint.accelerations[0].angular = msg->points[i].accelerations[0].angular;
      	transformedTrajectory.points.push_back(transformedTrajectoryPoint);
      	tf::Vector3 returnOrigin(x,y,z);
      	transform.setOrigin(returnOrigin);
      }
      transformed_trajectory.publish(transformedTrajectory);
      //ROS_INFO("I am done, I'm publishing");
  }
  else 
  {
    ROS_INFO("NO TF PUBLISHED, ASSUMING FRAMES ARE ALIGNED");
    //transformed_trajectory.publish(msg);
  }
}

void TrajectoryTransformer::poseCallback(const geometry_msgs::PoseStamped msg)
{
  if(transform_received)
  {
    tf::Vector3 _translation(msg.pose.position.x,msg.pose.position.y,msg.pose.position.z);
    tf::Vector3 _transformed_translation = transform(_translation);
    //transform.setOrigin(zeroOrigin);
    //tf::Vector3 returnOrigin(x,y,z);
    //transform.setOrigin(returnOrigin);
    // Dodati publishera i provjeriti @Matko
    geometry_msgs::PoseStamped transformedPoseStamped = msg;
    transformedPoseStamped.pose.position.x = _transformed_translation.getX();
    transformedPoseStamped.pose.position.y = _transformed_translation.getY(); 
    transformedPoseStamped.pose.position.z = _transformed_translation.getZ();
    //tf::poseStampedTFToMsg(_transformed_translation,transformedPoseStamped);
 
    transformedPosePub.publish(transformedPoseStamped);
  }
  else
  {
    ROS_INFO("NO TF PUBLISHED, ASSUMING FRAMES ARE ALIGNED");
  }
}

void TrajectoryTransformer::run()
{
  ros::Rate rate(10.0);
  while(ros::ok())
  {
    ros::spinOnce();
    /*if(!transform_received)
    {
      try{
	tf_listener.lookupTransform("/vicon", "/odom",ros::Time(0), transform);
	if(!transform_received) 
	{
	  ROS_INFO("RECEIVED FIRST TF");
	  transform_received = true;
	}
	
      }
      catch (tf::TransformException &ex) {
	ROS_ERROR("%s",ex.what());
	ros::Duration(1.0).sleep();
	continue;
      }
    }*/
      rate.sleep();
  }
}

int main(int argc, char** argv){
  ros::init(argc, argv, "trajectory_tf_transformer");
  TrajectoryTransformer traj_transform;
  traj_transform.run();
  return 0;
  
};