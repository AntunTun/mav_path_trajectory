/******************************************************************************
File name: trajectoryPlanning.cpp
Description: Functions for planning a trajectory for arducopter
Author: Marko Car, Antun Ivanovic
******************************************************************************/

#include "mav_path_trajectory/trajectoryPlanning.h"

trajectoryPlanning::trajectoryPlanning()
{
	// Initialize ros node handle for move group, it is a private node handle
	nhParams = ros::NodeHandle("~");
	nhParams.param("max_speed", maxSpeed, double(4.0));
	nhParams.param("max_acceleration", maxAcc, double(1.1));
	nhParams.param("max_yaw_speed", maxYawSpeed, double(0.3));
	nhParams.param("max_yaw_acc", maxYawAcc, double(1.0));
	nhParams.param("trajectory_sampling_frequency", trajectorySamplingFrequency,
		int(20));

	// Publishing sampled trajectory to trajectory_unprocessed topic. Later the
	// mission planner decides whether to use acceleration or not. Acceleration
	// is provided to mission planner anyways
	trajectorySampledPub = nhTopics.advertise<mav_path_trajectory::TrajectorySampled>(
		"trajectory_unprocessed", 1);
	// Waypoints are recieved through callback
	waypointSub = nhTopics.subscribe("waypoints", 1,
		&trajectoryPlanning::waypointCallback, this);
}

trajectoryPlanning::~trajectoryPlanning()
{

}

void trajectoryPlanning::run()
{
	ros::spin();
}

mav_path_trajectory::TrajectorySampled trajectoryPlanning::plan(mav_path_trajectory::WaypointArray wp)
{
	return hoCook56(wp);
}

void trajectoryPlanning::setTrajectorySamplingFrequency(int freq)
{
	trajectorySamplingFrequency = freq;
}

void trajectoryPlanning::setArducopterMaxSpeed(double speed)
{
	maxSpeed = speed;
}

void trajectoryPlanning::setArducopterMaxAcc(double acc)
{
	maxAcc = acc;
}

void trajectoryPlanning::getSplineCoefficientsThroughTwoPoints(
	double X0, double Xf, double V0, double Vf, double A0, double Af, 
	double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on fifth order polynomial:
	// x(t) = a5*t^5 + a4*t^4 + a3*t^3 + a2*t^2 + a1*t + a0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;

	// Calculating coefficients based on equations computed earlier
	coefficients[0] = X0;
	coefficients[1] = V0;
	coefficients[2] = A0/2;
	coefficients[3] = -(20*X0 - 20*Xf + 12*Tf*V0 + 8*Tf*Vf + 3*A0*Tf2 - Af*Tf2)
		/(2*Tf3);
	coefficients[4] = (30*X0 - 30*Xf + 16*Tf*V0 + 14*Tf*Vf + 3*A0*Tf2 - 2*Af*Tf2)
		/(2*Tf4);
	coefficients[5] = -(12*X0 - 12*Xf + 6*Tf*V0 + 6*Tf*Vf + A0*Tf2 - Af*Tf2)
		/(2*Tf5);
}

void trajectoryPlanning::sampleTrajectoryBetweenTwoPoints(
	std::vector<double> coefficientsX, std::vector<double> coefficientsY,
	std::vector<double> coefficientsZ, std::vector<double> coefficientsYaw,
	mav_path_trajectory::TrajectorySampled &trajectory, double trajectoryDuration, 
	int sampleFrequency)
{
	// First let's determine sample time from given sample frequency
	double sampleTime = 1.0/double(sampleFrequency);

	// currentDuration is time that has elapsed from beginning of trajectory
	double currentDuration = 0.0;

	// Temporary Vector3 variables for position, speed and acceleration that
	// will be stored in sampled trajectory
	geometry_msgs::Vector3 tempPosition;
	geometry_msgs::Vector3 tempSpeed;
	geometry_msgs::Vector3 tempAcceleration;
	std_msgs::Float64 tempYawPos;
	std_msgs::Float64 tempYawSpeed;
	std_msgs::Float64 tempYawAcc;

	// Given duration of trajectory must be greater than zero because otherwise
	// it makes no sense
	if (trajectoryDuration > 0)
	{	
		while(currentDuration <= trajectoryDuration)
		{
			// Here quadcopter is in trajectory execution and trajectory is
			// sampled. Position, speed and acceleration are calculated and 
			// pushed on trajectory vector. It is important to ensure that 
			// trajectory vector is empty before it's entering this function

			// Calculation for X-axis
			calculatePolynomialValueOrder5(coefficientsX, currentDuration, 
				tempPosition.x, tempSpeed.x, tempAcceleration.x);
			tempAcceleration.x = tempAcceleration.x;

			// Calculation for Y-axis
			calculatePolynomialValueOrder5(coefficientsY, currentDuration, 
				tempPosition.y, tempSpeed.y, tempAcceleration.y);
			tempAcceleration.y = tempAcceleration.y;

			// Calculation for Z-axis
			calculatePolynomialValueOrder5(coefficientsZ, currentDuration, 
				tempPosition.z, tempSpeed.z, tempAcceleration.z);
			tempAcceleration.z = tempAcceleration.z;

			// Calculation for yaw
			calculatePolynomialValueOrder5(coefficientsYaw, currentDuration, 
				tempYawPos.data, tempYawSpeed.data, tempYawAcc.data);

			// Increase currentDuration and push vectors on trajectory
			currentDuration = currentDuration + sampleTime;
			trajectory.position.push_back(tempPosition);
			trajectory.speed.push_back(tempSpeed);
			trajectory.acceleration.push_back(tempAcceleration);
			trajectory.yawPos.push_back(tempYawPos.data);
			trajectory.yawSpeed.push_back(tempYawSpeed.data);
			trajectory.yawAcc.push_back(tempYawAcc.data);
		}

		// This part will never execute. The idea is to push the last point of
		// trajectory to the end but this can produce duplicates. If we sample
		// larger trajectory through multiple points it is not recomended to 
		// have duplicates due to continuity.
		// This makes sense only on the last spline of the trajectory to ensure
		// quadcopter stops at right point. At this point it is not taken into 
		// calculation.
		if((currentDuration != trajectoryDuration) && false)
		{
			// At the end currentDuration should be the same as
			// trajectoryDuration. However since both variables are double
			// it is not always this way. That's why we have to check if they
			// are the same, if they are not that means currentDuration is
			// bigger than trajectoryDuration and one more point is pushed
			// on the end of the vector

			// Calculation for X-axis
			calculatePolynomialValueOrder5(coefficientsX, trajectoryDuration, 
				tempPosition.x, tempSpeed.x, tempAcceleration.x);
			tempAcceleration.x = tempAcceleration.x;

			// Calculation for Y-axis
			calculatePolynomialValueOrder5(coefficientsY, trajectoryDuration, 
				tempPosition.y, tempSpeed.y, tempAcceleration.y);
			tempAcceleration.y = tempAcceleration.y;

			// Calculation for Z-axis
			calculatePolynomialValueOrder5(coefficientsZ, trajectoryDuration, 
				tempPosition.z, tempSpeed.z, tempAcceleration.z);
			tempAcceleration.z = tempAcceleration.z;

			// Calculation for yaw
			calculatePolynomialValueOrder5(coefficientsYaw, trajectoryDuration, 
				tempYawPos.data, tempYawSpeed.data, tempYawAcc.data);
			
			// Push on trajectory
			trajectory.position.push_back(tempPosition);
			trajectory.speed.push_back(tempSpeed);
			trajectory.acceleration.push_back(tempAcceleration);
			trajectory.yawPos.push_back(tempYawPos.data);
			trajectory.yawSpeed.push_back(tempYawSpeed.data);
			trajectory.yawAcc.push_back(tempYawAcc.data);
		}

	}
}

double trajectoryPlanning::calculatePositionPolynomialValue(
	std::vector<double> a, double t)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double polynomialValue = 0.0;
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;
	double t5 = t4*t;

	// Calculate position based on time and coefficients given as arguments
	polynomialValue = a[5]*t5 + a[4]*t4 + a[3]*t3 + a[2]*t2 + a[1]*t1 + a[0];

	return polynomialValue;
}

double trajectoryPlanning::calculateSpeedPolynomialValue(
	std::vector<double> a, double t)
{
	double polynomialValue = 0.0;
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;

	// Speed is derivative of position so a[0] isn't here
	polynomialValue = 5*a[5]*t4 + 4*a[4]*t3 + 3*a[3]*t2 + 2*a[2]*t1 + a[1];

	return polynomialValue;
}

double trajectoryPlanning::calculateAccelerationPolynomialValue(
	std::vector<double> a, double t)
{
	double polynomialValue = 0.0;
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	
	// Acceleration is second derivative of position so a[0] and a[1] are
	// not here
	polynomialValue = 20*a[5]*t3 + 12*a[4]*t2 + 6*a[3]*t1 + 2*a[2];

	return polynomialValue;
}

void trajectoryPlanning::calculatePolynomialValueOrder6(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;
	double t5 = t4*t;
	double t6 = t5*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[6]*t6 + B[5]*t5 + B[4]*t4 + B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 6*B[6]*t5 + 5*B[5]*t4 + 4*B[4]*t3 + 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 30*B[6]*t4 + 20*B[5]*t3 + 12*B[4]*t2 + 6*B[3]*t1 + 2*B[2];
}

void trajectoryPlanning::calculatePolynomialValueOrder5(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;
	double t5 = t4*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[5]*t5 + B[4]*t4 + B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 5*B[5]*t4 + 4*B[4]*t3 + 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 20*B[5]*t3 + 12*B[4]*t2 + 6*B[3]*t1 + 2*B[2];
}

void trajectoryPlanning::calculatePolynomialValueOrder4(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[4]*t4 + B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 4*B[4]*t3 + 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 12*B[4]*t2 + 6*B[3]*t1 + 2*B[2];
}

void trajectoryPlanning::calculatePolynomialValueOrder3(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 6*B[3]*t1 + 2*B[2];
}

double trajectoryPlanning::getMaxTrajectorySpeed(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectorySpeed = -1.0;
	double currentSpeed = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.speed.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectorySpeed = sqrt(pow(trajectory.speed[0].x, 2) + 
			pow(trajectory.speed[0].y, 2) + pow(trajectory.speed[0].z, 2));
		currentSpeed = maxTrajectorySpeed;

		// Looping through trajectory in search for max
		for(int i=1; i<trajectory.speed.size(); i++)
		{	
			// Calculation for current speed in trajectory
			currentSpeed = sqrt(pow(trajectory.speed[i].x, 2) + 
				pow(trajectory.speed[i].y, 2) + pow(trajectory.speed[i].z, 2));

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectorySpeed < currentSpeed)
			{
				maxTrajectorySpeed = currentSpeed;
				index = i;
			}
		}
	}
	// Returning the max value
	return maxTrajectorySpeed;
}

double trajectoryPlanning::getMaxTrajectoryYawSpeed(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectoryYawSpeed = -1.0;
	double currentSpeed = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.yawSpeed.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectoryYawSpeed = abs(trajectory.yawSpeed[0]);
		currentSpeed = maxTrajectoryYawSpeed;

		// Looping through trajectory in search for max
		for(int i=1; i<trajectory.yawSpeed.size(); i++)
		{	
			// Calculation for current speed in trajectory
			currentSpeed = abs(trajectory.yawSpeed[i]);

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectoryYawSpeed < currentSpeed)
			{
				maxTrajectoryYawSpeed = currentSpeed;
				index = i;
			}
		}
	}
	// Returning the max value
	return maxTrajectoryYawSpeed;
}

double trajectoryPlanning::getMaxTrajectoryYawAcc(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectoryYawAcc = -1.0;
	double currentAcc = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.yawAcc.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectoryYawAcc = abs(trajectory.yawAcc[0]);
		currentAcc = maxTrajectoryYawAcc;

		// Looping through trajectory in search for max
		for(int i=1; i<trajectory.yawAcc.size(); i++)
		{	
			// Calculation for current speed in trajectory
			currentAcc = abs(trajectory.yawAcc[i]);

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectoryYawAcc < currentAcc)
			{
				maxTrajectoryYawAcc = currentAcc;
				index = i;
			}
		}
	}
	// Returning the max value
	return maxTrajectoryYawAcc;
}

double trajectoryPlanning::getMaxTrajectoryAcceleration(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// As in speed function here is the same error code, if maxAcc and index
	// are -1 the trajectory is empty
	double maxTrajectoryAcc = -1.0;
	double currentAcc = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max acceleration
	if (trajectory.acceleration.size() > 0)
	{
		// First point of trajectory defines initial value of max acceleration. 
		// At this point current acceleration is the same.
		// Acceleration is calculated as squared root of x, y and z components
		// squared. This is the total acceleration of quadcopter.
		maxTrajectoryAcc = sqrt(pow(trajectory.acceleration[0].x, 2) + 
			pow(trajectory.acceleration[0].y, 2) + 
			pow(trajectory.acceleration[0].z, 2));
		currentAcc = maxTrajectoryAcc;

		// Looping through trajectory in search for max acceleration
		for(int i=1; i<trajectory.acceleration.size(); i++)
		{
			// Calculation for current acceleration in trajectory
			currentAcc = sqrt(pow(trajectory.acceleration[i].x, 2) + 
				pow(trajectory.acceleration[i].y, 2) + 
				pow(trajectory.acceleration[i].z, 2));
			
			// If currentAcc becomes greater than maxTrajectoryAcc it becomes
			// maxTrajectoryAcc
			if(maxTrajectoryAcc < currentAcc)
			{
				maxTrajectoryAcc = currentAcc;
				index = i;
			}
		}
	}

	// Returning maxTrajectoryAcc
	return maxTrajectoryAcc;
}

void trajectoryPlanning::clearTrajectorySampled(
	mav_path_trajectory::TrajectorySampled &trajectory)
{
	trajectory.position.clear();
	trajectory.speed.clear();
	trajectory.acceleration.clear();
	trajectory.yawPos.clear();
	trajectory.yawSpeed.clear();
	trajectory.yawAcc.clear();
	trajectory.segment.clear();
}

double trajectoryPlanning::calculateTheoreticalMaxSpeed(
	double V0, double trajectoryLength, double maxAcceleration)
{
	// Theoretical max speed will be achieved at the middle of path
	// because on the second half of path quadcopter needs to decelerate

	return abs(V0) + abs(maxAcceleration*sqrt(1.0*trajectoryLength/maxAcceleration));
}

void trajectoryPlanning::getSplineCoefficientsOrder3(
	double X0, double Xf, double V0, double Vf, double Tf, 
	std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a3*t^3 + a2*t^2 + a1*t + a0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 4)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = -(3.0*X0 - 3.0*Xf + 2.0*Tf*V0 + Tf*Vf)/Tf2;
		coefficients[3] = (2*X0 - 2*Xf + Tf*V0 + Tf*Vf)/Tf3;
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder4AccStart(
	double X0, double Xf, double V0, double Vf, double A0, double Tf, 
	std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a3*t^3 + a2*t^2 + a1*t + a0
	// Acceleration to compute coefficients is given at the start x''(0)=A0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 5)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = -(4*X0 - 4*Xf + 3*Tf*V0 + Tf*Vf + A0*Tf2)/Tf3;
		coefficients[4] = (6*X0 - 6*Xf + 4*Tf*V0 + 2*Tf*Vf + A0*Tf2)/(2*Tf4);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder4AccEnd(
	double X0, double Xf, double V0, double Vf, double Af, double Tf, 
	std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a3*t^3 + a2*t^2 + a1*t + a0
	// Acceleration to compute coefficients is given at the start x''(Tf)=A0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 5)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = -(12*X0 - 12*Xf + 6*Tf*V0 + 6*Tf*Vf - Af*Tf2)/(2*Tf2);
		coefficients[3] = (8*X0 - 8*Xf + 3*Tf*V0 + 5*Tf*Vf - Af*Tf2)/Tf3;
		coefficients[4] = -(6*X0 - 6*Xf + 2*Tf*V0 + 4*Tf*Vf - Af*Tf2)/(2*Tf4);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder6JerkStart(
	double X0, double Xf, double V0, double Vf, double A0, double Af, 
	double J0, double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a0 + a1t + a2t^2 + a3t^3 + a4t^4 + a5t^5 + a6t^6
	// Acceleration to compute coefficients is given at the start x''(0)=A0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5, Tf6;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;
	Tf6 = Tf5*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 7)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = J0/6;
		coefficients[4] = -(30*X0 - 30*Xf + 20*V0*Tf + 10*Vf*Tf + 6*A0*Tf2 
			- Af*Tf2 + J0*Tf3)/(2*Tf4);
		coefficients[5] = (48*X0 - 48*Xf + 30*V0*Tf + 18*Vf*Tf + 
			8*A0*Tf2 - 2*Af*Tf2 + J0*Tf3)/(2*Tf5);
		coefficients[6] = -(60*X0 - 60*Xf + 36*V0*Tf + 24*Vf*Tf + 
			9*A0*Tf2 - 3*Af*Tf2 + J0*Tf3)/(6*Tf6);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder6JerkEnd(
	double X0, double Xf, double V0, double Vf, double A0, double Af, 
	double Jf, double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a0 + a1t + a2t^2 + a3t^3 + a4t^4 + a5t^5 + a6t^6
	// Acceleration to compute coefficients is given at the start x''(0)=A0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5, Tf6;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;
	Tf6 = Tf5*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 7)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = -(120*X0 - 120*Xf + 60*V0*Tf + 60*Vf*Tf + 
			12*A0*Tf2 - 12*Af*Tf2 + Jf*Tf3)/(6*Tf3);
		coefficients[4] = (90*X0 - 90*Xf + 40*V0*Tf + 50*Vf*Tf + 
			6*A0*Tf2 - 11*Af*Tf2 + Jf*Tf3)/(2*Tf4);
		coefficients[5] = -(72*X0 - 72*Xf + 30*V0*Tf + 42*Vf*Tf + 
			4*A0*Tf2 - 10*Af*Tf2 + Jf*Tf3)/(2*Tf5);
		coefficients[6] = (60*X0 - 60*Xf + 24*V0*Tf + 36*Vf*Tf + 
			3*A0*Tf2 - 9*Af*Tf2 + Jf*Tf3)/(6*Tf6);
	}
}


void trajectoryPlanning::sampleMultipleWaypointTrajectory(
	std::vector< std::vector<double> > coefficientsX, 
	std::vector< std::vector<double> > coefficientsY, 
	std::vector< std::vector<double> > coefficientsZ, 
	std::vector< std::vector<double> > coefficientsYAW, std::vector<double> t, 
	mav_path_trajectory::TrajectorySampled &trajectory, int sampleFrequency, 
	std::vector<int> &endIndexes)
{
	endIndexes.clear();
	geometry_msgs::Vector3 posVec, speedVec, accVec;
	double pos=0.0, speed=0.0, acc=0.0;
	double yawPos=0.0, yawSpeed=0.0, yawAcc=0.0;

	// First let's determine sample time from given sample frequency
	double sampleTime = 1.0/double(sampleFrequency);

	for(int i=0; i<t.size(); i++)
	{
		// Okay, we go through all the polynoms and sample the trajectory
		double currentTime = 0.0;

		// Sampling one polynom at the tame and pushing it in trajectory
		while(currentTime <= t[i]-sampleTime*0.5)
		{
			// X axis
			pos=0.0; speed=0.0; acc=0.0;
			if (coefficientsX[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsX[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsX[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsX[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsX[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsX[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsX[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsX[i], currentTime, 
					pos, speed, acc);
			}
			// setting position, speed and acc in vector3
			posVec.x = pos;
			speedVec.x = speed;
			accVec.x = acc;


			// Y axis
			pos=0.0; speed=0.0; acc=0.0;
			if (coefficientsY[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsY[i], currentTime, 
					pos, speed, acc);
			}
			if(coefficientsY[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsY[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsY[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsY[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsY[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsY[i], currentTime, 
					pos, speed, acc);
			}
			// setting position, speed and acc in vector3
			posVec.y = pos;
			speedVec.y = speed;
			accVec.y = acc;


			// Z axis
			pos=0.0; speed=0.0; acc=0.0;
			if (coefficientsZ[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsZ[i], currentTime, 
					pos, speed, acc);
			}
			if(coefficientsZ[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsZ[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsZ[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsZ[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsZ[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsZ[i], currentTime, 
					pos, speed, acc);
			}
			// setting position, speed and acc in vector3
			posVec.z = pos;
			speedVec.z = speed;
			accVec.z = acc;

			// YAW axis
			yawPos=0.0; yawSpeed=0.0; yawAcc=0.0;
			if (coefficientsYAW[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc);
			}
			if(coefficientsYAW[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc);
			}
			else if(coefficientsYAW[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc);
			}
			else if(coefficientsYAW[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc);
			}
			// setting position, speed and acc in vector3
			//posVec.z = pos;
			//speedVec.z = speed;
			//accVec.z = acc;

			// Now we can push trajectory points into sampled trajectory
			trajectory.position.push_back(posVec);
			trajectory.speed.push_back(speedVec);
			trajectory.acceleration.push_back(accVec);
			trajectory.yawPos.push_back(yawPos);
			trajectory.yawSpeed.push_back(yawSpeed);
			trajectory.yawAcc.push_back(yawAcc);

			currentTime = currentTime + sampleTime;
		}
		endIndexes.push_back(trajectory.position.size());
	}

	// Last point
	pos=0.0; speed=0.0; acc=0.0;
	if(coefficientsX[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsX[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsX[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsX[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	posVec.x = pos;
	speedVec.x = speed;
	accVec.x = acc;

	pos=0.0; speed=0.0; acc=0.0;
	if(coefficientsY[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsY[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsY[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsY[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	posVec.y = pos;
	speedVec.y = speed;
	accVec.y = acc;

	pos=0.0; speed=0.0; acc=0.0;
	if(coefficientsZ[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsZ[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsZ[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsZ[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	posVec.z = pos;
	speedVec.z = speed;
	accVec.z = acc;

	// YAW
	yawPos=0.0; yawSpeed=0.0; yawAcc=0.0;
	if(coefficientsYAW[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc);
	}
	else if(coefficientsYAW[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc);
	}
	else if(coefficientsYAW[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc);
	}
	else if(coefficientsYAW[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc);
	}

	// Now we can push trajectory points into sampled trajectory
	trajectory.position.push_back(posVec);
	trajectory.speed.push_back(speedVec);
	trajectory.acceleration.push_back(accVec);
	trajectory.yawPos.push_back(yawPos);
	trajectory.yawSpeed.push_back(yawSpeed);
	trajectory.yawAcc.push_back(yawAcc);
}

double trajectoryPlanning::getMaxTrajectorySpeedInterval(
	mav_path_trajectory::TrajectorySampled trajectory, int start, int end)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectorySpeed = -1.0;
	double currentSpeed = 0;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.speed.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectorySpeed = sqrt(pow(trajectory.speed[start].x, 2) + 
			pow(trajectory.speed[start].y, 2) + pow(trajectory.speed[start].z, 2));
		currentSpeed = maxTrajectorySpeed;

		// Looping through trajectory in search for max
		for(int i=start; i<=end; i++)
		{	
			// Calculation for current speed in trajectory
			currentSpeed = sqrt(pow(trajectory.speed[i].x, 2) + 
				pow(trajectory.speed[i].y, 2) + pow(trajectory.speed[i].z, 2));

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectorySpeed < currentSpeed)
			{
				maxTrajectorySpeed = currentSpeed;
			}
		}
	}
	// Returning the max value
	return maxTrajectorySpeed;
}

double trajectoryPlanning::getMaxTrajectoryAccelerationInterval(
	mav_path_trajectory::TrajectorySampled trajectory, int start, int end)
{
	// As in speed function here is the same error code, if maxAcc and index
	// are -1 the trajectory is empty
	double maxTrajectoryAcc = -1.0;
	double currentAcc = 0;

	// If trajectory has any elements we can loop through them and look for 
	// max acceleration
	if (trajectory.acceleration.size() > 0)
	{
		// First point of trajectory defines initial value of max acceleration. 
		// At this point current acceleration is the same.
		// Acceleration is calculated as squared root of x, y and z components
		// squared. This is the total acceleration of quadcopter.
		maxTrajectoryAcc = sqrt(pow(trajectory.acceleration[start].x, 2) + 
			pow(trajectory.acceleration[start].y, 2) + 
			pow(trajectory.acceleration[start].z, 2));
		currentAcc = maxTrajectoryAcc;

		// Looping through trajectory in search for max acceleration
		for(int i=start; i<=end; i++)
		{
			// Calculation for current acceleration in trajectory
			currentAcc = sqrt(pow(trajectory.acceleration[i].x, 2) + 
				pow(trajectory.acceleration[i].y, 2) + 
				pow(trajectory.acceleration[i].z, 2));
			
			// If currentAcc becomes greater than maxTrajectoryAcc it becomes
			// maxTrajectoryAcc
			if(maxTrajectoryAcc < currentAcc)
			{
				maxTrajectoryAcc = currentAcc;
			}
		}
	}

	// Returning maxTrajectoryAcc
	return maxTrajectoryAcc;
}


void trajectoryPlanning::twoWaypointsTrajectoryOrder3(
	mav_path_trajectory::WaypointArray waypointArray)
{
	// First check if there is at least two waypoints in waypointArray
	//cout << "tu sam" << endl;
	if(waypointArray.waypoints.size() >= 2)
	{
		// We can work if there are two waypoints. In case of multiple waypoints
		// only first two will be considered

		// For easier notation use extra variables
		double xPosStart, xPosEnd, yPosStart, yPosEnd, zPosStart, zPosEnd;
		double yawPosStart, yawPosEnd;

		// Assigning start and end values
		xPosStart = waypointArray.waypoints[0].position.x;
		yPosStart = waypointArray.waypoints[0].position.y;
		zPosStart = waypointArray.waypoints[0].position.z;
		yawPosStart = waypointArray.waypoints[0].orientation.z;

		xPosEnd = waypointArray.waypoints[1].position.x;
		yPosEnd = waypointArray.waypoints[1].position.y;
		zPosEnd = waypointArray.waypoints[1].position.z;
		yawPosEnd = waypointArray.waypoints[1].orientation.z;

		// Estimate time needed to complete trajectory using euclidian distance
		// that quadcopter needs to travel and max speed provided
		double tempDistance, trajectoryTime;
		tempDistance = sqrt(pow(xPosEnd - xPosStart, 2) + 
			pow(yPosEnd - yPosStart, 2) + pow(zPosEnd - zPosStart, 2));

		// Maximum speed of quadcopter is defined in parameter maxSpeed. In 
		// between two points there is theoretical limit for maximum speed of
		// quadcopter and it depends on maximum acceleration. Since max speed
		// is used to calculate trajectory time and also to recalculate time
		// it is important to use minimum of maxSpeed and theoreticalMaxSpeed.
		// Also initial speed of quadcopter is needed but here it will be zero
		// because quadcopter is going point to point
		double theoreticalMaxSpeed;
		theoreticalMaxSpeed = min(maxSpeed, calculateTheoreticalMaxSpeed(0, 
			tempDistance, maxAcc));

		// trajectoryTime is therefore calculated with realMaxSpeed
		trajectoryTime = tempDistance/theoreticalMaxSpeed;

		// Let's initialize everything needed for trajectory generation. There
		// are 4 coefficients in third order polynomial
		std::vector<double> xCoeffs, yCoeffs, zCoeffs, yawCoeffs;
		for (int i=0; i<=3; i++)
		{
			xCoeffs.push_back(0);
			yCoeffs.push_back(0);
			zCoeffs.push_back(0);
			yawCoeffs.push_back(0);
		}

		getSplineCoefficientsOrder3(xPosStart, xPosEnd, 0, 0, 
			trajectoryTime, xCoeffs);
		getSplineCoefficientsOrder3(yPosStart, yPosEnd, 0, 0, 
			trajectoryTime, yCoeffs);
		getSplineCoefficientsOrder3(zPosStart, zPosEnd, 0, 0, 
			trajectoryTime, zCoeffs);
		getSplineCoefficientsOrder3(yawPosStart, yawPosEnd, 0, 0, 
			trajectoryTime, yawCoeffs);

		// create vectors of vectors for trajectory to be able to use function
		std::vector< std::vector<double> > coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW;
		coefficientsX.push_back(xCoeffs);
		coefficientsY.push_back(yCoeffs);
		coefficientsZ.push_back(zCoeffs);
		coefficientsYAW.push_back(yawCoeffs);

		// Sample trajectory to check for max speed, and maybe later for max acc
		mav_path_trajectory::TrajectorySampled trajectorySampled;
		std::vector<double> parametricTimes;
		std::vector<int> endIndexes;
		parametricTimes.push_back(trajectoryTime);
		sampleMultipleWaypointTrajectory(coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, endIndexes);

		// Search for max speed to ensure trajectory is valid. If max speed is
		// smaller than maxSpeed parameter at constructor, trajectory is
		// replanned with lower execution time. If it's larger trajectory will
		// be slowed down. Index of max speed is also returned but it's now 
		// needed at this point.
		double trajectoryMaxSpeed, trajectoryMaxAcc;
		int tempIndex;
		trajectoryMaxSpeed = getMaxTrajectorySpeed(trajectorySampled, 
			tempIndex);
		trajectoryMaxAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
			tempIndex);
		double sa, sv, s;
		sa = sqrt(trajectoryMaxAcc/maxAcc);
		sv = trajectoryMaxSpeed/theoreticalMaxSpeed;
		s = max(sa, sv);

		// Recalculate trajectory time based on max speed achieved in initial
		// plan.
		trajectoryTime = trajectoryTime*s;

		getSplineCoefficientsOrder3(xPosStart, xPosEnd, 0, 0, 
			trajectoryTime, xCoeffs);
		getSplineCoefficientsOrder3(yPosStart, yPosEnd, 0, 0, 
			trajectoryTime, yCoeffs);
		getSplineCoefficientsOrder3(zPosStart, zPosEnd, 0, 0, 
			trajectoryTime, zCoeffs);
		getSplineCoefficientsOrder3(yawPosStart, yawPosEnd, 0, 0, 
			trajectoryTime, yawCoeffs);

		// Clear trajectory sampled to ensure empty variable is sent to function
		clearTrajectorySampled(trajectorySampled);

		coefficientsX.clear();
		coefficientsY.clear();
		coefficientsZ.clear();
		coefficientsYAW.clear();
		coefficientsX.push_back(xCoeffs);
		coefficientsY.push_back(yCoeffs);
		coefficientsZ.push_back(zCoeffs);
		coefficientsYAW.push_back(yawCoeffs);
		parametricTimes[0] = trajectoryTime;
		// Resample trajectory
		sampleMultipleWaypointTrajectory(coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, endIndexes);

		// In the end publish trajectory. The mission control node will make
		// sure that trajectory is not planned until previous is finished.

		addZeroSpeedAndAccAtTrajectoryEnd(trajectorySampled);
		trajectorySampledPub.publish(trajectorySampled);
	}
}

mav_path_trajectory::TrajectorySampled trajectoryPlanning::twoWaypointsTrajectoryOrder5(
	mav_path_trajectory::WaypointArray waypointArray)
{
	mav_path_trajectory::TrajectorySampled trajectorySampledReturn;
	// First check if there is at least two waypoints in waypointArray
	if(waypointArray.waypoints.size() >= 2)
	{
		// We can work if there is two waypoints. In case of multiple waypoints
		// only first two will be considered

		// For easier notation use extra variables
		double xPosStart, xPosEnd, yPosStart, yPosEnd, zPosStart, zPosEnd;
		double yawPosStart, yawPosEnd;

		// Assigning start and end values
		xPosStart = waypointArray.waypoints[0].position.x;
		yPosStart = waypointArray.waypoints[0].position.y;
		zPosStart = waypointArray.waypoints[0].position.z;
		yawPosStart = waypointArray.waypoints[0].orientation.z;

		xPosEnd = waypointArray.waypoints[1].position.x;
		yPosEnd = waypointArray.waypoints[1].position.y;
		zPosEnd = waypointArray.waypoints[1].position.z;
		yawPosEnd = waypointArray.waypoints[1].orientation.z;

		// Estimate time needed to complete trajectory using euclidian distance
		// that quadcopter needs to travel and max speed provided
		double tempDistance, trajectoryTime, tempYawDistance;
		tempDistance = sqrt(pow(xPosEnd - xPosStart, 2) + 
			pow(yPosEnd - yPosStart, 2) + pow(zPosEnd - zPosStart, 2));
		tempYawDistance = abs(yawPosEnd - yawPosStart);

		// Maximum speed of quadcopter is defined in parameter maxSpeed. In 
		// between two points there is theoretical limit for maximum speed of
		// quadcopter and it depends on maximum acceleration. Since max speed
		// is used to calculate trajectory time and also to recalculate time
		// it is important to use minimum of maxSpeed and theoreticalMaxSpeed.
		// Also initial speed of quadcopter is needed but here it will be zero
		// because quadcopter is going point to point
		double theoreticalMaxSpeed, theoreticalMaxSpeedYaw;
		theoreticalMaxSpeed = min(maxSpeed, calculateTheoreticalMaxSpeed(0, 
			tempDistance, maxAcc));
		theoreticalMaxSpeedYaw = min(maxYawSpeed, calculateTheoreticalMaxSpeed(0, 
			tempYawDistance, maxYawAcc));

		// trajectoryTime is therefore calculated with realMaxSpeed
		if(tempDistance == 0) theoreticalMaxSpeed = 1;
		if(tempYawDistance == 0) theoreticalMaxSpeedYaw = 1;
		trajectoryTime = max(tempDistance/theoreticalMaxSpeed, 
			tempYawDistance/theoreticalMaxSpeedYaw);
		//cout << trajectoryTime << endl;

		// Let's initialize everythin needed for trajectory generation
		std::vector<double> xCoeffs, yCoeffs, zCoeffs, yawCoeffs;
		for (int i=0; i<=5; i++)
		{
			xCoeffs.push_back(0);
			yCoeffs.push_back(0);
			zCoeffs.push_back(0);
			yawCoeffs.push_back(0);
		}

		// Now let's calculate spline coefficients for all degrees of freedom
		getSplineCoefficientsThroughTwoPoints(xPosStart, xPosEnd, 0, 0, 0, 0, 
			trajectoryTime, xCoeffs);
		getSplineCoefficientsThroughTwoPoints(yPosStart, yPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yCoeffs);
		getSplineCoefficientsThroughTwoPoints(zPosStart, zPosEnd, 0, 0, 0, 0, 
			trajectoryTime, zCoeffs);
		getSplineCoefficientsThroughTwoPoints(yawPosStart, yawPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yawCoeffs);

		// create vectors of vectors for trajectory to be able to use function
		std::vector< std::vector<double> > coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW;
		coefficientsX.push_back(xCoeffs);
		coefficientsY.push_back(yCoeffs);
		coefficientsZ.push_back(zCoeffs);
		coefficientsYAW.push_back(yawCoeffs);

		// Sample trajectory to check for max speed, and maybe later for max acc
		mav_path_trajectory::TrajectorySampled trajectorySampled;
		std::vector<double> parametricTimes;
		std::vector<int> endIndexes;
		parametricTimes.push_back(trajectoryTime);
		sampleMultipleWaypointTrajectory(coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, endIndexes);

		// Search for max speed to ensure trajectory is valid. If max speed is
		// smaller than maxSpeed parameter at constructor, trajectory is
		// replanned with lower execution time. If it's larger trajectory will
		// be slowed down. Index of max speed is also returned but it's now 
		// needed at this point.
		double trajectoryMaxSpeed, trajectoryMaxAcc;
		double trajectoryMaxYawSpeed, trajectoryMaxYawAcc;
		int tempIndex;
		trajectoryMaxSpeed = getMaxTrajectorySpeed(trajectorySampled, 
			tempIndex);
		trajectoryMaxAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
			tempIndex);
		trajectoryMaxYawSpeed = getMaxTrajectoryYawSpeed(trajectorySampled, 
			tempIndex);
		trajectoryMaxYawAcc = getMaxTrajectoryYawAcc(trajectorySampled, 
			tempIndex);
		double sa, sv, s, saYaw, svYaw;
		sa = sqrt(trajectoryMaxAcc/maxAcc);
		sv = trajectoryMaxSpeed/theoreticalMaxSpeed;
		saYaw = sqrt(trajectoryMaxYawAcc/maxYawAcc);
		svYaw = trajectoryMaxYawSpeed/theoreticalMaxSpeedYaw;
		s = max(max(max(sa, sv), saYaw), svYaw);

		// Recalculate trajectory time based on max speed achieved in initial
		// plan.
		trajectoryTime = trajectoryTime*s;

		// Recreate trajectory with new time
		// Calculate spline coefficients for all degrees of freedom
		getSplineCoefficientsThroughTwoPoints(xPosStart, xPosEnd, 0, 0, 0, 0, 
			trajectoryTime, xCoeffs);
		getSplineCoefficientsThroughTwoPoints(yPosStart, yPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yCoeffs);
		getSplineCoefficientsThroughTwoPoints(zPosStart, zPosEnd, 0, 0, 0, 0, 
			trajectoryTime, zCoeffs);
		getSplineCoefficientsThroughTwoPoints(yawPosStart, yawPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yawCoeffs);

		// Clear trajectory sampled to ensure empty variable is sent to function
		clearTrajectorySampled(trajectorySampled);

		coefficientsX.clear();
		coefficientsY.clear();
		coefficientsZ.clear();
		coefficientsYAW.clear();
		coefficientsX.push_back(xCoeffs);
		coefficientsY.push_back(yCoeffs);
		coefficientsZ.push_back(zCoeffs);
		coefficientsYAW.push_back(yawCoeffs);
		parametricTimes[0] = trajectoryTime;
		// Resample trajectory
		sampleMultipleWaypointTrajectory(coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, endIndexes);
		for(int i=0; i<trajectorySampled.position.size(); i++)
		{
			trajectorySampled.segment.push_back(1);
		}

		// In the end publish trajectory. The mission control node will make
		// sure that trajectory is not planned until previous is finished.
		addZeroSpeedAndAccAtTrajectoryEnd(trajectorySampled);
		trajectorySampledReturn = trajectorySampled;
		trajectorySampledPub.publish(trajectorySampled);

		// !!! IMPORTANT !!!
		// It would be good to have a service that checks with control node if
		// current trajectory is executed, and when it's executed publish
		// a new one. This has to be considered further
	}

	return trajectorySampledReturn;
}

mav_path_trajectory::TrajectorySampled trajectoryPlanning::hoCook56(
	mav_path_trajectory::WaypointArray waypointArray)
{
	mav_path_trajectory::TrajectorySampled trajectorySampledReturn;
	// This function uses Ho-Cook method for spline planning and generates
	// trajectory for quadcopter. In this case polynomial orders are 6 for 
	// first and last segment and 5 for all other segments. Minimul number of
	// waypoints to use this function is 3. If two waypoints are provided
	// trajectory will be generated through two points using 
	// twoWaypointsTrajectoryOrder5 function

	// Check for size
	int m = waypointArray.waypoints.size(); // Number of points in trajectory
	if(m == 2)
	{	
		// If there is not enough points for Ho-Cook method simply compute
		// and publish a trajectory through two points
		//trajectorySampledReturn = twoWaypointsTrajectoryOrder5(waypointArray);
		geometry_msgs::Pose tempPose;
		tempPose.position.x = (waypointArray.waypoints[0].position.x + 
			waypointArray.waypoints[1].position.x)/2.0;
		tempPose.position.y = (waypointArray.waypoints[0].position.y + 
			waypointArray.waypoints[1].position.y)/2.0;
		tempPose.position.z = (waypointArray.waypoints[0].position.z + 
			waypointArray.waypoints[1].position.z)/2.0;
		tempPose.orientation.z = (waypointArray.waypoints[0].orientation.z + 
			waypointArray.waypoints[1].orientation.z)/2.0;
		waypointArray.waypoints.insert(waypointArray.waypoints.begin()+1, tempPose);
		//for (int i=0; i<waypointArray.waypoints.size(); i++)
		//{
		//	cout << waypointArray.waypoints[i];
		//}
	}



	m = waypointArray.waypoints.size();
	if(m >= 3)
	{
		// Now we can interpolate using 4 and 5 order polynoms.

		// First of all we can compute parametrical times for the trajectory 
		// segments. It is simply euclidian distance divided by preset max speed
		// of quadcopter given through ROS param in constructor.
		std::vector<double> parametricTimes, parametricLengths;
		double tempXstart, tempXend, tempYstart, tempYend, tempZstart, tempZend;
		double tempYawStart, tempYawEnd;
		//cout << "parametricalTimes: " << endl;
		for(int i=2; i<=m; i++)
		{
			// Temporary values just to have clear equation later. Since it is
			// starting at 2 and waypoints are starting from zero. 
			tempXstart = waypointArray.waypoints[i-2].position.x;
			tempXend = waypointArray.waypoints[i-1].position.x;
			tempYstart = waypointArray.waypoints[i-2].position.y;
			tempYend = waypointArray.waypoints[i-1].position.y;
			tempZstart = waypointArray.waypoints[i-2].position.z;
			tempZend = waypointArray.waypoints[i-1].position.z;
			tempYawStart = waypointArray.waypoints[i-2].orientation.z;
			tempYawEnd = waypointArray.waypoints[i-1].orientation.z;

			// Computing parametrical time
			parametricTimes.push_back(max(sqrt(pow(tempXend - tempXstart, 2)
				+ pow(tempYend - tempYstart, 2) 
				+ pow(tempZend - tempZstart, 2))/maxSpeed, 
				abs(tempYawEnd - tempYawStart)/maxYawSpeed));
			parametricLengths.push_back(parametricTimes[i-2]*maxSpeed);
			//cout << parametricTimes[i-2] << endl;
		}

		// Here things get somewhat more complicated than in hoCook34. There
		// are twice as much conditions here because we are continuing splines
		// using speed and acceleration to have jerk free trajectory. Matrix M
		// is no longer (m-2)x(m-2) but [2*(m-2)]x[2*(m-2)] due to twice as
		// much conditions. Therefore matrix M will be divided in 4 matrices 
		// and combined at the end. Also all the members of this matrix will
		// be set to 0 initially.
		Eigen::MatrixXd M(2*(m-2), 2*(m-2));
		// Set all members of matrix to 0. This is actually faster than
		// M << M*0.0 and better since there were sam NAN elements in matrix
		// otherwise
		for(int i=0; i<2*(m-2); i++)
		{
			for(int j=0; j<2*(m-2); j++)
			{
				M(i,j) = 0.0;
			}
		}

		// Now we have matrix M all set, we just have to fill it.
		createMatrixMForHoCook56(M, parametricTimes, m);

		// Now that matrix M is here it's good to create matrices A for all
		// three axis. Additionaly create matrix A for yaw allthough yaw is
		// not used in initial parametric time calculation.
		Eigen::MatrixXd Ax(1, 2*(m-2));
		Eigen::MatrixXd Ay(1, 2*(m-2));
		Eigen::MatrixXd Az(1, 2*(m-2));
		Eigen::MatrixXd Ayaw(1, 2*(m-2));
		// This is messy work so it's done in function. It will be done in one
		// function called 3 times for x, y and z so all waypoints data has to
		// be in a vector. Adding yaw
		std::vector<double> positionsX, positionsY, positionsZ, positionsYAW;
		for(int i=0; i<waypointArray.waypoints.size(); i++)
		{
			positionsX.push_back(waypointArray.waypoints[i].position.x);
			positionsY.push_back(waypointArray.waypoints[i].position.y);
			positionsZ.push_back(waypointArray.waypoints[i].position.z);
			positionsYAW.push_back(waypointArray.waypoints[i].orientation.z);
		}

		// Create matrices
		createMatrixAForHoCook56(Ax, positionsX, parametricTimes, m);
		createMatrixAForHoCook56(Ay, positionsY, parametricTimes, m);
		createMatrixAForHoCook56(Az, positionsZ, parametricTimes, m);
		createMatrixAForHoCook56(Ayaw, positionsYAW, parametricTimes, m);
		//cout << Az << endl;
		
		// Now we can compute vector of speed and acceleration conditions using
		// matrices A and M. First create new matrix that will be inverse of
		// M so it won't be inverted three times
		Eigen::MatrixXd Minv = M.inverse();

		// Vectors containing conditions
		Eigen::MatrixXd xd, yd, zd, yawd;
		xd = Ax*Minv;
		yd = Ay*Minv;
		zd = Az*Minv;
		yawd = Ayaw*Minv;

		// Okay, here is everything needed for coefficients calculation. 
		// They'll be put in a vector of vectors, first and last segment with
		// 6 coefficients; other segments with 5 coefficients.
		std::vector< std::vector< double > > trajectoryCoefficientsX, 
		trajectoryCoefficientsY, trajectoryCoefficientsZ, trajectoryCoefficientsYAW;
		double tempV7[7] = {0, 0, 0, 0, 0, 0, 0};
		double tempV6[6] = {0, 0, 0, 0, 0, 0};

		// First goes segment one with 7 coefficients
		std::vector<double> tempVector7(&tempV7[0], &tempV7[0]+7);
		trajectoryCoefficientsX.push_back(tempVector7);
		trajectoryCoefficientsY.push_back(tempVector7);
		trajectoryCoefficientsZ.push_back(tempVector7);
		trajectoryCoefficientsYAW.push_back(tempVector7);

		// Now for the segments except last
		for(int i=1; i<m-1-1; i++)
		{
			std::vector<double> tempVector(&tempV6[0], &tempV6[0]+6);
			trajectoryCoefficientsX.push_back(tempVector);
			trajectoryCoefficientsY.push_back(tempVector);
			trajectoryCoefficientsZ.push_back(tempVector);
			trajectoryCoefficientsYAW.push_back(tempVector);
		}

		// Last segment coefficients are also of length 7. This works.
		trajectoryCoefficientsX.push_back(tempVector7);
		trajectoryCoefficientsY.push_back(tempVector7);
		trajectoryCoefficientsZ.push_back(tempVector7);
		trajectoryCoefficientsYAW.push_back(tempVector7);

		// Everything is set for calculation of coefficients of spline segments
		// based on waypoints, calculated conditions and parametric times.
		calculateCoefficientsHoCook56(trajectoryCoefficientsX, xd, positionsX, 
			parametricTimes);
		calculateCoefficientsHoCook56(trajectoryCoefficientsY, yd, positionsY, 
			parametricTimes);
		calculateCoefficientsHoCook56(trajectoryCoefficientsZ, zd, positionsZ, 
			parametricTimes);
		calculateCoefficientsHoCook56(trajectoryCoefficientsYAW, yawd, 
			positionsYAW, parametricTimes);


		// Now we can sample that trajectory to find out max speed and acceleration
		// Let's first create trajectory
		mav_path_trajectory::TrajectorySampled trajectorySampled;
		std::vector<int> trajectoryEndIndexes;
		sampleMultipleWaypointTrajectory(trajectoryCoefficientsX, 
			trajectoryCoefficientsY, trajectoryCoefficientsZ, 
			trajectoryCoefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, trajectoryEndIndexes);

		// Trajectory is here but speeds and accelerations are not what they are
		// most likely too big. That's why we now enter the second stage of
		// Ho-Cook method in which we find critical acceleration and speed or
		// in other words the biggest by absolute value. That is done using 
		// total speed and acceleration of quadcopter. This part may require
		// more than one iteration, based on sample time given. In the end the
		// scaling factor should be 1, or in our case between 0.99 and 1.01.

		// First implementation goes with only one iteration
		int tempIndexAcc, tempIndexSpeed;
		double maxTrajectorySpeed = getMaxTrajectorySpeed(trajectorySampled, 
			tempIndexSpeed);
		double maxTrajectoryAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
			tempIndexAcc);
		double maxTrajectoryYawSpeed = getMaxTrajectoryYawSpeed(trajectorySampled, 
			tempIndexSpeed);
		double maxTrajectoryYawAcc = getMaxTrajectoryYawAcc(trajectorySampled, 
			tempIndexAcc);

		// Speed and acceleration scaling factors
		double sv = maxTrajectorySpeed/maxSpeed;
		double sa = sqrt(maxTrajectoryAcc/maxAcc*1.0);
		double svYaw = maxTrajectoryYawSpeed/maxYawSpeed;
		double saYaw = sqrt(maxTrajectoryYawAcc/maxAcc*1.0);
		double s = max(max(max(sv, sa), svYaw), saYaw);
		//cout << "sa: " << sa << " sv: " << sv << " speed: " 
		//	<< maxTrajectorySpeed << " acc: " << maxTrajectoryAcc << endl;

		// First parametric times are scaled so one segment has maximum speed
		// or acceleration. Then, in a loop, each segment is scaled separately
		// to achieve agressive behavior
		std::vector<double> parametricTimeSegmentScaler, 
			parametricTimeSegmentScalerFlags;
		for(int pts=0; pts<parametricTimes.size(); pts++)
		{
			parametricTimeSegmentScaler.push_back(1.0);
			parametricTimeSegmentScalerFlags.push_back(true);
		}

		// Flag which exits while loop
		bool segmentsScaledFlag = false;
		int iteration = 0;

		//cout << trajectorySampled << endl;
		//s = 1.0;

		while(iteration <= 0)
		{
			iteration++;
			// Scale parametric times in each iteration to achieve perfection
			// Basically redoing Ho-Cook but it's very important to clear
			// sampled trajectory before we get into everything.
			clearTrajectorySampled(trajectorySampled);

			// After that a slight modification. parametricTimes must be 
			// scaled with scaling factor s so:
			for(int i=0; i<parametricTimes.size(); i++)
			{
				if(parametricTimeSegmentScalerFlags[i])
				{
					parametricTimes[i] =
						parametricTimes[i]*s*parametricTimeSegmentScaler[i];
				}
			}

			// At this point parametric times are computed and we can begin
			// forming matrix M. It is the size 2(m-2)x2(m-2) and inverse will be
			// needed later so Eigen seems like pretty logical solution. Initial
			// values are all set to 0
			for(int i=0; i<2*(m-2); i++)
			{
				for(int j=0; j<2*(m-2); j++)
				{
					M(i,j) = 0.0;
				}
			}

			// Now we have matrix M all set, we just have to fill it.
			createMatrixMForHoCook56(M, parametricTimes, m);

			// Initialize all zeros for A matrices
			Ax << Ax*0.0;
			Ay << Ay*0.0;
			Az << Az*0.0;
			Ayaw << Ayaw*0.0;

			// Create matrices
			createMatrixAForHoCook56(Ax, positionsX, parametricTimes, m);
			createMatrixAForHoCook56(Ay, positionsY, parametricTimes, m);
			createMatrixAForHoCook56(Az, positionsZ, parametricTimes, m);
			createMatrixAForHoCook56(Ayaw, positionsYAW, parametricTimes, m);

			// Now we can compute vector of speed and acceleration conditions 
			// using matrices A and M. First create new matrix that will be 
			// inverse of M so it won't be inverted three times
			Minv = M.inverse();

			// Vectors containing conditions
			xd = Ax*Minv;
			yd = Ay*Minv;
			zd = Az*Minv;
			yawd = Ayaw*Minv;

			// Everything is set for calculation of coefficients of spline 
			// segments based on waypoints, calculated conditions and 
			// parametric times.
			calculateCoefficientsHoCook56(trajectoryCoefficientsX, xd, positionsX, 
				parametricTimes);
			calculateCoefficientsHoCook56(trajectoryCoefficientsY, yd, positionsY, 
				parametricTimes);
			calculateCoefficientsHoCook56(trajectoryCoefficientsZ, zd, positionsZ, 
				parametricTimes);
			calculateCoefficientsHoCook56(trajectoryCoefficientsYAW, yawd, positionsYAW, 
				parametricTimes);

			// Here we also have everything needed
			sampleMultipleWaypointTrajectory(trajectoryCoefficientsX, 
				trajectoryCoefficientsY, trajectoryCoefficientsZ, 
				trajectoryCoefficientsYAW, parametricTimes, 
				trajectorySampled, trajectorySamplingFrequency, trajectoryEndIndexes);

			// Getting the max speeds, it's more of a legacy than it's needed
			// but okay
			maxTrajectorySpeed = getMaxTrajectorySpeed(trajectorySampled, 
				tempIndexSpeed);
			maxTrajectoryAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
				tempIndexAcc);

			//cout << "maxSpd: " << maxTrajectorySpeed << 
			//	" maxAcc: " << maxTrajectoryAcc << endl;

			// Speed and acceleration scaling factors
			s = 1.0;

			// In second iteration set parametricTimeSegmentScaler
			// The idea is to scale each segment to maximum value. Problem
			// here is that if segment is too short it can't develop maxSpeed
			// or maxAcc so calculation for theoretical maxumum values is
			// needed at this point.
			//parametricTimeSegmentScaler

			for(int i=0; i<parametricTimes.size(); i++)
			{
				double svSegment, saSegment, maxSpeedSegment, maxAccSegment;
				double theoreticalMaxSpeed, theoreticalMaxSpeedEnd, minSpeed;
				if(i == 0)
				{
					maxSpeedSegment = getMaxTrajectorySpeedInterval(
						trajectorySampled, 0, trajectoryEndIndexes[i]);
					maxAccSegment = getMaxTrajectoryAccelerationInterval(
						trajectorySampled, 0, trajectoryEndIndexes[i]);

					double tempInitialSpeed = 0.0;
					theoreticalMaxSpeed = calculateTheoreticalMaxSpeed(
						tempInitialSpeed, parametricLengths[i], maxAcc);
					tempInitialSpeed = sqrt(pow(xd(0,i+1),2)+ 
						pow(yd(0,i+1),2) + pow(zd(0,i+1),2));
					theoreticalMaxSpeedEnd = calculateTheoreticalMaxSpeed(
						tempInitialSpeed, parametricLengths[i], maxAcc);
					theoreticalMaxSpeedEnd = maxSpeed;

					minSpeed = min(maxSpeed, min(theoreticalMaxSpeed, 
						theoreticalMaxSpeedEnd));
					svSegment = maxSpeedSegment/minSpeed;
					saSegment = sqrt(maxAccSegment/maxAcc*1.0);
					parametricTimeSegmentScaler[i] = max(svSegment, saSegment);
					//cout << "th max speed " << theoreticalMaxSpeed << " end: "
					//	<< theoreticalMaxSpeedEnd << " min: " << minSpeed << endl;
					//cout << "saSeg: " << saSegment << " svSeg: " << svSegment << endl;
				}

				else if (i > 0 && (i < (parametricTimes.size() - 1)))
				{
					maxSpeedSegment = getMaxTrajectorySpeedInterval(
						trajectorySampled, trajectoryEndIndexes[i-1], trajectoryEndIndexes[i]);
					maxAccSegment = getMaxTrajectoryAccelerationInterval(
						trajectorySampled, trajectoryEndIndexes[i-1], trajectoryEndIndexes[i]);

					double tempInitialSpeed = sqrt(pow(xd(0,i-1),2)+ 
						pow(yd(0,i-1),2) + pow(zd(0,i-1),2));
					theoreticalMaxSpeed = calculateTheoreticalMaxSpeed(
						tempInitialSpeed, parametricLengths[i], maxAcc);
					tempInitialSpeed = sqrt(pow(xd(0,i),2)+ 
						pow(yd(0,i),2) + pow(zd(0,i),2));
					theoreticalMaxSpeedEnd = calculateTheoreticalMaxSpeed(
						tempInitialSpeed, parametricLengths[i], maxAcc);
					theoreticalMaxSpeedEnd = maxSpeed;

					minSpeed = min(maxSpeed, min(theoreticalMaxSpeed, 
						theoreticalMaxSpeedEnd));
					svSegment = maxSpeedSegment/minSpeed;
					//cout << "th max speed " << theoreticalMaxSpeed << " end: "
					//	<< theoreticalMaxSpeedEnd << " min: " << minSpeed << endl;
					saSegment = sqrt(maxAccSegment/maxAcc*1.0);
					//cout << "saSeg: " << saSegment << " svSeg: " << svSegment << endl;
					parametricTimeSegmentScaler[i] = max(svSegment, saSegment);
				}
				else if (i == (parametricTimes.size()-1))
				{
					maxSpeedSegment = getMaxTrajectorySpeedInterval(
						trajectorySampled, trajectoryEndIndexes[i-1], trajectoryEndIndexes[i]);
					maxAccSegment = getMaxTrajectoryAccelerationInterval(
						trajectorySampled, trajectoryEndIndexes[i-1], trajectoryEndIndexes[i]);

					double tempInitialSpeed = sqrt(pow(xd(0,i-1),2)+ 
						pow(yd(0,i-1),2) + pow(zd(0,i-1),2));
					theoreticalMaxSpeed = calculateTheoreticalMaxSpeed(
						tempInitialSpeed, parametricLengths[i], maxAcc);
					tempInitialSpeed = 0.0;
					theoreticalMaxSpeedEnd = calculateTheoreticalMaxSpeed(
						tempInitialSpeed, parametricLengths[i], maxAcc);
					theoreticalMaxSpeedEnd = maxSpeed;

					minSpeed = min(maxSpeed, min(theoreticalMaxSpeed, 
						theoreticalMaxSpeedEnd));
					svSegment = maxSpeedSegment/minSpeed;
					saSegment = sqrt(maxAccSegment/maxAcc*1.0);
					parametricTimeSegmentScaler[i] = max(svSegment, saSegment);
					/*cout << "th max speed " << theoreticalMaxSpeed << " end: "
						<< theoreticalMaxSpeedEnd << " min: " << minSpeed << endl;
					cout << "saSeg: " << saSegment << " svSeg: " << svSegment << endl;*/
				}
			}

			for(int i=0; i<parametricTimeSegmentScaler.size(); i++)
			{
				if(parametricTimeSegmentScaler[i] > 0.95 && 
					parametricTimeSegmentScaler[i] < 1.05)
				{
					//parametricTimeSegmentScaler[i] = 1.0;
					parametricTimeSegmentScalerFlags[i] = false;
				}
				//cout << parametricTimeSegmentScaler[i] << " " 
				//	<< parametricTimes[i] << endl;
			}
			for(int i=0; i<trajectoryEndIndexes.size(); i++)
			{
				//cout << trajectoryEndIndexes[i] << endl;
			}

			// Adding information about which index belongs to which segmet
			int segmentCount = 1;
			for(int i=0; i<trajectorySampled.position.size(); i++)
			{
				trajectorySampled.segment.push_back(segmentCount);
				if(trajectoryEndIndexes[segmentCount-1] == i)
				{
					segmentCount++;
				}
			}

			// First let's see with one iteration
			//break;
			//cout << "------------------------------------------------------" << endl;
		}
		addZeroSpeedAndAccAtTrajectoryEnd(trajectorySampled);
		trajectorySampledReturn = trajectorySampled;
		trajectorySampledPub.publish(trajectorySampled);
	}

	return trajectorySampledReturn;
}


void trajectoryPlanning::createMatrixMForHoCook56(
	Eigen::MatrixXd &M, std::vector<double> t, int m)
{
	// First initialize submatrices of matrix M. Matrix M can be represented as
	//     [M1 | M2]
	// M = [-------]
	//     [M3 | M4]
	// Where M1, M2, M3 and M4 submatrices are of dimensions (m-2)x(m-2)
	Eigen::MatrixXd M1(m-2, m-2);
	Eigen::MatrixXd M2(m-2, m-2);
	Eigen::MatrixXd M3(m-2, m-2);
	Eigen::MatrixXd M4(m-2, m-2);
	// And fill them up with zeros
	for(int i=0; i<(m-2); i++)
	{
		for(int j=0; j<(m-2); j++)
		{
			M1(i,j) = 0.0;
			M2(i,j) = 0.0;
			M3(i,j) = 0.0;
			M4(i,j) = 0.0;
		}
	}

	// All matrices will be filled column by column. First and last column are
	// different than all the others so these will be filled separately.
	if(m == 3)
	{
		// In every parametrical time we have correction factor -2 because
		// in reality they start at t2 but indexing of arrays goes from 0
		// so a bit of subtraction is needed. This is special case when
		// there are only two segments. Also it is not relevant whether first
		// or last segment is used since it's only a number. Maybe it is
		// relevant if there is difference in + or - but I hope it's not.
		M1(0,0) = 20.0*pow(t[3-2], 2) - 12*pow(t[2-2], 2);
		M2(0,0) = 40.0*pow(t[3-2], 3) + 16*pow(t[2-2], 3);
		M3(0,0) = -4.0*t[2-2]*pow(t[3-2], 2) - 3.0*pow(t[2-2], 2)*t[3-2];
		M4(0,0) = -6.0*t[2-2]*pow(t[3-2], 3) + 3.0*pow(t[2-2], 3)*t[3-2];
		// CHECKED, working
	}

	if(m >= 4)
	{
		// Filling first and last columns of submatrices
		// CHECKED, works
		// M1:
		M1(0,0) = 20.0*pow(t[3-2], 2) - 12*pow(t[2-2], 2);
		M1(1,0) = -8.0*pow(t[2-2], 2);
		M1(m-3-1, m-2-1) = 8.0*pow(t[m-2], 2);
		M1(m-2-1, m-2-1) = 12.0*pow(t[m-2], 2) - 20.0*pow(t[m-1-2], 2);
		// M2:
		M2(0,0) = 40.0*pow(t[3-2], 3) + 16*pow(t[2-2], 3);
		M2(1,0) = 14.0*pow(t[2-2], 3);
		M2(m-3-1, m-2-1) = 14.0*pow(t[m-2], 3);
		M2(m-2-1, m-2-1) = 16.0*pow(t[m-2], 3) + 40.0*pow(t[m-1-2], 3);
		// M3:
		M3(0,0) = -4.0*t[2-2]*pow(t[3-2], 2) - 3.0*pow(t[2-2], 2)*t[3-2];
		M3(1,0) = pow(t[2-2], 2)*t[3-2];
		M3(m-3-1, m-2-1) = t[m-1-2]*pow(t[m-2], 2);
		M3(m-2-1, m-2-1) = -3.0*t[m-1-2]*pow(t[m-2], 2) - 4.0*pow(t[m-1-2], 2)*t[m-2];
		// M4:
		M4(0,0) = -6.0*t[2-2]*pow(t[3-2], 3) + 3.0*pow(t[2-2], 3)*t[3-2];
		M4(1,0) = -2.0*pow(t[2-2], 3)*t[3-2];
		M4(m-3-1, m-2-1) = 2*t[m-1-2]*pow(t[m-2], 3);
		M4(m-2-1, m-2-1) = -3.0*t[m-1-2]*pow(t[m-2], 3) + 6.0*pow(t[m-1-2], 3)*t[m-2];

		// Now we can start filling the inner parts of matrices
		for(int col=1; col <= (m-3)-1; col++)
		{
			// When filling inner columns row starts at col-1 as we look at
			// the matrix. Three rows are to be filled so we increment row
			// two times. Also that means the matrix will have elements only
			// on the main diagonal and it's subdiagonals.
			int row = col-1;
			// Also there is a rule to use parametrical times with column as
			// index. There is -2 subtraction on every place in indexing and
			// that's because parametrical times start at index 0. Example:
			// t2 = t[2-2] => t2 = t[0]

			// All four matrices are very similar, with main diagonal and one 
			// or two subdiagonals so we can use same indexes
			M1(row, col) = 8.0*pow(t[col+3-2], 2);
			M1(row+1, col) = 12.0*(pow(t[col+3-2], 2) - pow(t[col+2-2], 2));
			M1(row+2, col) = -8.0*pow(t[col+2-2], 2);

			M2(row, col) = 14.0*pow(t[col+3-2], 3);
			M2(row+1, col) = 16.0*(pow(t[col+2-2], 3) + pow(t[col+3-2], 3));
			M2(row+2, col) = 14.0*pow(t[col+2-2], 3);

			M3(row, col) = 1.0*t[col+2-2]*pow(t[col+3-2], 2);
			M3(row+1, col) = -3.0*(t[col+2-2]*pow(t[col+3-2], 2) + 
				pow(t[col+2-2], 2)*t[col+3-2]);
			M3(row+2, col) = pow(t[col+2-2], 2)*t[col+3-2];

			M4(row, col) = -2.0*t[col+2-2]*pow(t[col+3-2], 3);
			M4(row+1, col) = 3.0*(-t[col+2-2]*pow(t[col+3-2], 3) + 
				pow(t[col+2-2], 3)*t[col+3-2]);
			M4(row+2, col) = -2.0*pow(t[col+2-2], 3)*t[col+3-2];
			// CHECKED, should work
		}
	}

	// In the end fill the matrix M with M1, M2, M3 and M4
	for(int i=0; i<(m-2); i++)
	{
		for(int j=0; j<(m-2); j++)
		{
			M(i,j) = M1(i,j);
			M(i,j+m-2) = M2(i,j);
			M(i+m-2,j) = M3(i,j);
			M(i+m-2, j+m-2) = M4(i,j);
		}
	}
}

void trajectoryPlanning::createMatrixAForHoCook56(
	Eigen::MatrixXd &A, std::vector<double> x, std::vector<double> t, int m)
{
	// Function calculates matrix A based on parametric times and waypoints.
	if(m == 3)
	{
		// Here we will have only two colmns of matrix A
		A(0,0) = (20.0/(t[2-2]*t[3-2]))*(2.0*pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 3)*(x[2-1] - x[3-1]));

		A(0,(1-1)+m-2) = (30.0/(t[2-2]*t[3-2]))*(3*pow(t[3-2], 4)*(x[2-1] - x[1-1]) 
			+ pow(t[2-2], 4)*(x[3-1] - x[2-1]));
	}
	else if(m >= 4)
	{
		// Add the first and last segments, these are corresponding to
		// k=1, k=m-1 for upper and lower part of matrix. Everything will be
		// done here in one loop
		// Upper part of matrix, equations based on continuous x'''
		A(0,0) = (20.0/(t[2-2]*t[3-2]))*(2.0*pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 3)*(x[2-1] - x[3-1]));

		A(0, m-2-1) = (20.0/(t[m-1-2]*t[m-2]))*(
			pow(t[m-2], 3)*(x[m-1-1] - x[m-2-1]) + 
			2.0*pow(t[m-1-2], 3)*(x[m-1-1] - x[m-1]));

		// Lower part of matrix equations based on x''''
		A(0, (1-1)+m-2) = (30.0/(t[2-2]*t[3-2]))*(3*pow(t[3-2], 4)*(x[2-1] - x[1-1]) 
			+ pow(t[2-2], 4)*(x[3-1] - x[2-1]));

		A(0, (m-2-1)+m-2) = (30.0/(t[m-1-2]*t[m-2]))*(
			pow(t[m-2], 4)*(x[m-1-1] - x[m-2-1]) + 
			3.0*pow(t[m-1-2], 4)*(x[m-1] - x[m-1-1]));

		// Now all the other members. Lower part of matrix is m-2 members 
		// "ahead" upper part so there will be offset of (m-2) on indexes
		// regarding upper part of matrix
		for(int k=2; k<=(m-3); k++)
		{
			// Upper equation
			A(0, k-1) = (20.0/(t[k+1-2]*t[k+2-2]))*(
				pow(t[k+2-2], 3)*(x[k+1-1] - x[k-1]) + 
				pow(t[k+1-2], 3)*(x[k+1-1] - x[k+2-1]));

			// Lower equation
			A(0, k-1+m-2) = (30.0/(t[k+1-2]*t[k+2-2]))*(
				pow(t[k+2-2], 4)*(x[k+1-1] - x[k-1]) + 
				pow(t[k+1-2], 4)*(x[k+2-1] - x[k+1-1]));
		}
	}
}

void trajectoryPlanning::calculateCoefficientsHoCook56(
	std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
	std::vector<double> x, std::vector<double> t)
{
 	// Function used to calculate polynomial coefficients for one axis at
	// the time. 

	// Get how many waypoints are in trajectory
	int m = x.size();

	// First segment is 6th order so 7 coeffs are needed. Starting jerk is set
	// to 0 here
	getSplineCoefficientsOrder6JerkStart(x[0], x[1], 0, D(0,0), 0, D(0, m-2), 
		0, t[0], coefficients[0]);

	// Now the middle coefficients, they are all 4. order polynoms with
	// acceleration condition at start.
	//           k<(m-3) is equivalent
	for(int k=2; k<=(m-2); k++)
	{
		getSplineCoefficientsThroughTwoPoints(x[k-1], x[k], D(0, k-2), D(0, k-1), 
			D(0, k-2+m-2), D(0, k-1+m-2), t[k+1-2], coefficients[k-1]);
	}

	// And the last segment is order 6 with 0 jerk at the end
	getSplineCoefficientsOrder6JerkEnd(x[m-1-1], x[m-1], D(0, m-2-1), 0, 
		D(0, (m-2-1)+m-2), 0, 0, t[m-2], coefficients[m-2]);
}

void trajectoryPlanning::waypointCallback(
	const mav_path_trajectory::WaypointArray &msg)
{
	// Getting the waypoints for quadcopter, there can be multiple waypoints.
	// The ultimate goal is to use MoveIt! to plan a trajectory in between
	// all waypoints in waypoint array

	// Initial testing involves creating trajectory through two waypoints
	//twoWaypointsTrajectoryOrder5(msg);

	//hoCook34(msg);
	//hoCook45(msg);
	//hoCook45_v2(msg);
	hoCook56(msg);
	//twoWaypointsTrajectoryOrder3(msg);
	//twoWaypointsTrajectoryOrder5(msg);
	//twoWaypointsTrajectoryOrder4AccStart(msg);
	//twoWaypointsTrajectoryOrder4AccEnd(msg);
}

void trajectoryPlanning::addZeroSpeedAndAccAtTrajectoryEnd(
	mav_path_trajectory::TrajectorySampled &trajectorySampled)
{
	trajectorySampled.position.push_back(
		trajectorySampled.position[trajectorySampled.position.size()-1]);
	geometry_msgs::Vector3 tempVector;
	tempVector.x = 0.0;
	tempVector.y = 0.0;
	tempVector.z = 0.0;
	trajectorySampled.speed.push_back(tempVector);
	trajectorySampled.acceleration.push_back(tempVector);
	trajectorySampled.yawPos.push_back(trajectorySampled.yawPos[trajectorySampled.yawPos.size()-1]);
	trajectorySampled.yawSpeed.push_back(0.0);
	trajectorySampled.yawAcc.push_back(0.0);
}
