#!/usr/bin/python
import heapq
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point, Twist, Vector3, PoseStamped, Pose
#from mav_msgs.msg import CommandTrajectory
from nav_msgs.msg import Path
from std_msgs.msg import ColorRGBA
import rospy
import copy
import numpy as np
import scipy
from scipy import misc
from math import ceil,atan2,sin,cos,sqrt,floor
import sys

#from euroc_solution.srv import *

sys.setrecursionlimit(1000000) #needed for large maps

from ompl import base as ob
from ompl import geometric as og
import json
import rospkg

class PathPlanner:

    def __init__(self):
        rospack = rospkg.RosPack()
        self.gridUAV = misc.imread(
            rospack.get_path('mav_path_trajectory') + 
            '/resource/FreeStyleMapQuad.png',1)
        self.gridUGV = misc.imread(
            rospack.get_path('mav_path_trajectory') + 
            '/resource/FreeStyleMapMobile.png',1)

        # Visualize path related stuff
        self.publishPath = rospy.Publisher("/path_cells_vis_path", Path, 
            queue_size=1)
        self.path = Path()
        tempPoseStamped = PoseStamped()
        self.path.poses.append(tempPoseStamped)

        self.startGoalSub = rospy.Subscriber("/start_goal", Pose, 
            self.startGoalCallback)
        self.start = Vector3()
        self.goal = Vector3()

        # Marker
        self.markerPub = rospy.Publisher("/path_markers", Marker, 
            queue_size=1)

    def run(self):
        # Visualize path
        r = rospy.Rate(30)

        while not rospy.is_shutdown():
            self.path.header.stamp = rospy.Time.now()
            self.path.header.frame_id = "map"

            self.publishPath.publish(self.path)

            r.sleep()

    def startGoalCallback(self, msg):
        self.start.x = msg.position.x
        self.start.y = msg.position.y
        self.start.z = msg.position.z
        self.goal.x = msg.orientation.x
        self.goal.y = msg.orientation.y
        self.goal.z = msg.orientation.z

        self.plan()

    def plan(self):
        # Create and SE3 state space, in this case we are going to use it
        # on fixed height, so it's esentially SE2
        self.space = ob.SE3StateSpace()

        # set lower and upper bounds
        bounds = ob.RealVectorBounds(3)
        #bounds.setLow(0, 0)
        #bounds.setHigh(0, 3)
        #bounds.setLow(1, 0)
        #bounds.setHigh(1, 4)
        #bounds.setLow(2, 0)
        #bounds.setHigh(2, 0)
        bounds.setLow(0, -1.5)
        bounds.setHigh(0, 1.5)
        bounds.setLow(1, -2)
        bounds.setHigh(1, 2)
        bounds.setLow(2, 0)
        bounds.setHigh(2, 0)
        self.space.setBounds(bounds)

        # create a simple setup object
        self.ss = og.SimpleSetup(self.space)
        self.ss.setStateValidityChecker(
            ob.StateValidityCheckerFn(self.isStateValid))
        self.space.setLongestValidSegmentFraction(
            0.05/self.space.getMaximumExtent())

        # (optionally) set planner
        si = self.ss.getSpaceInformation()
        planner = og.RRTstar(si)
        #planner.setPrune(True)
        planner.setGoalBias(0.05)
        planner.setRange(0.5)

        # planner.setDelayCC(True)
        #planner.setPruneStatesImprovementThreshold(0.95)
        self.ss.setPlanner(planner)
        start = ob.State(self.space)
        # we can pick a random start state...
        # ... or set specific values
        start().setXYZ(self.start.x, self.start.y, self.start.z*0)
        #always ultra light
        start().rotation().setAxisAngle(0, 0, 0, 1)
        goal = ob.State(self.space)
        # we can pick a random goal state...
        # ... or set specific values
        goal().setXYZ(self.goal.x, self.goal.y, self.goal.z*0)
        goal().rotation().setAxisAngle(0, 0, 0, 1)

        self.ss.setStartAndGoalStates(start, goal)
        dt = 0
        plan_flag = True

        self.ix = 0; self.iy = 0

        startValid = self.isStateValid(start())
        #self.gridUAV[self.ix][self.iy] = 1.0
        #print startValid, self.ix, self.iy
        #misc.imshow(self.gridUAV)

        endValid = self.isStateValid(goal())
        print startValid, endValid

        if not (startValid and endValid):
            rospy.loginfo("Invalid start/end point.")
            return -1

        while (plan_flag):
            #Plan, while not successful. Add 2 sec each time planner faild to return a plan.
            solved = self.ss.solve(2 + dt)
            dt = dt + 2
            if dt == 22:
                break
            if solved:
                self.path.poses = []
                self.pathMap = self.ss.getSolutionPath()
                pt_num = self.pathMap.getStateCount()
                
                TempPose = PoseStamped();
                self.path.header.stamp = rospy.Time.now()
                self.path.header.frame_id = "map";
                end_point = self.pathMap.getState(pt_num - 1)
                plan_flag = not(abs(end_point.getX() - self.goal.x) < 0.05 and 
                    abs(end_point.getY() - self.goal.y) < 0.05 and
                    abs(end_point.getZ() - self.goal.z) < 0.05)
        
                for i in range(pt_num):
                    point = self.pathMap.getState(i)
                    TempPose.pose.position.x = point.getX()
                    TempPose.pose.position.y = point.getY()
                    TempPose.pose.position.z = point.getZ()
                    TempPose.pose.orientation.w = 1.0

                    self.path.poses.append(copy.deepcopy(TempPose));

                # Call function to get the points where UAV must takeoff with
                # UGV and land
                self.getTakeoffAndLandPoints(self.pathMap)

        if plan_flag:
            rospy.loginfo("path plan failed")
            return -1
        else:
            rospy.loginfo("path planned")
            print "Path points: ", len(self.path.poses)
            print "Path length: ", self.pathMap.length()
            return self.pathMap.length()


    def isStateValid(self, state):
        (m, n) = self.gridUAV.shape
        #self.gridUAV[500][100] = 1.0
        #print self.gridUAV[500][100]
        #misc.imshow(self.gridUAV)
        #x_low = 0; x_high = 3.0; # Vertical axis
        #y_low = 0; y_high = 4.0; # Horizontal axis
        x_low = -1.5; x_high = 1.5; # Vertical axis
        y_low = -2; y_high = 2; # Horizontal axis
        x_offset = int(m/2)-1; y_offset = int(n/2)-1;

        if (state.getX() < x_low) or (state.getX() > x_high):
            return False
        if (state.getY() < y_low) or (state.getY() > y_high):
            return False

        x_index = int(m*state.getX()/(x_high-x_low)+x_offset)
        y_index = int(n*state.getY()/(y_high-y_low)+y_offset)
        treshold = 0.0
        self.ix = x_index; self.iy = y_index;

        if (self.gridUAV[x_index][y_index]>treshold):
            return True;
        else: 
            return False;

    def isStateValidMobile(self, state):
        (m, n) = self.gridUAV.shape
        x_low = -1.5; x_high = 1.5; # Vertical axis
        y_low = -2; y_high = 2; # Horizontal axis
        x_offset = int(m/2)-1; y_offset = int(n/2)-1;

        if (state.getX() < x_low) or (state.getX() > x_high):
            return False
        if (state.getY() < y_low) or (state.getY() > y_high):
            return False

        x_index = int(m*state.getX()/(x_high-x_low)+x_offset)
        y_index = int(n*state.getY()/(y_high-y_low)+y_offset)
        treshold = 0.0
        self.ix = x_index; self.iy = y_index;

        if (self.gridUGV[x_index][y_index]>treshold):
            return True;
        else: 
            return False;

    def getTakeoffAndLandPoints(self, fmap):
        pathLength = self.pathMap.getStateCount()

        sampleStep = 0.1
        sampleOffset = 2 # 20cm
        sampledPath = []
        space = ob.SE3StateSpace()
        tempState = ob.State(space)
        tempState().setXYZ(0,0,0)
        tempState().rotation().setAxisAngle(0, 0, 0, 1)
        Duk = 0
        tempVector = Point()
        for i in range(pathLength-1):
            # We will sample path with sampleStep and search for points that
            # are hitting the obstacle. After first obstacle hit we will take
            # a point that is behind the first hitpoint for sampleOffset
            # points and use that as takeoff point. After that we sample again
            # and find the first point after obstacle. We then sample a bit
            # further for sampleOffset-1 and take that point as landing.
            point1 = fmap.getState(i)
            point2 = fmap.getState(i+1)
            #m = (point2.getY() - point1.getY())/(point2.getX() - point1.getX())
            # Get distance in x and y direction as well as overall distance
            Dx = point2.getX()-point1.getX()
            Dy = point2.getY()-point1.getY()
            D = sqrt(Dx*Dx + Dy*Dy)
            Duk = Duk + D
            
            # Determine number of points to be sampled. Floor is here 
            n = int(floor(D/sampleStep))
            if n>0:
                dx = Dx/n
                dy = Dy/n

                for j in range(n):
                    tempState().setXYZ(point1.getX()+j*dx, point1.getY()+j*dy, 0)
                    tempVector.x = tempState().getX()
                    tempVector.y = tempState().getY()
                    tempVector.z = 0
                    sampledPath.append(copy.deepcopy(tempVector))

        tempState = fmap.getState(pathLength-1)
        tempVector.x = tempState.getX()
        tempVector.y = tempState.getY()
        tempVector.z = 0
        sampledPath.append(copy.deepcopy(tempVector))
        takeoffPoints = []
        landPoints = []
        inObstacleFlag = False

        for i in range(len(sampledPath)):
            # create state first
            tempState.setXYZ(sampledPath[i].x, sampledPath[i].y, 0)
            isValidFlag = self.isStateValidMobile(tempState)

            if isValidFlag==True and inObstacleFlag==False:
                # Nothing to do, keep searching
                pass

            elif isValidFlag==False and inObstacleFlag==False:
                # We hit an obstacle for the first time
                inObstacleFlag=True
                # Take a point that is sampleOffset points from current
                a = i-sampleOffset
                if a<0:
                    a=0
                takeoffPoints.append(copy.deepcopy(sampledPath[a]))

            elif isValidFlag==False and inObstacleFlag==True:
                # We are in obstacle and there is no sign of getting
                # out. Keep going forward.
                pass

            elif isValidFlag==True and inObstacleFlag==True:
                # We got out of obstacle, what an amazing time to be alive
                inObstacleFlag = False
                a = i+1
                if a>(len(sampledPath)-1):
                    a=len(sampledPath)-1
                landPoints.append(copy.deepcopy(sampledPath[a]))

        tempTakeoffLandPoints = []

        for i in range(len(takeoffPoints)):
            tempTakeoffLandPoints.append(copy.deepcopy(takeoffPoints[i]))
            tempTakeoffLandPoints.append(copy.deepcopy(landPoints[i]))

        self.visualizeMarkerArray(tempTakeoffLandPoints)


    def visualizeMarkerArray(self, path):
        marker = Marker()

        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = "map"
        marker.id = 1
        marker.ns = "markeri"
        marker.type = 7 #Sphere list
        marker.action = 0
        marker.pose.orientation.w = 1
        marker.scale = Vector3(0.05, 0.05, 0.05)
        tempColorRGBA = ColorRGBA(0,0,1,1) # Blue
        marker.lifetime = rospy.Duration(0)
        #marker.points = copy.deepcopy(path)
        for i in range(len(path)):
            marker.points.append(path[i])
            marker.points[i].z = 0.02
            marker.colors.append(tempColorRGBA)

        #print len(marker.points), len(marker.colors)

        self.markerPub.publish(marker)



if __name__ == '__main__':
    rospy.init_node('path_planner', anonymous=True)
    path_planner = PathPlanner()
    path_planner.run()