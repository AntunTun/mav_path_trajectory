/******************************************************************************
File name: trajectoryPlanning.cpp
Description: Functions for planning a trajectory for arducopter
Author: Marko Car, Antun Ivanovic
******************************************************************************/

#include "mav_path_trajectory/trajectoryPlanning2.h"

trajectoryPlanning::trajectoryPlanning()
{
	// Initialize ros node handle for move group, it is a private node handle
	nhParams = ros::NodeHandle("~");
	nhParams.param("max_speed", maxSpeed, double(2.0));
	nhParams.param("max_acceleration", maxAcc, double(0.8));
	nhParams.param("max_yaw_speed", maxYawSpeed, double(1.5));
	nhParams.param("max_yaw_acc", maxYawAcc, double(20.0));
	nhParams.param("trajectory_sampling_frequency", trajectorySamplingFrequency,
		int(100));
	nhParams.param("acc_scaler_z", trajectoryAccScalerZ, double(1.0));
	nhParams.param("speed_scaler_z", trajectorySpeedScalerZ, double(1.0));

	// Publishing sampled trajectory to trajectory_unprocessed topic. Later the
	// mission planner decides whether to use acceleration or not. Acceleration
	// is provided to mission planner anyways
	trajectorySampledPub = nhTopics.advertise<mav_path_trajectory::TrajectorySampled>(
		"trajectory_unprocessed", 1);
	// Waypoints are recieved through callback
	waypointSub = nhTopics.subscribe("waypoints", 1,
		&trajectoryPlanning::waypointCallback, this);
	cout << "Trajectory acceleration scale z = " << trajectoryAccScalerZ << endl;
	cout << "Trajectory speed scale z = " << trajectorySpeedScalerZ << endl;
}

trajectoryPlanning::~trajectoryPlanning()
{

}

void trajectoryPlanning::run()
{
	ros::spin();
}

mav_path_trajectory::TrajectorySampled trajectoryPlanning::plan(mav_path_trajectory::WaypointArray wp)
{
	cout << "Planning trajectory" << endl;

	// If there are only two waypoints insert midpoint
	if(wp.waypoints.size() == 2)
	{
		geometry_msgs::Pose tempPose;
		tempPose.position.x = (wp.waypoints[0].position.x + 
			wp.waypoints[1].position.x)/2.0;
		tempPose.position.y = (wp.waypoints[0].position.y + 
			wp.waypoints[1].position.y)/2.0;
		tempPose.position.z = (wp.waypoints[0].position.z + 
			wp.waypoints[1].position.z)/2.0;
		tempPose.orientation.z = (wp.waypoints[0].orientation.z + 
			wp.waypoints[1].orientation.z)/2.0;
		wp.waypoints.insert(wp.waypoints.begin()+1, tempPose);
		//for (int i=0; i<waypointArray.waypoints.size(); i++)
		//{
		//	cout << waypointArray.waypoints[i];
		//}
	}

	TrajectoryDerivativeConditions conditions;
	DerivativeConditions tempCond;
	tempCond.speed = 0.0;
	tempCond.acceleration = 0.0;
	tempCond.jerk = 0.0;
	tempCond.split = 0.0;
	conditions.x.start = tempCond;
	conditions.x.end = tempCond;
	conditions.y.start = tempCond;
	conditions.y.end = tempCond;
	conditions.z.start = tempCond;
	conditions.z.end = tempCond;
	conditions.yaw.start = tempCond;
	conditions.yaw.end = tempCond;

	//return optimizedPlan(wp);
	int temp;
	mav_path_trajectory::TrajectorySampled returnTrajectory;
	returnTrajectory = hoCook56(wp, conditions, temp);
	// Multiply z speed and acceleration with scaler
	for(int i=0; i<returnTrajectory.acceleration.size(); i++)
	{
		returnTrajectory.speed[i].z *= trajectorySpeedScalerZ;
		returnTrajectory.acceleration[i].z *= trajectoryAccScalerZ;
	}
	return returnTrajectory;
	cout << "scalingSegmentID: " << temp << endl;
}

void trajectoryPlanning::setTrajectorySamplingFrequency(int freq)
{
	trajectorySamplingFrequency = freq;
}

void trajectoryPlanning::setArducopterMaxSpeed(double speed)
{
	maxSpeed = speed;
}

void trajectoryPlanning::setArducopterMaxAcc(double acc)
{
	maxAcc = acc;
}

// Functions for calculating spline coefficients for polynomial orders
// 3, 4, 5, 6. There are two functions for polynomial coefficients with
// even order of polynomial because a condition can be set either at the 
// beginning or at the end of the polynomial.
void trajectoryPlanning::getSplineCoefficientsOrder3(
	double X0, double Xf, double V0, double Vf, double Tf, 
	std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a3*t^3 + a2*t^2 + a1*t + a0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 4)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = -(3.0*X0 - 3.0*Xf + 2.0*Tf*V0 + Tf*Vf)/Tf2;
		coefficients[3] = (2*X0 - 2*Xf + Tf*V0 + Tf*Vf)/Tf3;
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder4AccStart(
	double X0, double Xf, double V0, double Vf, double A0, double Tf, 
	std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a4*t^4 + a3*t^3 + a2*t^2 + a1*t + a0
	// Acceleration to compute coefficients is given at the start x''(0)=A0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 5)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = -(4*X0 - 4*Xf + 3*Tf*V0 + Tf*Vf + A0*Tf2)/Tf3;
		coefficients[4] = (6*X0 - 6*Xf + 4*Tf*V0 + 2*Tf*Vf + A0*Tf2)/(2*Tf4);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder4AccEnd(
	double X0, double Xf, double V0, double Vf, double Af, double Tf, 
	std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a4*t^4 + a3*t^3 + a2*t^2 + a1*t + a0
	// Acceleration to compute coefficients is given at the start x''(Tf)=Af
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 5)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = -(12*X0 - 12*Xf + 6*Tf*V0 + 6*Tf*Vf - Af*Tf2)/(2*Tf2);
		coefficients[3] = (8*X0 - 8*Xf + 3*Tf*V0 + 5*Tf*Vf - Af*Tf2)/Tf3;
		coefficients[4] = -(6*X0 - 6*Xf + 2*Tf*V0 + 4*Tf*Vf - Af*Tf2)/(2*Tf4);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder5(
	double X0, double Xf, double V0, double Vf, double A0, double Af, 
	double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on fifth order polynomial:
	// x(t) = a5*t^5 + a4*t^4 + a3*t^3 + a2*t^2 + a1*t + a0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;

	// Calculating coefficients based on equations computed earlier
	coefficients[0] = X0;
	coefficients[1] = V0;
	coefficients[2] = A0/2;
	coefficients[3] = -(20*X0 - 20*Xf + 12*Tf*V0 + 8*Tf*Vf + 3*A0*Tf2 - Af*Tf2)
		/(2*Tf3);
	coefficients[4] = (30*X0 - 30*Xf + 16*Tf*V0 + 14*Tf*Vf + 3*A0*Tf2 - 2*Af*Tf2)
		/(2*Tf4);
	coefficients[5] = -(12*X0 - 12*Xf + 6*Tf*V0 + 6*Tf*Vf + A0*Tf2 - Af*Tf2)
		/(2*Tf5);
}

void trajectoryPlanning::getSplineCoefficientsOrder6JerkStart(
	double X0, double Xf, double V0, double Vf, double A0, double Af, 
	double J0, double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a0 + a1t + a2t^2 + a3t^3 + a4t^4 + a5t^5 + a6t^6
	// Acceleration to compute coefficients is given at the start x'''(0)=J0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5, Tf6;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;
	Tf6 = Tf5*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 7)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = J0/6;
		coefficients[4] = -(30*X0 - 30*Xf + 20*V0*Tf + 10*Vf*Tf + 6*A0*Tf2 
			- Af*Tf2 + J0*Tf3)/(2*Tf4);
		coefficients[5] = (48*X0 - 48*Xf + 30*V0*Tf + 18*Vf*Tf + 
			8*A0*Tf2 - 2*Af*Tf2 + J0*Tf3)/(2*Tf5);
		coefficients[6] = -(60*X0 - 60*Xf + 36*V0*Tf + 24*Vf*Tf + 
			9*A0*Tf2 - 3*Af*Tf2 + J0*Tf3)/(6*Tf6);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder6JerkEnd(
	double X0, double Xf, double V0, double Vf, double A0, double Af, 
	double Jf, double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a0 + a1t + a2t^2 + a3t^3 + a4t^4 + a5t^5 + a6t^6
	// Acceleration to compute coefficients is given at the start x'''(Tf)=Jf
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5, Tf6;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;
	Tf6 = Tf5*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 7)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = -(120*X0 - 120*Xf + 60*V0*Tf + 60*Vf*Tf + 
			12*A0*Tf2 - 12*Af*Tf2 + Jf*Tf3)/(6*Tf3);
		coefficients[4] = (90*X0 - 90*Xf + 40*V0*Tf + 50*Vf*Tf + 
			6*A0*Tf2 - 11*Af*Tf2 + Jf*Tf3)/(2*Tf4);
		coefficients[5] = -(72*X0 - 72*Xf + 30*V0*Tf + 42*Vf*Tf + 
			4*A0*Tf2 - 10*Af*Tf2 + Jf*Tf3)/(2*Tf5);
		coefficients[6] = (60*X0 - 60*Xf + 24*V0*Tf + 36*Vf*Tf + 
			3*A0*Tf2 - 9*Af*Tf2 + Jf*Tf3)/(6*Tf6);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder7JerkSplitStart(
	double X0, double Xf, double V0, double Vf, double A0, double Af,
	double J0, double S0, double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a0 + a1t + a2t^2 + a3t^3 + a4t^4 + a5t^5 + a6t^6 + a7t^7
	// Jerg and split to compute coefficients are given at the start x'''(0)=J0
	// x''''(0) = S0
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5, Tf6, Tf7;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;
	Tf6 = Tf5*Tf;
	Tf7 = Tf6*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 8)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = J0/6;
		coefficients[4] = S0/24;
		coefficients[5] = -(168*X0 - 168*Xf + 120*Tf*V0 + 48*Tf*Vf + 
			40*A0*Tf2 - 4*Af*Tf2 + 8*J0*Tf3 + S0*Tf4)/(8*Tf5);
		coefficients[6] = (840*X0 - 840*Xf + 576*Tf*V0 + 264*Tf*Vf + 
			180*A0*Tf2 - 24*Af*Tf2 + 32*J0*Tf3 + 3*S0*Tf4)/(24*Tf6);
		coefficients[7] = -(360*X0 - 360*Xf + 240*Tf*V0 + 120*Tf*Vf + 
			72*A0*Tf2 - 12*Af*Tf2 + 12*J0*Tf3 + S0*Tf4)/(24*Tf7);
	}
}

void trajectoryPlanning::getSplineCoefficientsOrder7JerkSplitEnd(
	double X0, double Xf, double V0, double Vf, double A0, double Af,
	double Jf, double Sf, double Tf, std::vector<double> &coefficients)
{
	// Coefficients are based on third order polynomial:
	// x(t) = a0 + a1t + a2t^2 + a3t^3 + a4t^4 + a5t^5 + a6t^6 + a7t^7
	// Jerg and split to compute coefficients are given at the end x'''(Tf)=Jf
	// x''''(Tf) = Sf
	// Input arguments are determining initial and final conditions

	double Tf2, Tf3, Tf4, Tf5, Tf6, Tf7;
	Tf2 = Tf*Tf;
	Tf3 = Tf2*Tf;
	Tf4 = Tf3*Tf;
	Tf5 = Tf4*Tf;
	Tf6 = Tf5*Tf;
	Tf7 = Tf6*Tf;

	// Calculating coefficients based on equations computed earlier
	if(coefficients.size() == 8)
	{
		coefficients[0] = X0;
		coefficients[1] = V0;
		coefficients[2] = A0/2;
		coefficients[3] = -(840*X0 - 840*Xf + 360*Tf*V0 + 480*Tf*Vf + 
			60*A0*Tf2 - 120*Af*Tf2 + 16*Jf*Tf3 - Sf*Tf4)/(24*Tf3);
		coefficients[4] = (630*X0 - 630*Xf + 240*Tf*V0 + 390*Tf*Vf + 
			30*A0*Tf2 - 105*Af*Tf2 + 15*Jf*Tf3 - Sf*Tf4)/(6*Tf4);
		coefficients[5] = -(504*X0 - 504*Xf + 180*Tf*V0 + 324*Tf*Vf + 
			20*A0*Tf2 - 92*Af*Tf2 + 14*Jf*Tf3 - Sf*Tf4)/(4*Tf5);
		coefficients[6] = (420*X0 - 420*Xf + 144*Tf*V0 + 276*Tf*Vf + 
			15*A0*Tf2 - 81*Af*Tf2 + 13*Jf*Tf3 - Sf*Tf4)/(6*Tf6);
		coefficients[7] = -(360*X0 - 360*Xf + 120*Tf*V0 + 240*Tf*Vf + 
			12*A0*Tf2 - 72*Af*Tf2 + 12*Jf*Tf3 - Sf*Tf4)/(24*Tf7);
	}
}


// Following set of functions calculates polynomial values at specific point
// in time. These are used for sampling position, speed and acceleration.
void trajectoryPlanning::calculatePolynomialValueOrder3(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 6*B[3]*t1 + 2*B[2];
}

void trajectoryPlanning::calculatePolynomialValueOrder4(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[4]*t4 + B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 4*B[4]*t3 + 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 12*B[4]*t2 + 6*B[3]*t1 + 2*B[2];
}

void trajectoryPlanning::calculatePolynomialValueOrder5(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration, double &jerk, double &split)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;
	double t5 = t4*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[5]*t5 + B[4]*t4 + B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 5*B[5]*t4 + 4*B[4]*t3 + 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 20*B[5]*t3 + 12*B[4]*t2 + 6*B[3]*t1 + 2*B[2];

	// Jerk is third derivative, again we lose one coefficient
	jerk = 60*B[5]*t2 + 24*B[4]*t1 + 6*B[3];

	// Split is fourth derivative
	split = 120*B[5]*t1 + 24*B[4];
}

void trajectoryPlanning::calculatePolynomialValueOrder6(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration, double &jerk, double &split)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;
	double t5 = t4*t;
	double t6 = t5*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[6]*t6 + B[5]*t5 + B[4]*t4 + B[3]*t3 + B[2]*t2 + B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 6*B[6]*t5 + 5*B[5]*t4 + 4*B[4]*t3 + 3*B[3]*t2 + 2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 30*B[6]*t4 + 20*B[5]*t3 + 12*B[4]*t2 + 6*B[3]*t1 + 2*B[2];

	// Jerk is third derivative
	jerk = 120*B[6]*t3 + 60*B[5]*t2 + 24*B[4]*t1 + 6*B[3];

	// Split is fourth derivative
	split = 360*B[6]*t2 + 120*B[5]*t1 + 24*B[4];
}

void trajectoryPlanning::calculatePolynomialValueOrder7(
	std::vector<double> B, double t, double &position, double &speed, 
	double &acceleration, double &jerk, double &split)
{
	// Initialize return value and powers of time to make computation easier
	// to read
	double t1 = t;
	double t2 = t1*t;
	double t3 = t2*t;
	double t4 = t3*t;
	double t5 = t4*t;
	double t6 = t5*t;
	double t7 = t6*t;

	// Calculate position based on time and coefficients given as arguments
	position = B[7]*t7 + B[6]*t6 + B[5]*t5 + B[4]*t4 + B[3]*t3 + B[2]*t2 + 
		B[1]*t1 + B[0];

	// Speed is derivative of position so B[0] isn't here
	speed = 7*B[7]*t6 + 6*B[6]*t5 + 5*B[5]*t4 + 4*B[4]*t3 + 3*B[3]*t2 + 
		2*B[2]*t1 + B[1];

	// Acceleration is second derivative of position so B[0] and B[1] are
	// not here
	acceleration = 42*B[7]*t5 + 30*B[6]*t4 + 20*B[5]*t3 + 12*B[4]*t2 + 
		6*B[3]*t1 + 2*B[2];

	// Jerk is third derivative
	jerk = 210*B[7]*t4 + 120*B[6]*t3 + 60*B[5]*t2 + 24*B[4]*t1 + 6*B[3];

	// Split is fourth derivative
	split = 840*B[7]*t3 + 360*B[6]*t2 + 120*B[5]*t1 + 24*B[4];
}



// Set of functions to get max values in trajectory for scaling the whole
// trajectory to predefined maxSpeed and maxAcc
double trajectoryPlanning::getMaxTrajectorySpeed(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectorySpeed = -1.0;
	double currentSpeed = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.speed.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectorySpeed = sqrt(pow(trajectory.speed[0].x, 2) + 
			pow(trajectory.speed[0].y, 2) + pow(trajectory.speed[0].z, 2));
		currentSpeed = maxTrajectorySpeed;
		index = 0;

		// Looping through trajectory in search for max
		for(int i=1; i<trajectory.speed.size(); i++)
		{	
			// Calculation for current speed in trajectory
			currentSpeed = sqrt(pow(trajectory.speed[i].x, 2) + 
				pow(trajectory.speed[i].y, 2) + pow(trajectory.speed[i].z, 2));

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectorySpeed < currentSpeed)
			{
				maxTrajectorySpeed = currentSpeed;
				index = i;
			}
		}
	}
	// Returning the max value
	return maxTrajectorySpeed;
}

double trajectoryPlanning::getMaxTrajectoryYawSpeed(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectoryYawSpeed = -1.0;
	double currentSpeed = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.yawSpeed.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectoryYawSpeed = abs(trajectory.yawSpeed[0]);
		currentSpeed = maxTrajectoryYawSpeed;
		index = 0;

		// Looping through trajectory in search for max
		for(int i=1; i<trajectory.yawSpeed.size(); i++)
		{	
			// Calculation for current speed in trajectory
			currentSpeed = abs(trajectory.yawSpeed[i]);

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectoryYawSpeed < currentSpeed)
			{
				maxTrajectoryYawSpeed = currentSpeed;
				index = i;
			}
		}
	}
	// Returning the max value
	return maxTrajectoryYawSpeed;
}

double trajectoryPlanning::getMaxTrajectoryYawAcc(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectoryYawAcc = -1.0;
	double currentAcc = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.yawAcc.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectoryYawAcc = abs(trajectory.yawAcc[0]);
		currentAcc = maxTrajectoryYawAcc;
		index = 0;

		// Looping through trajectory in search for max
		for(int i=1; i<trajectory.yawAcc.size(); i++)
		{	
			// Calculation for current speed in trajectory
			currentAcc = abs(trajectory.yawAcc[i]);

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectoryYawAcc < currentAcc)
			{
				maxTrajectoryYawAcc = currentAcc;
				index = i;
			}
		}
	}
	// Returning the max value
	return maxTrajectoryYawAcc;
}

double trajectoryPlanning::getMaxTrajectoryAcceleration(
	mav_path_trajectory::TrajectorySampled trajectory, int &index)
{
	// As in speed function here is the same error code, if maxAcc and index
	// are -1 the trajectory is empty
	double maxTrajectoryAcc = -1.0;
	double currentAcc = 0;
	index = -1;

	// If trajectory has any elements we can loop through them and look for 
	// max acceleration
	if (trajectory.acceleration.size() > 0)
	{
		// First point of trajectory defines initial value of max acceleration. 
		// At this point current acceleration is the same.
		// Acceleration is calculated as squared root of x, y and z components
		// squared. This is the total acceleration of quadcopter.
		maxTrajectoryAcc = sqrt(pow(trajectory.acceleration[0].x, 2) + 
			pow(trajectory.acceleration[0].y, 2) + 
			pow(trajectory.acceleration[0].z, 2));
		currentAcc = maxTrajectoryAcc;
		index = 0;

		// Looping through trajectory in search for max acceleration
		for(int i=1; i<trajectory.acceleration.size(); i++)
		{
			// Calculation for current acceleration in trajectory
			currentAcc = sqrt(pow(trajectory.acceleration[i].x, 2) + 
				pow(trajectory.acceleration[i].y, 2) + 
				pow(trajectory.acceleration[i].z, 2));
			
			// If currentAcc becomes greater than maxTrajectoryAcc it becomes
			// maxTrajectoryAcc
			if(maxTrajectoryAcc < currentAcc)
			{
				maxTrajectoryAcc = currentAcc;
				index = i;
			}
		}
	}

	// Returning maxTrajectoryAcc
	return maxTrajectoryAcc;
}


// Trajectory sampling functions. Samples the trajectory with given frequency.
// Also there is a function to clear all vectors in trajectory and a function
// to add zero speed and acceleration at the end to ensure that trajectory
// will end with zeros.
void trajectoryPlanning::sampleMultipleWaypointTrajectory(
	std::vector< std::vector<double> > coefficientsX, 
	std::vector< std::vector<double> > coefficientsY, 
	std::vector< std::vector<double> > coefficientsZ, 
	std::vector< std::vector<double> > coefficientsYAW, std::vector<double> t, 
	mav_path_trajectory::TrajectorySampled &trajectory, int sampleFrequency, 
	std::vector<int> &endIndexes)
{
	endIndexes.clear();
	geometry_msgs::Vector3 posVec, speedVec, accVec, jerkVec, splitVec;
	double pos=0.0, speed=0.0, acc=0.0, jerk=0.0, split=0.0;
	double yawPos=0.0, yawSpeed=0.0, yawAcc=0.0, yawJerk=0.0, yawSplit=0.0;

	// First let's determine sample time from given sample frequency
	double sampleTime = 1.0/double(sampleFrequency);

	for(int i=0; i<t.size(); i++)
	{
		// Okay, we go through all the polynoms and sample the trajectory
		double currentTime = 0.0;

		// Sampling one polynom at the time and pushing it in trajectory
		while(currentTime <= t[i]-sampleTime*0.5)
		{
			// X axis
			pos=0.0; speed=0.0; acc=0.0;
			if (coefficientsX[i].size() == 8)
			{
				// Order 7 polynomial appears
				calculatePolynomialValueOrder7(coefficientsX[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if (coefficientsX[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsX[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if(coefficientsX[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsX[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if(coefficientsX[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsX[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsX[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsX[i], currentTime, 
					pos, speed, acc);
			}
			// setting position, speed and acc in vector3
			posVec.x = pos;
			speedVec.x = speed;
			accVec.x = acc;
			jerkVec.x = jerk;
			splitVec.x = split;


			// Y axis
			pos=0.0; speed=0.0; acc=0.0;
			if (coefficientsY[i].size() == 8)
			{
				// Order 7 polynomial appears
				calculatePolynomialValueOrder7(coefficientsY[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if (coefficientsY[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsY[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if(coefficientsY[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsY[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if(coefficientsY[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsY[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsY[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsY[i], currentTime, 
					pos, speed, acc);
			}
			// setting position, speed and acc in vector3
			posVec.y = pos;
			speedVec.y = speed;
			accVec.y = acc;
			jerkVec.y = jerk;
			splitVec.y = split;


			// Z axis
			pos=0.0; speed=0.0; acc=0.0;
			if (coefficientsZ[i].size() == 8)
			{
				// Order 7 polynomial appears
				calculatePolynomialValueOrder7(coefficientsZ[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if (coefficientsZ[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsZ[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if(coefficientsZ[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsZ[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if(coefficientsZ[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsZ[i], currentTime, 
					pos, speed, acc);
			}
			else if(coefficientsZ[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsZ[i], currentTime, 
					pos, speed, acc);
			}
			// setting position, speed and acc in vector3
			posVec.z = pos;
			speedVec.z = speed;
			accVec.z = acc;
			jerkVec.z = jerk;
			splitVec.z = split;

			// YAW axis
			yawPos=0.0; yawSpeed=0.0; yawAcc=0.0;
			if (coefficientsYAW[i].size() == 8)
			{
				// Order 7 polynomial appears
				calculatePolynomialValueOrder7(coefficientsYAW[i], currentTime, 
					pos, speed, acc, jerk, split);
			}
			else if (coefficientsYAW[i].size() == 7)
			{
				// Order 6 polynomial appears
				calculatePolynomialValueOrder6(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc, jerk, split);
			}
			else if(coefficientsYAW[i].size() == 6)
			{
				// Order 5 polynomial appears
				calculatePolynomialValueOrder5(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc, jerk, split);
			}
			else if(coefficientsYAW[i].size() == 5)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder4(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc);
			}
			else if(coefficientsYAW[i].size() == 4)
			{
				// Order 4 polynomial appears
				calculatePolynomialValueOrder3(coefficientsYAW[i], currentTime, 
					yawPos, yawSpeed, yawAcc);
			}
			// setting position, speed and acc in vector3
			//posVec.z = pos;
			//speedVec.z = speed;
			//accVec.z = acc;

			// Now we can push trajectory points into sampled trajectory
			trajectory.position.push_back(posVec);
			trajectory.speed.push_back(speedVec);
			trajectory.acceleration.push_back(accVec);
			trajectory.jerk.push_back(jerkVec);
			trajectory.split.push_back(splitVec);
			trajectory.yawPos.push_back(yawPos);
			trajectory.yawSpeed.push_back(yawSpeed);
			trajectory.yawAcc.push_back(yawAcc);
			trajectory.yawJerk.push_back(yawJerk);
			trajectory.yawSplit.push_back(yawSplit);

			currentTime = currentTime + sampleTime;
		}
		endIndexes.push_back(trajectory.position.size());
	}

	// Last point
	pos=0.0; speed=0.0; acc=0.0; jerk=0.0; split=0.0;
	if(coefficientsX[t.size()-1].size() == 8)
	{
		// Order 7 polynomial appears
		calculatePolynomialValueOrder7(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsX[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsX[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsX[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsX[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsX[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	posVec.x = pos;
	speedVec.x = speed;
	accVec.x = acc;
	jerkVec.x = jerk;
	splitVec.x = split;

	pos=0.0; speed=0.0; acc=0.0;
	if(coefficientsY[t.size()-1].size() == 8)
	{
		// Order 7 polynomial appears
		calculatePolynomialValueOrder7(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsY[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsY[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsY[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsY[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsY[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	posVec.y = pos;
	speedVec.y = speed;
	accVec.y = acc;
	jerkVec.y = jerk;
	splitVec.y = split;

	pos=0.0; speed=0.0; acc=0.0;
	if(coefficientsZ[t.size()-1].size() == 8)
	{
		// Order 7 polynomial appears
		calculatePolynomialValueOrder7(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsZ[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsZ[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsZ[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	else if(coefficientsZ[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsZ[t.size()-1], t[t.size()-1], 
			pos, speed, acc);
	}
	posVec.z = pos;
	speedVec.z = speed;
	accVec.z = acc;
	jerkVec.z = jerk;
	splitVec.z = split;

	// YAW
	yawPos=0.0; yawSpeed=0.0; yawAcc=0.0;
	if(coefficientsYAW[t.size()-1].size() == 8)
	{
		// Order 7 polynomial appears
		calculatePolynomialValueOrder7(coefficientsYAW[t.size()-1], t[t.size()-1], 
			pos, speed, acc, jerk, split);
	}
	else if(coefficientsYAW[t.size()-1].size() == 7)
	{
		// Order 6 polynomial appears
		calculatePolynomialValueOrder6(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc, jerk, split);
	}
	else if(coefficientsYAW[t.size()-1].size() == 6)
	{
		// Order 5 polynomial appears
		calculatePolynomialValueOrder5(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc, jerk, split);
	}
	else if(coefficientsYAW[t.size()-1].size() == 5)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder4(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc);
	}
	else if(coefficientsYAW[t.size()-1].size() == 4)
	{
		// Order 4 polynomial appears
		calculatePolynomialValueOrder3(coefficientsYAW[t.size()-1], t[t.size()-1], 
			yawPos, yawSpeed, yawAcc);
	}

	// Now we can push trajectory points into sampled trajectory
	trajectory.position.push_back(posVec);
	trajectory.speed.push_back(speedVec);
	trajectory.acceleration.push_back(accVec);
	trajectory.jerk.push_back(jerkVec);
	trajectory.split.push_back(splitVec);
	trajectory.yawPos.push_back(yawPos);
	trajectory.yawSpeed.push_back(yawSpeed);
	trajectory.yawAcc.push_back(yawAcc);
	trajectory.yawJerk.push_back(yawJerk);
	trajectory.yawSplit.push_back(yawSplit);

	// Adding information about which index belongs to which segmet
	int segmentCount = 1;
	for(int i=0; i<trajectory.position.size(); i++)
	{
		trajectory.segment.push_back(segmentCount);
		if(endIndexes[segmentCount-1] == i)
		{
			segmentCount++;
		}
	}
}

void trajectoryPlanning::clearTrajectorySampled(
	mav_path_trajectory::TrajectorySampled &trajectory)
{
	trajectory.position.clear();
	trajectory.speed.clear();
	trajectory.acceleration.clear();
	trajectory.jerk.clear();
	trajectory.split.clear();
	trajectory.yawPos.clear();
	trajectory.yawSpeed.clear();
	trajectory.yawAcc.clear();
	trajectory.yawJerk.clear();
	trajectory.yawSplit.clear();
	trajectory.segment.clear();
}

void trajectoryPlanning::addZeroSpeedAndAccAtTrajectoryEnd(
	mav_path_trajectory::TrajectorySampled &trajectorySampled)
{
	trajectorySampled.position.push_back(
		trajectorySampled.position[trajectorySampled.position.size()-1]);
	geometry_msgs::Vector3 tempVector;
	tempVector.x = 0.0;
	tempVector.y = 0.0;
	tempVector.z = 0.0;
	trajectorySampled.speed.push_back(tempVector);
	trajectorySampled.acceleration.push_back(tempVector);
	trajectorySampled.jerk.push_back(tempVector);
	trajectorySampled.split.push_back(tempVector);
	trajectorySampled.yawPos.push_back(trajectorySampled.yawPos[trajectorySampled.yawPos.size()-1]);
	trajectorySampled.yawSpeed.push_back(0.0);
	trajectorySampled.yawAcc.push_back(0.0);
	trajectorySampled.yawJerk.push_back(0.0);
	trajectorySampled.yawSplit.push_back(0.0);
}




double trajectoryPlanning::calculateTheoreticalMaxSpeed(
	double V0, double trajectoryLength, double maxAcceleration)
{
	// Theoretical max speed will be achieved at the middle of path
	// because on the second half of path quadcopter needs to decelerate

	return abs(V0) + abs(maxAcceleration*sqrt(1.0*trajectoryLength/maxAcceleration));
}


double trajectoryPlanning::getMaxTrajectorySpeedInterval(
	mav_path_trajectory::TrajectorySampled trajectory, int start, int end)
{
	// Initial max speed and index are -1. This is an error code for empty
	// trajectory
	double maxTrajectorySpeed = -1.0;
	double currentSpeed = 0;

	// If trajectory has any elements we can loop through them and look for 
	// max speed
	if (trajectory.speed.size() > 0)
	{
		// First point of trajectory defines initial value of max speed. 
		// At this point current speed is the same.
		// Speed is calculated as squared root of x, y and z components squared.
		// This is the total speed of quadcopter.
		maxTrajectorySpeed = sqrt(pow(trajectory.speed[start].x, 2) + 
			pow(trajectory.speed[start].y, 2) + pow(trajectory.speed[start].z, 2));
		currentSpeed = maxTrajectorySpeed;

		// Looping through trajectory in search for max
		for(int i=start; i<=end; i++)
		{	
			// Calculation for current speed in trajectory
			currentSpeed = sqrt(pow(trajectory.speed[i].x, 2) + 
				pow(trajectory.speed[i].y, 2) + pow(trajectory.speed[i].z, 2));

			// If maxTrajectorySpeed is smaller than currentSpeed than 
			// currentSpeed becomes max.
			if(maxTrajectorySpeed < currentSpeed)
			{
				maxTrajectorySpeed = currentSpeed;
			}
		}
	}
	// Returning the max value
	return maxTrajectorySpeed;
}

double trajectoryPlanning::getMaxTrajectoryAccelerationInterval(
	mav_path_trajectory::TrajectorySampled trajectory, int start, int end)
{
	// As in speed function here is the same error code, if maxAcc and index
	// are -1 the trajectory is empty
	double maxTrajectoryAcc = -1.0;
	double currentAcc = 0;

	// If trajectory has any elements we can loop through them and look for 
	// max acceleration
	if (trajectory.acceleration.size() > 0)
	{
		// First point of trajectory defines initial value of max acceleration. 
		// At this point current acceleration is the same.
		// Acceleration is calculated as squared root of x, y and z components
		// squared. This is the total acceleration of quadcopter.
		maxTrajectoryAcc = sqrt(pow(trajectory.acceleration[start].x, 2) + 
			pow(trajectory.acceleration[start].y, 2) + 
			pow(trajectory.acceleration[start].z, 2));
		currentAcc = maxTrajectoryAcc;

		// Looping through trajectory in search for max acceleration
		for(int i=start; i<=end; i++)
		{
			// Calculation for current acceleration in trajectory
			currentAcc = sqrt(pow(trajectory.acceleration[i].x, 2) + 
				pow(trajectory.acceleration[i].y, 2) + 
				pow(trajectory.acceleration[i].z, 2));
			
			// If currentAcc becomes greater than maxTrajectoryAcc it becomes
			// maxTrajectoryAcc
			if(maxTrajectoryAcc < currentAcc)
			{
				maxTrajectoryAcc = currentAcc;
			}
		}
	}

	// Returning maxTrajectoryAcc
	return maxTrajectoryAcc;
}


mav_path_trajectory::TrajectorySampled trajectoryPlanning::twoWaypointsTrajectoryOrder5(
	mav_path_trajectory::WaypointArray waypointArray)
{
	mav_path_trajectory::TrajectorySampled trajectorySampledReturn;
	// First check if there is at least two waypoints in waypointArray
	if(waypointArray.waypoints.size() >= 2)
	{
		// We can work if there is two waypoints. In case of multiple waypoints
		// only first two will be considered

		// For easier notation use extra variables
		double xPosStart, xPosEnd, yPosStart, yPosEnd, zPosStart, zPosEnd;
		double yawPosStart, yawPosEnd;

		// Assigning start and end values
		xPosStart = waypointArray.waypoints[0].position.x;
		yPosStart = waypointArray.waypoints[0].position.y;
		zPosStart = waypointArray.waypoints[0].position.z;
		yawPosStart = waypointArray.waypoints[0].orientation.z;

		xPosEnd = waypointArray.waypoints[1].position.x;
		yPosEnd = waypointArray.waypoints[1].position.y;
		zPosEnd = waypointArray.waypoints[1].position.z;
		yawPosEnd = waypointArray.waypoints[1].orientation.z;

		// Estimate time needed to complete trajectory using euclidian distance
		// that quadcopter needs to travel and max speed provided
		double tempDistance, trajectoryTime, tempYawDistance;
		tempDistance = sqrt(pow(xPosEnd - xPosStart, 2) + 
			pow(yPosEnd - yPosStart, 2) + pow(zPosEnd - zPosStart, 2));
		tempYawDistance = abs(yawPosEnd - yawPosStart);

		// Maximum speed of quadcopter is defined in parameter maxSpeed. In 
		// between two points there is theoretical limit for maximum speed of
		// quadcopter and it depends on maximum acceleration. Since max speed
		// is used to calculate trajectory time and also to recalculate time
		// it is important to use minimum of maxSpeed and theoreticalMaxSpeed.
		// Also initial speed of quadcopter is needed but here it will be zero
		// because quadcopter is going point to point
		double theoreticalMaxSpeed, theoreticalMaxSpeedYaw;
		theoreticalMaxSpeed = min(maxSpeed, calculateTheoreticalMaxSpeed(0, 
			tempDistance, maxAcc));
		theoreticalMaxSpeedYaw = min(maxYawSpeed, calculateTheoreticalMaxSpeed(0, 
			tempYawDistance, maxYawAcc));

		// trajectoryTime is therefore calculated with realMaxSpeed
		if(tempDistance == 0) theoreticalMaxSpeed = 1;
		if(tempYawDistance == 0) theoreticalMaxSpeedYaw = 1;
		trajectoryTime = max(tempDistance/theoreticalMaxSpeed, 
			tempYawDistance/theoreticalMaxSpeedYaw);
		//cout << trajectoryTime << endl;

		// Let's initialize everythin needed for trajectory generation
		std::vector<double> xCoeffs, yCoeffs, zCoeffs, yawCoeffs;
		for (int i=0; i<=5; i++)
		{
			xCoeffs.push_back(0);
			yCoeffs.push_back(0);
			zCoeffs.push_back(0);
			yawCoeffs.push_back(0);
		}

		// Now let's calculate spline coefficients for all degrees of freedom
		getSplineCoefficientsOrder5(xPosStart, xPosEnd, 0, 0, 0, 0, 
			trajectoryTime, xCoeffs);
		getSplineCoefficientsOrder5(yPosStart, yPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yCoeffs);
		getSplineCoefficientsOrder5(zPosStart, zPosEnd, 0, 0, 0, 0, 
			trajectoryTime, zCoeffs);
		getSplineCoefficientsOrder5(yawPosStart, yawPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yawCoeffs);

		// create vectors of vectors for trajectory to be able to use function
		std::vector< std::vector<double> > coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW;
		coefficientsX.push_back(xCoeffs);
		coefficientsY.push_back(yCoeffs);
		coefficientsZ.push_back(zCoeffs);
		coefficientsYAW.push_back(yawCoeffs);

		// Sample trajectory to check for max speed, and maybe later for max acc
		mav_path_trajectory::TrajectorySampled trajectorySampled;
		std::vector<double> parametricTimes;
		std::vector<int> endIndexes;
		parametricTimes.push_back(trajectoryTime);
		sampleMultipleWaypointTrajectory(coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, endIndexes);

		// Search for max speed to ensure trajectory is valid. If max speed is
		// smaller than maxSpeed parameter at constructor, trajectory is
		// replanned with lower execution time. If it's larger trajectory will
		// be slowed down. Index of max speed is also returned but it's now 
		// needed at this point.
		double trajectoryMaxSpeed, trajectoryMaxAcc;
		double trajectoryMaxYawSpeed, trajectoryMaxYawAcc;
		int tempIndex;
		trajectoryMaxSpeed = getMaxTrajectorySpeed(trajectorySampled, 
			tempIndex);
		trajectoryMaxAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
			tempIndex);
		trajectoryMaxYawSpeed = getMaxTrajectoryYawSpeed(trajectorySampled, 
			tempIndex);
		trajectoryMaxYawAcc = getMaxTrajectoryYawAcc(trajectorySampled, 
			tempIndex);
		double sa, sv, s, saYaw, svYaw;
		sa = sqrt(trajectoryMaxAcc/maxAcc);
		sv = trajectoryMaxSpeed/theoreticalMaxSpeed;
		saYaw = sqrt(trajectoryMaxYawAcc/maxYawAcc);
		svYaw = trajectoryMaxYawSpeed/theoreticalMaxSpeedYaw;
		s = max(max(max(sa, sv), saYaw), svYaw);

		// Recalculate trajectory time based on max speed achieved in initial
		// plan.
		trajectoryTime = trajectoryTime*s;

		// Recreate trajectory with new time
		// Calculate spline coefficients for all degrees of freedom
		getSplineCoefficientsOrder5(xPosStart, xPosEnd, 0, 0, 0, 0, 
			trajectoryTime, xCoeffs);
		getSplineCoefficientsOrder5(yPosStart, yPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yCoeffs);
		getSplineCoefficientsOrder5(zPosStart, zPosEnd, 0, 0, 0, 0, 
			trajectoryTime, zCoeffs);
		getSplineCoefficientsOrder5(yawPosStart, yawPosEnd, 0, 0, 0, 0, 
			trajectoryTime, yawCoeffs);

		// Clear trajectory sampled to ensure empty variable is sent to function
		clearTrajectorySampled(trajectorySampled);

		coefficientsX.clear();
		coefficientsY.clear();
		coefficientsZ.clear();
		coefficientsYAW.clear();
		coefficientsX.push_back(xCoeffs);
		coefficientsY.push_back(yCoeffs);
		coefficientsZ.push_back(zCoeffs);
		coefficientsYAW.push_back(yawCoeffs);
		parametricTimes[0] = trajectoryTime;
		// Resample trajectory
		sampleMultipleWaypointTrajectory(coefficientsX, coefficientsY, 
			coefficientsZ, coefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, endIndexes);
		for(int i=0; i<trajectorySampled.position.size(); i++)
		{
			trajectorySampled.segment.push_back(1);
		}

		// In the end publish trajectory. The mission control node will make
		// sure that trajectory is not planned until previous is finished.
		addZeroSpeedAndAccAtTrajectoryEnd(trajectorySampled);
		trajectorySampledReturn = trajectorySampled;
		trajectorySampledPub.publish(trajectorySampled);

		// !!! IMPORTANT !!!
		// It would be good to have a service that checks with control node if
		// current trajectory is executed, and when it's executed publish
		// a new one. This has to be considered further
	}

	return trajectorySampledReturn;
}


// Ho cook 56 method with 5th and 6th order polynomials
mav_path_trajectory::TrajectorySampled trajectoryPlanning::hoCook56(
	mav_path_trajectory::WaypointArray waypointArray, 
	TrajectoryDerivativeConditions initialConditions, 
	int &scalingSegmentID)
{
	// Precondition yaw. Difference for yaw is current-previous which can
	// be bad if it turns more than 180deg. 
	double yawDiff;
	for (int i = 1; i < waypointArray.waypoints.size(); i++)
	{	
		yawDiff = 	waypointArray.waypoints[i].orientation.z - 
			waypointArray.waypoints[i-1].orientation.z;
		if (abs(yawDiff) > M_PI)
		{
			if (yawDiff < 0.0)
			{
				waypointArray.waypoints[i].orientation.z += 2.0*M_PI;
				for (int j = i + 1; j < waypointArray.waypoints.size(); j++)
				{
					waypointArray.waypoints[j].orientation.z += 2.0*M_PI;
				}
			}
			else
			{
				waypointArray.waypoints[i].orientation.z -= 2.0*M_PI;
				for (int j = i + 1; j < waypointArray.waypoints.size(); j++)
				{
					waypointArray.waypoints[j].orientation.z -= 2.0*M_PI;
				}
			}
		}
	}

	//cout << initialConditions.x.end.jerk << endl;
	mav_path_trajectory::TrajectorySampled trajectorySampledReturn;
	// This function uses Ho-Cook method for spline planning and generates
	// trajectory for quadcopter. In this case polynomial orders are 6 for 
	// first and last segment and 5 for all other segments. Minimul number of
	// waypoints to use this function is 3. If two waypoints are provided
	// trajectory will be generated through two points using 
	// twoWaypointsTrajectoryOrder5 function

	// Check for size
	int m = waypointArray.waypoints.size(); // Number of points in trajectory
	if(m == 2)
	{	
		// If there is not enough points for Ho-Cook method simply compute
		// and publish a trajectory through two points
		//trajectorySampledReturn = twoWaypointsTrajectoryOrder5(waypointArray);
		geometry_msgs::Pose tempPose;
		tempPose.position.x = (waypointArray.waypoints[0].position.x + 
			waypointArray.waypoints[1].position.x)/2.0;
		tempPose.position.y = (waypointArray.waypoints[0].position.y + 
			waypointArray.waypoints[1].position.y)/2.0;
		tempPose.position.z = (waypointArray.waypoints[0].position.z + 
			waypointArray.waypoints[1].position.z)/2.0;
		tempPose.orientation.z = (waypointArray.waypoints[0].orientation.z + 
			waypointArray.waypoints[1].orientation.z)/2.0;
		waypointArray.waypoints.insert(waypointArray.waypoints.begin()+1, tempPose);
		//for (int i=0; i<waypointArray.waypoints.size(); i++)
		//{
		//	cout << waypointArray.waypoints[i];
		//}
	}

	m = waypointArray.waypoints.size();
	if(m >= 3)
	{
		// Now we can interpolate using 4 and 5 order polynoms.

		// First of all we can compute parametrical times for the trajectory 
		// segments. It is simply euclidian distance divided by preset max speed
		// of quadcopter given through ROS param in constructor.
		std::vector<double> parametricTimes, parametricLengths;
		double tempXstart, tempXend, tempYstart, tempYend, tempZstart, tempZend;
		double tempYawStart, tempYawEnd;
		//cout << "parametricalTimes: " << endl;
		for(int i=2; i<=m; i++)
		{
			// Temporary values just to have clear equation later. Since it is
			// starting at 2 and waypoints are starting from zero. 
			tempXstart = waypointArray.waypoints[i-2].position.x;
			tempXend = waypointArray.waypoints[i-1].position.x;
			tempYstart = waypointArray.waypoints[i-2].position.y;
			tempYend = waypointArray.waypoints[i-1].position.y;
			tempZstart = waypointArray.waypoints[i-2].position.z;
			tempZend = waypointArray.waypoints[i-1].position.z;
			tempYawStart = waypointArray.waypoints[i-2].orientation.z;
			tempYawEnd = waypointArray.waypoints[i-1].orientation.z;

			// Computing parametrical time
			parametricTimes.push_back(max(sqrt(pow(tempXend - tempXstart, 2)
				+ pow(tempYend - tempYstart, 2) 
				+ pow(tempZend - tempZstart, 2))/maxSpeed, 
				abs(tempYawEnd - tempYawStart)/maxYawSpeed));
			parametricLengths.push_back(parametricTimes[i-2]*maxSpeed);
			//cout << parametricTimes[i-2] << endl;
		}

		// Here things get somewhat more complicated than in hoCook34. There
		// are twice as much conditions here because we are continuing splines
		// using speed and acceleration to have jerk free trajectory. Matrix M
		// is no longer (m-2)x(m-2) but [2*(m-2)]x[2*(m-2)] due to twice as
		// much conditions. Therefore matrix M will be divided in 4 matrices 
		// and combined at the end. Also all the members of this matrix will
		// be set to 0 initially.
		Eigen::MatrixXd M(2*(m-2), 2*(m-2));
		// Set all members of matrix to 0. This is actually faster than
		// M << M*0.0 and better since there were sam NAN elements in matrix
		// otherwise
		for(int i=0; i<2*(m-2); i++)
		{
			for(int j=0; j<2*(m-2); j++)
			{
				M(i,j) = 0.0;
			}
		}

		// Now we have matrix M all set, we just have to fill it.
		createMatrixMForHoCook56(M, parametricTimes, m);

		// Now that matrix M is here it's good to create matrices A for all
		// three axis. Additionaly create matrix A for yaw allthough yaw is
		// not used in initial parametric time calculation.
		Eigen::MatrixXd Ax(1, 2*(m-2));
		Eigen::MatrixXd Ay(1, 2*(m-2));
		Eigen::MatrixXd Az(1, 2*(m-2));
		Eigen::MatrixXd Ayaw(1, 2*(m-2));
		// This is messy work so it's done in function. It will be done in one
		// function called 3 times for x, y and z so all waypoints data has to
		// be in a vector. Adding yaw
		std::vector<double> positionsX, positionsY, positionsZ, positionsYAW;
		for(int i=0; i<waypointArray.waypoints.size(); i++)
		{
			positionsX.push_back(waypointArray.waypoints[i].position.x);
			positionsY.push_back(waypointArray.waypoints[i].position.y);
			positionsZ.push_back(waypointArray.waypoints[i].position.z);
			positionsYAW.push_back(waypointArray.waypoints[i].orientation.z);
		}

		// Create matrices
		createMatrixAForHoCook56(Ax, positionsX, parametricTimes, m, initialConditions.x);
		createMatrixAForHoCook56(Ay, positionsY, parametricTimes, m, initialConditions.y);
		createMatrixAForHoCook56(Az, positionsZ, parametricTimes, m, initialConditions.z);
		createMatrixAForHoCook56(Ayaw, positionsYAW, parametricTimes, m, initialConditions.yaw);
		//cout << Az << endl;
		
		// Now we can compute vector of speed and acceleration conditions using
		// matrices A and M. First create new matrix that will be inverse of
		// M so it won't be inverted three times
		Eigen::MatrixXd Minv = M.inverse();

		// Vectors containing conditions
		Eigen::MatrixXd xd, yd, zd, yawd;
		xd = Ax*Minv;
		yd = Ay*Minv;
		zd = Az*Minv;
		yawd = Ayaw*Minv;

		// Okay, here is everything needed for coefficients calculation. 
		// They'll be put in a vector of vectors, first and last segment with
		// 6 coefficients; other segments with 5 coefficients.
		std::vector< std::vector< double > > trajectoryCoefficientsX, 
		trajectoryCoefficientsY, trajectoryCoefficientsZ, trajectoryCoefficientsYAW;
		double tempV7[7] = {0, 0, 0, 0, 0, 0, 0};
		double tempV6[6] = {0, 0, 0, 0, 0, 0};

		// First goes segment one with 7 coefficients
		std::vector<double> tempVector7(&tempV7[0], &tempV7[0]+7);
		trajectoryCoefficientsX.push_back(tempVector7);
		trajectoryCoefficientsY.push_back(tempVector7);
		trajectoryCoefficientsZ.push_back(tempVector7);
		trajectoryCoefficientsYAW.push_back(tempVector7);

		// Now for the segments except last
		for(int i=1; i<m-1-1; i++)
		{
			std::vector<double> tempVector(&tempV6[0], &tempV6[0]+6);
			trajectoryCoefficientsX.push_back(tempVector);
			trajectoryCoefficientsY.push_back(tempVector);
			trajectoryCoefficientsZ.push_back(tempVector);
			trajectoryCoefficientsYAW.push_back(tempVector);
		}

		// Last segment coefficients are also of length 7. This works.
		trajectoryCoefficientsX.push_back(tempVector7);
		trajectoryCoefficientsY.push_back(tempVector7);
		trajectoryCoefficientsZ.push_back(tempVector7);
		trajectoryCoefficientsYAW.push_back(tempVector7);

		// Everything is set for calculation of coefficients of spline segments
		// based on waypoints, calculated conditions and parametric times.
		calculateCoefficientsHoCook56(trajectoryCoefficientsX, xd, positionsX, 
			parametricTimes, initialConditions.x);
		calculateCoefficientsHoCook56(trajectoryCoefficientsY, yd, positionsY, 
			parametricTimes, initialConditions.y);
		calculateCoefficientsHoCook56(trajectoryCoefficientsZ, zd, positionsZ, 
			parametricTimes, initialConditions.z);
		calculateCoefficientsHoCook56(trajectoryCoefficientsYAW, yawd, 
			positionsYAW, parametricTimes, initialConditions.yaw);


		// Now we can sample that trajectory to find out max speed and acceleration
		// Let's first create trajectory
		mav_path_trajectory::TrajectorySampled trajectorySampled;
		std::vector<int> trajectoryEndIndexes;
		sampleMultipleWaypointTrajectory(trajectoryCoefficientsX, 
			trajectoryCoefficientsY, trajectoryCoefficientsZ, 
			trajectoryCoefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, trajectoryEndIndexes);

		// Trajectory is here but speeds and accelerations are not what they are
		// most likely too big. That's why we now enter the second stage of
		// Ho-Cook method in which we find critical acceleration and speed or
		// in other words the biggest by absolute value. That is done using 
		// total speed and acceleration of quadcopter. This part may require
		// more than one iteration, based on sample time given. In the end the
		// scaling factor should be 1, or in our case between 0.99 and 1.01.

		// First implementation goes with only one iteration
		int tempIndexAcc, tempIndexSpeed, tempIndexYawSpeed, tempIndexYawAcc;
		double maxTrajectorySpeed = getMaxTrajectorySpeed(trajectorySampled, 
			tempIndexSpeed);
		double maxTrajectoryAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
			tempIndexAcc);
		double maxTrajectoryYawSpeed = getMaxTrajectoryYawSpeed(trajectorySampled, 
			tempIndexYawSpeed);
		double maxTrajectoryYawAcc = getMaxTrajectoryYawAcc(trajectorySampled, 
			tempIndexYawAcc);

		// Speed and acceleration scaling factors
		double sv = maxTrajectorySpeed/maxSpeed;
		double sa = sqrt(maxTrajectoryAcc/maxAcc*1.0);
		double svYaw = maxTrajectoryYawSpeed/maxYawSpeed;
		double saYaw = sqrt(maxTrajectoryYawAcc/maxAcc*1.0);
		double s = max(max(max(sv, sa), svYaw), saYaw);

		// Get the scaling segment index
		//int scalingSegmentID;
		if(sv>sa && sv>svYaw && sv>saYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexSpeed];
		}
		if(sa>sv && sa>svYaw && sa>saYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexAcc];
		}
		if(svYaw>sv && svYaw>sa && svYaw>saYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexYawSpeed];
		}
		if(saYaw>sv && saYaw>sa && saYaw>svYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexYawAcc];
		}

		//cout << sv << " " << sa << " " << svYaw << " " << saYaw << endl;
		//cout << "sa: " << sa << " sv: " << sv << " speed: " 
		//	<< maxTrajectorySpeed << " acc: " << maxTrajectoryAcc << endl;

		// After finding scaling factor we have to scale parametric times
		for(int i=0; i<parametricTimes.size(); i++)
		{
			parametricTimes[i] = parametricTimes[i]*s;
		}
		// Clear the trajectory because we have to create new one with new set
		// of parametric times.
		clearTrajectorySampled(trajectorySampled);

		// At this point parametric times are computed and we can begin
		// forming matrix M. It is the size 2(m-2)x2(m-2) and inverse will be
		// needed later so Eigen seems like pretty logical solution. Initial
		// values are all set to 0
		for(int i=0; i<2*(m-2); i++)
		{
			for(int j=0; j<2*(m-2); j++)
			{
				M(i,j) = 0.0;
			}
		}

		// Now we have matrix M all set, we just have to fill it.
		createMatrixMForHoCook56(M, parametricTimes, m);

		// Initialize all zeros for A matrices
		Ax << Ax*0.0;
		Ay << Ay*0.0;
		Az << Az*0.0;
		Ayaw << Ayaw*0.0;

		// Create matrices
		createMatrixAForHoCook56(Ax, positionsX, parametricTimes, m, initialConditions.x);
		createMatrixAForHoCook56(Ay, positionsY, parametricTimes, m, initialConditions.y);
		createMatrixAForHoCook56(Az, positionsZ, parametricTimes, m, initialConditions.z);
		createMatrixAForHoCook56(Ayaw, positionsYAW, parametricTimes, m, initialConditions.yaw);

		// Now we can compute vector of speed and acceleration conditions 
		// using matrices A and M. First create new matrix that will be 
		// inverse of M so it won't be inverted three times
		Minv = M.inverse();

		// Vectors containing conditions
		xd = Ax*Minv;
		yd = Ay*Minv;
		zd = Az*Minv;
		yawd = Ayaw*Minv;

		// Everything is set for calculation of coefficients of spline 
		// segments based on waypoints, calculated conditions and 
		// parametric times.
		calculateCoefficientsHoCook56(trajectoryCoefficientsX, xd, positionsX, 
			parametricTimes, initialConditions.x);
		calculateCoefficientsHoCook56(trajectoryCoefficientsY, yd, positionsY, 
			parametricTimes, initialConditions.y);
		calculateCoefficientsHoCook56(trajectoryCoefficientsZ, zd, positionsZ, 
			parametricTimes, initialConditions.z);
		calculateCoefficientsHoCook56(trajectoryCoefficientsYAW, yawd, positionsYAW, 
			parametricTimes, initialConditions.yaw);

		// Here we also have everything needed
		sampleMultipleWaypointTrajectory(trajectoryCoefficientsX, 
			trajectoryCoefficientsY, trajectoryCoefficientsZ, 
			trajectoryCoefficientsYAW, parametricTimes, 
			trajectorySampled, trajectorySamplingFrequency, trajectoryEndIndexes);

		// Adding information about which index belongs to which segmet
		/*int segmentCount = 1;
		for(int i=0; i<trajectorySampled.position.size(); i++)
		{
			trajectorySampled.segment.push_back(segmentCount);
			if(trajectoryEndIndexes[segmentCount-1] == i)
			{
				segmentCount++;
			}
		}*/

		// Add zero speed to the end and publish the trajectory.
		//addZeroSpeedAndAccAtTrajectoryEnd(trajectorySampled);
		trajectorySampledReturn = trajectorySampled;
		//trajectorySampledPub.publish(trajectorySampled);
	}

	// Since we modified yaw it is okay if we pass through all yaw values and 
	// wrap them to interval [-PI, PI]
	double currentYaw;
	for (int i = 0; i < trajectorySampledReturn.yawPos.size(); i++)
	{
		currentYaw = trajectorySampledReturn.yawPos[i];
		trajectorySampledReturn.yawPos[i] = atan2(sin(currentYaw), 
			cos(currentYaw));
	}

	return trajectorySampledReturn;
}

void trajectoryPlanning::createMatrixMForHoCook56(
	Eigen::MatrixXd &M, std::vector<double> t, int m)
{
	// First initialize submatrices of matrix M. Matrix M can be represented as
	//     [M1 | M2]
	// M = [-------]
	//     [M3 | M4]
	// Where M1, M2, M3 and M4 submatrices are of dimensions (m-2)x(m-2)
	Eigen::MatrixXd M1(m-2, m-2);
	Eigen::MatrixXd M2(m-2, m-2);
	Eigen::MatrixXd M3(m-2, m-2);
	Eigen::MatrixXd M4(m-2, m-2);
	// And fill them up with zeros
	for(int i=0; i<(m-2); i++)
	{
		for(int j=0; j<(m-2); j++)
		{
			M1(i,j) = 0.0;
			M2(i,j) = 0.0;
			M3(i,j) = 0.0;
			M4(i,j) = 0.0;
		}
	}

	// All matrices will be filled column by column. First and last column are
	// different than all the others so these will be filled separately.
	if(m == 3)
	{
		// In every parametrical time we have correction factor -2 because
		// in reality they start at t2 but indexing of arrays goes from 0
		// so a bit of subtraction is needed. This is special case when
		// there are only two segments. Also it is not relevant whether first
		// or last segment is used since it's only a number. Maybe it is
		// relevant if there is difference in + or - but I hope it's not.
		//M1(0,0) = 20.0*pow(t[3-2], 2) - 12*pow(t[2-2], 2);
		//M2(0,0) = 40.0*pow(t[3-2], 3) + 16*pow(t[2-2], 3);
		//M3(0,0) = -4.0*t[2-2]*pow(t[3-2], 2) - 3.0*pow(t[2-2], 2)*t[3-2];
		//M4(0,0) = -6.0*t[2-2]*pow(t[3-2], 3) + 3.0*pow(t[2-2], 3)*t[3-2];

		// A bit different approach here because we have to continue two 6th
		// order polynomials and coefficients are a bit different
		M1(0,0) = 5.0*(pow(t[3-2], 2) - pow(t[2-2], 2));
		M2(0,0) = 40.0*(pow(t[3-2], 3) + pow(t[2-2], 3));
		M3(0,0) = -t[2-2]*pow(t[3-2], 2) - pow(t[2-2], 2)*t[3-2];
		M4(0,0) = 6.0*(pow(t[2-2], 3)*t[3-2] - t[2-2]*pow(t[3-2], 3));
		//cout << "tu" << endl;

		// CHECKED, working
	}

	if(m >= 4)
	{
		// Filling first and last columns of submatrices
		// CHECKED, works
		// M1:
		M1(0,0) = 20.0*pow(t[3-2], 2) - 12*pow(t[2-2], 2);
		M1(1,0) = -8.0*pow(t[2-2], 2);
		M1(m-3-1, m-2-1) = 8.0*pow(t[m-2], 2);
		M1(m-2-1, m-2-1) = 12.0*pow(t[m-2], 2) - 20.0*pow(t[m-1-2], 2);
		// M2:
		M2(0,0) = 40.0*pow(t[3-2], 3) + 16*pow(t[2-2], 3);
		M2(1,0) = 14.0*pow(t[2-2], 3);
		M2(m-3-1, m-2-1) = 14.0*pow(t[m-2], 3);
		M2(m-2-1, m-2-1) = 16.0*pow(t[m-2], 3) + 40.0*pow(t[m-1-2], 3);
		// M3:
		M3(0,0) = -4.0*t[2-2]*pow(t[3-2], 2) - 3.0*pow(t[2-2], 2)*t[3-2];
		M3(1,0) = pow(t[2-2], 2)*t[3-2];
		M3(m-3-1, m-2-1) = t[m-1-2]*pow(t[m-2], 2);
		M3(m-2-1, m-2-1) = -3.0*t[m-1-2]*pow(t[m-2], 2) - 4.0*pow(t[m-1-2], 2)*t[m-2];
		// M4:
		M4(0,0) = -6.0*t[2-2]*pow(t[3-2], 3) + 3.0*pow(t[2-2], 3)*t[3-2];
		M4(1,0) = -2.0*pow(t[2-2], 3)*t[3-2];
		M4(m-3-1, m-2-1) = 2*t[m-1-2]*pow(t[m-2], 3);
		M4(m-2-1, m-2-1) = -3.0*t[m-1-2]*pow(t[m-2], 3) + 6.0*pow(t[m-1-2], 3)*t[m-2];

		// Now we can start filling the inner parts of matrices
		for(int col=1; col <= (m-3)-1; col++)
		{
			//cout << "M >=4, matrix M" << endl;
			// When filling inner columns row starts at col-1 as we look at
			// the matrix. Three rows are to be filled so we increment row
			// two times. Also that means the matrix will have elements only
			// on the main diagonal and it's subdiagonals.
			int row = col-1;
			// Also there is a rule to use parametrical times with column as
			// index. There is -2 subtraction on every place in indexing and
			// that's because parametrical times start at index 0. Example:
			// t2 = t[2-2] => t2 = t[0]

			// All four matrices are very similar, with main diagonal and one 
			// or two subdiagonals so we can use same indexes
			M1(row, col) = 8.0*pow(t[col+3-2], 2);
			M1(row+1, col) = 12.0*(pow(t[col+3-2], 2) - pow(t[col+2-2], 2));
			M1(row+2, col) = -8.0*pow(t[col+2-2], 2);

			M2(row, col) = 14.0*pow(t[col+3-2], 3);
			M2(row+1, col) = 16.0*(pow(t[col+2-2], 3) + pow(t[col+3-2], 3));
			M2(row+2, col) = 14.0*pow(t[col+2-2], 3);

			M3(row, col) = 1.0*t[col+2-2]*pow(t[col+3-2], 2);
			M3(row+1, col) = -3.0*(t[col+2-2]*pow(t[col+3-2], 2) + 
				pow(t[col+2-2], 2)*t[col+3-2]);
			M3(row+2, col) = pow(t[col+2-2], 2)*t[col+3-2];

			M4(row, col) = 2.0*t[col+2-2]*pow(t[col+3-2], 3);
			M4(row+1, col) = 3.0*(-t[col+2-2]*pow(t[col+3-2], 3) + 
				pow(t[col+2-2], 3)*t[col+3-2]);
			M4(row+2, col) = -2.0*pow(t[col+2-2], 3)*t[col+3-2];
			// CHECKED, should work
		}
	}
	//cout << M1 << " " << M2 << " " << M3 << " " << M4 << endl;

	// In the end fill the matrix M with M1, M2, M3 and M4
	for(int i=0; i<(m-2); i++)
	{
		for(int j=0; j<(m-2); j++)
		{
			M(i,j) = M1(i,j);
			M(i,j+m-2) = M2(i,j);
			M(i+m-2,j) = M3(i,j);
			M(i+m-2, j+m-2) = M4(i,j);
		}
	}
	//cout << M << endl;
}

void trajectoryPlanning::createMatrixAForHoCook56(
	Eigen::MatrixXd &A, std::vector<double> x, std::vector<double> t, int m, 
	DerivativeConditionsStartEnd conditions)
{
	// Getting the information about start and end values of derivatives. The
	// idea is to be able to replan segments and for that we need to continue
	// speed, acceleration and jerk. The problem may occur in split calculation
	double v0, vf, a0, af, j0, jf, s0, sf;
	v0 = conditions.start.speed;
	vf = conditions.end.speed;
	a0 = conditions.start.acceleration;
	af = conditions.end.acceleration;
	j0 = conditions.start.jerk;
	jf = conditions.end.jerk;
	s0 = conditions.start.split;
	sf = conditions.end.split;

	// Function calculates matrix A based on parametric times and waypoints.
	if(m == 3)
	{
		// Here we will have only two colmns of matrix A
		/*A(0,0) = (20.0/(t[2-2]*t[3-2]))*(2.0*pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 3)*(x[2-1] - x[3-1])) + 
			(pow(t[2-2], 2)*pow(t[3-2], 2))*(
				-20.0*v0/pow(t[2-2], 2) - 4.0*a0/t[2-2] - (1.0/3.0)*j0);

		A(0,(1-1)+m-2) = (30.0/(t[2-2]*t[3-2]))*(3.0*pow(t[3-2], 4)*(x[2-1] - x[1-1]) 
			+ pow(t[2-2], 4)*(x[3-1] - x[2-1])) + 
			(pow(t[2-2], 3)*pow(t[3-2], 3))*(
				-50.0*v0/pow(t[2-2], 3) - 11.0*a0/pow(t[2-2], 2) - 1.0*j0/t[2-2]);*/

		// Also a bit different coefficients because we have to stitch two
		// 6th order polynomials
		A(0,0) = (10.0/(t[2-2]*t[3-2]))*(pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 3)*(x[2-1] - x[3-1])) + 
			5.0*(pow(t[2-2], 2)*vf - pow(t[3-2], 2)*v0) - 
			t[2-2]*t[3-2]*(t[3-2]*a0 + t[2-2]*af) + 
			(1.0/12.0)*pow(t[2-2], 2)*pow(t[3-2], 2)*(jf - j0);

		A(0,1) = (90.0/(t[2-2]*t[3-2]))*(pow(t[3-2], 4)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 4)*(x[3-1] - x[2-1])) -
			50.0*(pow(t[3-2], 3)*v0 + pow(t[2-2], 3)*vf) + 
			11.0*(pow(t[2-2], 3)*t[3-2]*af - t[2-2]*pow(t[3-2], 3)*a0) - 
			pow(t[2-2], 2)*pow(t[3-2], 2)*(t[3-2]*j0 + t[2-2]*jf);

		//cout << "tu2" << endl;

	}
	else if(m >= 4)
	{
		//cout << "M >=4, matrix A" << endl;
		// Add the first and last segments, these are corresponding to
		// k=1, k=m-1 for upper and lower part of matrix. Everything will be
		// done here in one loop
		// Upper part of matrix, equations based on continuous x'''
		A(0,0) = (20.0/(t[2-2]*t[3-2]))*(2.0*pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 3)*(x[2-1] - x[3-1])) +  
			(pow(t[2-2], 2)*pow(t[3-2], 2))*(
				-20.0*v0/pow(t[2-2], 2) - 4.0*a0/t[2-2] - (1.0/3.0)*j0);

		A(0, m-2-1) = (20.0/(t[m-1-2]*t[m-2]))*(
			pow(t[m-2], 3)*(x[m-1-1] - x[m-2-1]) + 
			2.0*pow(t[m-1-2], 3)*(x[m-1-1] - x[m-1])) + 
			(pow(t[m-1-2], 2)*pow(t[m-2], 2))*(
				20.0*vf/pow(t[m-2],2) - 4.0*af/t[m-2] + (1.0/3.0)*jf);

		// Lower part of matrix equations based on x''''
		A(0, (1-1)+m-2) = (30.0/(t[2-2]*t[3-2]))*(3*pow(t[3-2], 4)*(x[2-1] - x[1-1]) 
			+ pow(t[2-2], 4)*(x[3-1] - x[2-1])) +
			(pow(t[2-2], 3)*pow(t[3-2], 3))*(
				-50.0*v0/pow(t[2-2], 3) - 11.0*a0/pow(t[2-2], 2) - 1.0*j0/t[2-2]);

		A(0, (m-2-1)+m-2) = (30.0/(t[m-1-2]*t[m-2]))*(
			pow(t[m-2], 4)*(x[m-1-1] - x[m-2-1]) + 
			3.0*pow(t[m-1-2], 4)*(x[m-1] - x[m-1-1])) +
			(pow(t[m-1-2], 3)*pow(t[m-2], 3))*(
				-50.0*vf/pow(t[m-2], 3) + 11.0*af/pow(t[m-2], 2) - 1.0*jf/t[m-2]);

		// Now all the other members. Lower part of matrix is m-2 members 
		// "ahead" upper part so there will be offset of (m-2) on indexes
		// regarding upper part of matrix
		for(int k=2; k<=(m-3); k++)
		{
			// Upper equation
			A(0, k-1) = (20.0/(t[k+1-2]*t[k+2-2]))*(
				pow(t[k+2-2], 3)*(x[k+1-1] - x[k-1]) + 
				pow(t[k+1-2], 3)*(x[k+1-1] - x[k+2-1]));

			// Lower equation
			A(0, k-1+m-2) = (30.0/(t[k+1-2]*t[k+2-2]))*(
				pow(t[k+2-2], 4)*(x[k+1-1] - x[k-1]) + 
				pow(t[k+1-2], 4)*(x[k+2-1] - x[k+1-1]));
		}
	}
}

void trajectoryPlanning::calculateCoefficientsHoCook56(
	std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
	std::vector<double> x, std::vector<double> t, 
	DerivativeConditionsStartEnd conditions)
{
	// Getting the information about start and end values of derivatives. The
	// idea is to be able to replan segments and for that we need to continue
	// speed, acceleration and jerk. The problem may occur in split calculation
	double v0, vf, a0, af, j0, jf, s0, sf;
	v0 = conditions.start.speed;
	vf = conditions.end.speed;
	a0 = conditions.start.acceleration;
	af = conditions.end.acceleration;
	j0 = conditions.start.jerk;
	jf = conditions.end.jerk;
	s0 = conditions.start.split;
	sf = conditions.end.split;

	//cout << v0 << ' ' << a0 << ' ' << j0 << ' ' << vf << ' ' << af << ' ' << jf << endl;

	// Function used to calculate polynomial coefficients for one axis at
	// the time. 

	// Get how many waypoints are in trajectory
	int m = x.size();

	// First segment is 6th order so 7 coeffs are needed. Starting jerk is set
	// to 0 here
	getSplineCoefficientsOrder6JerkStart(x[0], x[1], v0, D(0,0), a0, D(0, m-2), 
		j0, t[0], coefficients[0]);

	// Now the middle coefficients, they are all 4. order polynoms with
	// acceleration condition at start.
	//           k<(m-3) is equivalent
	for(int k=2; k<=(m-2); k++)
	{
		getSplineCoefficientsOrder5(x[k-1], x[k], D(0, k-2), D(0, k-1), 
			D(0, k-2+m-2), D(0, k-1+m-2), t[k+1-2], coefficients[k-1]);
	}

	// And the last segment is order 6 with 0 jerk at the end
	getSplineCoefficientsOrder6JerkEnd(x[m-1-1], x[m-1], D(0, m-2-1), vf, 
		D(0, (m-2-1)+m-2), af, jf, t[m-2], coefficients[m-2]);
}


// HoCook57 method takes into account split at start and the end.
mav_path_trajectory::TrajectorySampled trajectoryPlanning::hoCook57(
	mav_path_trajectory::WaypointArray waypointArray, 
	TrajectoryDerivativeConditions initialConditions, 
	int &scalingSegmentID)
{
	//cout << initialConditions.x.end.jerk << endl;
	mav_path_trajectory::TrajectorySampled trajectorySampledReturn;
	// This function uses Ho-Cook method for spline planning and generates
	// trajectory for quadcopter. In this case polynomial orders are 6 for 
	// first and last segment and 5 for all other segments. Minimul number of
	// waypoints to use this function is 3. If two waypoints are provided
	// trajectory will be generated through two points using 
	// twoWaypointsTrajectoryOrder5 function

	// Check for size
	int m = waypointArray.waypoints.size(); // Number of points in trajectory
	if(m == 2)
	{	
		// If there is not enough points for Ho-Cook method simply compute
		// and publish a trajectory through two points
		//trajectorySampledReturn = twoWaypointsTrajectoryOrder5(waypointArray);
		geometry_msgs::Pose tempPose;
		tempPose.position.x = (waypointArray.waypoints[0].position.x + 
			waypointArray.waypoints[1].position.x)/2.0;
		tempPose.position.y = (waypointArray.waypoints[0].position.y + 
			waypointArray.waypoints[1].position.y)/2.0;
		tempPose.position.z = (waypointArray.waypoints[0].position.z + 
			waypointArray.waypoints[1].position.z)/2.0;
		tempPose.orientation.z = (waypointArray.waypoints[0].orientation.z + 
			waypointArray.waypoints[1].orientation.z)/2.0;
		waypointArray.waypoints.insert(waypointArray.waypoints.begin()+1, tempPose);
		//for (int i=0; i<waypointArray.waypoints.size(); i++)
		//{
		//	cout << waypointArray.waypoints[i];
		//}
	}

	m = waypointArray.waypoints.size();
	if(m >= 3)
	{
		// Now we can interpolate using 4 and 5 order polynoms.

		// First of all we can compute parametrical times for the trajectory 
		// segments. It is simply euclidian distance divided by preset max speed
		// of quadcopter given through ROS param in constructor.
		std::vector<double> parametricTimes, parametricLengths;
		double tempXstart, tempXend, tempYstart, tempYend, tempZstart, tempZend;
		double tempYawStart, tempYawEnd;
		//cout << "parametricalTimes: " << endl;
		for(int i=2; i<=m; i++)
		{
			// Temporary values just to have clear equation later. Since it is
			// starting at 2 and waypoints are starting from zero. 
			tempXstart = waypointArray.waypoints[i-2].position.x;
			tempXend = waypointArray.waypoints[i-1].position.x;
			tempYstart = waypointArray.waypoints[i-2].position.y;
			tempYend = waypointArray.waypoints[i-1].position.y;
			tempZstart = waypointArray.waypoints[i-2].position.z;
			tempZend = waypointArray.waypoints[i-1].position.z;
			tempYawStart = waypointArray.waypoints[i-2].orientation.z;
			tempYawEnd = waypointArray.waypoints[i-1].orientation.z;

			// Computing parametrical time
			parametricTimes.push_back(max(sqrt(pow(tempXend - tempXstart, 2)
				+ pow(tempYend - tempYstart, 2) 
				+ pow(tempZend - tempZstart, 2))/maxSpeed, 
				abs(tempYawEnd - tempYawStart)/maxYawSpeed));
			parametricLengths.push_back(parametricTimes[i-2]*maxSpeed);
			//cout << parametricTimes[i-2] << endl;
		}

		// Here things get somewhat more complicated than in hoCook34. There
		// are twice as much conditions here because we are continuing splines
		// using speed and acceleration to have jerk free trajectory. Matrix M
		// is no longer (m-2)x(m-2) but [2*(m-2)]x[2*(m-2)] due to twice as
		// much conditions. Therefore matrix M will be divided in 4 matrices 
		// and combined at the end. Also all the members of this matrix will
		// be set to 0 initially.
		Eigen::MatrixXd M(2*(m-2), 2*(m-2));
		// Set all members of matrix to 0. This is actually faster than
		// M << M*0.0 and better since there were sam NAN elements in matrix
		// otherwise
		for(int i=0; i<2*(m-2); i++)
		{
			for(int j=0; j<2*(m-2); j++)
			{
				M(i,j) = 0.0;
			}
		}

		// Now we have matrix M all set, we just have to fill it.
		createMatrixMForHoCook57(M, parametricTimes, m);

		// Now that matrix M is here it's good to create matrices A for all
		// three axis. Additionaly create matrix A for yaw allthough yaw is
		// not used in initial parametric time calculation.
		Eigen::MatrixXd Ax(1, 2*(m-2));
		Eigen::MatrixXd Ay(1, 2*(m-2));
		Eigen::MatrixXd Az(1, 2*(m-2));
		Eigen::MatrixXd Ayaw(1, 2*(m-2));
		// This is messy work so it's done in function. It will be done in one
		// function called 3 times for x, y and z so all waypoints data has to
		// be in a vector. Adding yaw
		std::vector<double> positionsX, positionsY, positionsZ, positionsYAW;
		for(int i=0; i<waypointArray.waypoints.size(); i++)
		{
			positionsX.push_back(waypointArray.waypoints[i].position.x);
			positionsY.push_back(waypointArray.waypoints[i].position.y);
			positionsZ.push_back(waypointArray.waypoints[i].position.z);
			positionsYAW.push_back(waypointArray.waypoints[i].orientation.z);
		}

		// Create matrices
		createMatrixAForHoCook57(Ax, positionsX, parametricTimes, m, initialConditions.x);
		createMatrixAForHoCook57(Ay, positionsY, parametricTimes, m, initialConditions.y);
		createMatrixAForHoCook57(Az, positionsZ, parametricTimes, m, initialConditions.z);
		createMatrixAForHoCook57(Ayaw, positionsYAW, parametricTimes, m, initialConditions.yaw);
		//cout << Az << endl;
		
		// Now we can compute vector of speed and acceleration conditions using
		// matrices A and M. First create new matrix that will be inverse of
		// M so it won't be inverted three times
		Eigen::MatrixXd Minv = M.inverse();

		// Vectors containing conditions
		Eigen::MatrixXd xd, yd, zd, yawd;
		xd = Ax*Minv;
		yd = Ay*Minv;
		zd = Az*Minv;
		yawd = Ayaw*Minv;

		// Okay, here is everything needed for coefficients calculation. 
		// They'll be put in a vector of vectors, first and last segment with
		// 8 coefficients; other segments with 6 coefficients.
		std::vector< std::vector< double > > trajectoryCoefficientsX, 
		trajectoryCoefficientsY, trajectoryCoefficientsZ, trajectoryCoefficientsYAW;
		double tempV8[8] = {0, 0, 0, 0, 0, 0, 0, 0};
		double tempV6[6] = {0, 0, 0, 0, 0, 0};

		// First goes segment one with 7 coefficients
		std::vector<double> tempVector8(&tempV8[0], &tempV8[0]+8);
		trajectoryCoefficientsX.push_back(tempVector8);
		trajectoryCoefficientsY.push_back(tempVector8);
		trajectoryCoefficientsZ.push_back(tempVector8);
		trajectoryCoefficientsYAW.push_back(tempVector8);

		// Now for the segments except last
		for(int i=1; i<m-1-1; i++)
		{
			std::vector<double> tempVector(&tempV6[0], &tempV6[0]+6);
			trajectoryCoefficientsX.push_back(tempVector);
			trajectoryCoefficientsY.push_back(tempVector);
			trajectoryCoefficientsZ.push_back(tempVector);
			trajectoryCoefficientsYAW.push_back(tempVector);
		}

		// Last segment coefficients are also of length 7. This works.
		trajectoryCoefficientsX.push_back(tempVector8);
		trajectoryCoefficientsY.push_back(tempVector8);
		trajectoryCoefficientsZ.push_back(tempVector8);
		trajectoryCoefficientsYAW.push_back(tempVector8);

		// Everything is set for calculation of coefficients of spline segments
		// based on waypoints, calculated conditions and parametric times.
		calculateCoefficientsHoCook57(trajectoryCoefficientsX, xd, positionsX, 
			parametricTimes, initialConditions.x);
		calculateCoefficientsHoCook57(trajectoryCoefficientsY, yd, positionsY, 
			parametricTimes, initialConditions.y);
		calculateCoefficientsHoCook57(trajectoryCoefficientsZ, zd, positionsZ, 
			parametricTimes, initialConditions.z);
		calculateCoefficientsHoCook57(trajectoryCoefficientsYAW, yawd, 
			positionsYAW, parametricTimes, initialConditions.yaw);


		// Now we can sample that trajectory to find out max speed and acceleration
		// Let's first create trajectory
		mav_path_trajectory::TrajectorySampled trajectorySampled;
		std::vector<int> trajectoryEndIndexes;
		sampleMultipleWaypointTrajectory(trajectoryCoefficientsX, 
			trajectoryCoefficientsY, trajectoryCoefficientsZ, 
			trajectoryCoefficientsYAW, parametricTimes, trajectorySampled, 
			trajectorySamplingFrequency, trajectoryEndIndexes);

		// Trajectory is here but speeds and accelerations are not what they are
		// most likely too big. That's why we now enter the second stage of
		// Ho-Cook method in which we find critical acceleration and speed or
		// in other words the biggest by absolute value. That is done using 
		// total speed and acceleration of quadcopter. This part may require
		// more than one iteration, based on sample time given. In the end the
		// scaling factor should be 1, or in our case between 0.99 and 1.01.

		// First implementation goes with only one iteration
		int tempIndexAcc, tempIndexSpeed, tempIndexYawSpeed, tempIndexYawAcc;
		double maxTrajectorySpeed = getMaxTrajectorySpeed(trajectorySampled, 
			tempIndexSpeed);
		double maxTrajectoryAcc = getMaxTrajectoryAcceleration(trajectorySampled, 
			tempIndexAcc);
		double maxTrajectoryYawSpeed = getMaxTrajectoryYawSpeed(trajectorySampled, 
			tempIndexYawSpeed);
		double maxTrajectoryYawAcc = getMaxTrajectoryYawAcc(trajectorySampled, 
			tempIndexYawAcc);

		// Speed and acceleration scaling factors
		double sv = maxTrajectorySpeed/maxSpeed;
		double sa = sqrt(maxTrajectoryAcc/maxAcc*1.0);
		double svYaw = maxTrajectoryYawSpeed/maxYawSpeed;
		double saYaw = sqrt(maxTrajectoryYawAcc/maxAcc*1.0);
		double s = max(max(max(sv, sa), svYaw), saYaw);

		// Get the scaling segment index
		//int scalingSegmentID;
		if(sv>sa && sv>svYaw && sv>saYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexSpeed];
		}
		if(sa>sv && sa>svYaw && sa>saYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexAcc];
		}
		if(svYaw>sv && svYaw>sa && svYaw>saYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexYawSpeed];
		}
		if(saYaw>sv && saYaw>sa && saYaw>svYaw)
		{
			scalingSegmentID = trajectorySampled.segment[tempIndexYawAcc];
		}

		//cout << sv << " " << sa << " " << svYaw << " " << saYaw << endl;
		//cout << "sa: " << sa << " sv: " << sv << " speed: " 
		//	<< maxTrajectorySpeed << " acc: " << maxTrajectoryAcc << endl;

		// After finding scaling factor we have to scale parametric times
		for(int i=0; i<parametricTimes.size(); i++)
		{
			parametricTimes[i] = parametricTimes[i]*s;
		}
		// Clear the trajectory because we have to create new one with new set
		// of parametric times.
		clearTrajectorySampled(trajectorySampled);

		// At this point parametric times are computed and we can begin
		// forming matrix M. It is the size 2(m-2)x2(m-2) and inverse will be
		// needed later so Eigen seems like pretty logical solution. Initial
		// values are all set to 0
		for(int i=0; i<2*(m-2); i++)
		{
			for(int j=0; j<2*(m-2); j++)
			{
				M(i,j) = 0.0;
			}
		}

		// Now we have matrix M all set, we just have to fill it.
		createMatrixMForHoCook57(M, parametricTimes, m);

		// Initialize all zeros for A matrices
		Ax << Ax*0.0;
		Ay << Ay*0.0;
		Az << Az*0.0;
		Ayaw << Ayaw*0.0;

		// Create matrices
		createMatrixAForHoCook57(Ax, positionsX, parametricTimes, m, initialConditions.x);
		createMatrixAForHoCook57(Ay, positionsY, parametricTimes, m, initialConditions.y);
		createMatrixAForHoCook57(Az, positionsZ, parametricTimes, m, initialConditions.z);
		createMatrixAForHoCook57(Ayaw, positionsYAW, parametricTimes, m, initialConditions.yaw);

		// Now we can compute vector of speed and acceleration conditions 
		// using matrices A and M. First create new matrix that will be 
		// inverse of M so it won't be inverted three times
		Minv = M.inverse();

		// Vectors containing conditions
		xd = Ax*Minv;
		yd = Ay*Minv;
		zd = Az*Minv;
		yawd = Ayaw*Minv;

		// Everything is set for calculation of coefficients of spline 
		// segments based on waypoints, calculated conditions and 
		// parametric times.
		calculateCoefficientsHoCook57(trajectoryCoefficientsX, xd, positionsX, 
			parametricTimes, initialConditions.x);
		calculateCoefficientsHoCook57(trajectoryCoefficientsY, yd, positionsY, 
			parametricTimes, initialConditions.y);
		calculateCoefficientsHoCook57(trajectoryCoefficientsZ, zd, positionsZ, 
			parametricTimes, initialConditions.z);
		calculateCoefficientsHoCook57(trajectoryCoefficientsYAW, yawd, positionsYAW, 
			parametricTimes, initialConditions.yaw);

		// Here we also have everything needed
		sampleMultipleWaypointTrajectory(trajectoryCoefficientsX, 
			trajectoryCoefficientsY, trajectoryCoefficientsZ, 
			trajectoryCoefficientsYAW, parametricTimes, 
			trajectorySampled, trajectorySamplingFrequency, trajectoryEndIndexes);

		// Adding information about which index belongs to which segmet
		/*int segmentCount = 1;
		for(int i=0; i<trajectorySampled.position.size(); i++)
		{
			trajectorySampled.segment.push_back(segmentCount);
			if(trajectoryEndIndexes[segmentCount-1] == i)
			{
				segmentCount++;
			}
		}*/

		// Add zero speed to the end and publish the trajectory.
		//addZeroSpeedAndAccAtTrajectoryEnd(trajectorySampled);
		trajectorySampledReturn = trajectorySampled;
		//trajectorySampledPub.publish(trajectorySampled);
	}

	return trajectorySampledReturn;
}

void trajectoryPlanning::createMatrixMForHoCook57(
	Eigen::MatrixXd &M, std::vector<double> t, int m)
{
	// First initialize submatrices of matrix M. Matrix M can be represented as
	//     [M1 | M2]
	// M = [-------]
	//     [M3 | M4]
	// Where M1, M2, M3 and M4 submatrices are of dimensions (m-2)x(m-2)
	Eigen::MatrixXd M1(m-2, m-2);
	Eigen::MatrixXd M2(m-2, m-2);
	Eigen::MatrixXd M3(m-2, m-2);
	Eigen::MatrixXd M4(m-2, m-2);
	// And fill them up with zeros
	for(int i=0; i<(m-2); i++)
	{
		for(int j=0; j<(m-2); j++)
		{
			M1(i,j) = 0.0;
			M2(i,j) = 0.0;
			M3(i,j) = 0.0;
			M4(i,j) = 0.0;
		}
	}

	// All matrices will be filled column by column. First and last column are
	// different than all the others so these will be filled separately.
	if(m == 3)
	{
		// In every parametrical time we have correction factor -2 because
		// in reality they start at t2 but indexing of arrays goes from 0
		// so a bit of subtraction is needed. This is special case when
		// there are only two segments. Also it is not relevant whether first
		// or last segment is used since it's only a number. Maybe it is
		// relevant if there is difference in + or - but I hope it's not.
		//M1(0,0) = 20.0*pow(t[3-2], 2) - 12*pow(t[2-2], 2);
		//M2(0,0) = 40.0*pow(t[3-2], 3) + 16*pow(t[2-2], 3);
		//M3(0,0) = -4.0*t[2-2]*pow(t[3-2], 2) - 3.0*pow(t[2-2], 2)*t[3-2];
		//M4(0,0) = -6.0*t[2-2]*pow(t[3-2], 3) + 3.0*pow(t[2-2], 3)*t[3-2];

		// A bit different approach here because we have to continue two 6th
		// order polynomials and coefficients are a bit different
		M1(0,0) = 30.0*(pow(t[3-2], 2) - pow(t[2-2], 2));
		M2(0,0) = 80.0*(pow(t[3-2], 3) + pow(t[2-2], 3));
		M3(0,0) = -5.0*(t[2-2]*pow(t[3-2], 2) + pow(t[2-2], 2)*t[3-2]);
		M4(0,0) = 10.0*(pow(t[2-2], 3)*t[3-2] - t[2-2]*pow(t[3-2], 3));
		//cout << "tu" << endl;

		// CHECKED, working
	}

	if(m >= 4)
	{
		// Filling first and last columns of submatrices
		// CHECKED, works
		// M1:
		M1(0,0) = 30.0*pow(t[3-2], 2) - 12*pow(t[2-2], 2);
		M1(1,0) = -8.0*pow(t[2-2], 2);
		M1(m-3-1, m-2-1) = 8.0*pow(t[m-2], 2);
		M1(m-2-1, m-2-1) = 12.0*pow(t[m-2], 2) - 30.0*pow(t[m-1-2], 2);
		// M2:
		M2(0,0) = 80.0*pow(t[3-2], 3) + 16*pow(t[2-2], 3);
		M2(1,0) = 14.0*pow(t[2-2], 3);
		M2(m-3-1, m-2-1) = 14.0*pow(t[m-2], 3);
		M2(m-2-1, m-2-1) = 16.0*pow(t[m-2], 3) + 80.0*pow(t[m-1-2], 3);
		// M3:
		M3(0,0) = -5.0*t[2-2]*pow(t[3-2], 2) - 3.0*pow(t[2-2], 2)*t[3-2];
		M3(1,0) = pow(t[2-2], 2)*t[3-2];
		M3(m-3-1, m-2-1) = t[m-1-2]*pow(t[m-2], 2);
		M3(m-2-1, m-2-1) = -3.0*t[m-1-2]*pow(t[m-2], 2) - 5.0*pow(t[m-1-2], 2)*t[m-2];
		// M4:
		M4(0,0) = -10.0*t[2-2]*pow(t[3-2], 3) + 3.0*pow(t[2-2], 3)*t[3-2];
		M4(1,0) = -2.0*pow(t[2-2], 3)*t[3-2];
		M4(m-3-1, m-2-1) = 2*t[m-1-2]*pow(t[m-2], 3);
		M4(m-2-1, m-2-1) = -3.0*t[m-1-2]*pow(t[m-2], 3) + 10.0*pow(t[m-1-2], 3)*t[m-2];

		// Now we can start filling the inner parts of matrices
		for(int col=1; col <= (m-3)-1; col++)
		{
			//cout << "M >=4, matrix M" << endl;
			// When filling inner columns row starts at col-1 as we look at
			// the matrix. Three rows are to be filled so we increment row
			// two times. Also that means the matrix will have elements only
			// on the main diagonal and it's subdiagonals.
			int row = col-1;
			// Also there is a rule to use parametrical times with column as
			// index. There is -2 subtraction on every place in indexing and
			// that's because parametrical times start at index 0. Example:
			// t2 = t[2-2] => t2 = t[0]

			// All four matrices are very similar, with main diagonal and one 
			// or two subdiagonals so we can use same indexes
			M1(row, col) = 8.0*pow(t[col+3-2], 2);
			M1(row+1, col) = 12.0*(pow(t[col+3-2], 2) - pow(t[col+2-2], 2));
			M1(row+2, col) = -8.0*pow(t[col+2-2], 2);

			M2(row, col) = 14.0*pow(t[col+3-2], 3);
			M2(row+1, col) = 16.0*(pow(t[col+2-2], 3) + pow(t[col+3-2], 3));
			M2(row+2, col) = 14.0*pow(t[col+2-2], 3);

			M3(row, col) = 1.0*t[col+2-2]*pow(t[col+3-2], 2);
			M3(row+1, col) = -3.0*(t[col+2-2]*pow(t[col+3-2], 2) + 
				pow(t[col+2-2], 2)*t[col+3-2]);
			M3(row+2, col) = pow(t[col+2-2], 2)*t[col+3-2];

			M4(row, col) = 2.0*t[col+2-2]*pow(t[col+3-2], 3);
			M4(row+1, col) = 3.0*(-t[col+2-2]*pow(t[col+3-2], 3) + 
				pow(t[col+2-2], 3)*t[col+3-2]);
			M4(row+2, col) = -2.0*pow(t[col+2-2], 3)*t[col+3-2];
			// CHECKED, should work
		}
	}
	//cout << M1 << " " << M2 << " " << M3 << " " << M4 << endl;

	// In the end fill the matrix M with M1, M2, M3 and M4
	for(int i=0; i<(m-2); i++)
	{
		for(int j=0; j<(m-2); j++)
		{
			M(i,j) = M1(i,j);
			M(i,j+m-2) = M2(i,j);
			M(i+m-2,j) = M3(i,j);
			M(i+m-2, j+m-2) = M4(i,j);
		}
	}
	//cout << M << endl;
	//cout << t[0] << " " << t[1] << " " << t[2] << endl << endl;
}

void trajectoryPlanning::createMatrixAForHoCook57(
	Eigen::MatrixXd &A, std::vector<double> x, std::vector<double> t, int m, 
	DerivativeConditionsStartEnd conditions)
{
	// Getting the information about start and end values of derivatives. The
	// idea is to be able to replan segments and for that we need to continue
	// speed, acceleration and jerk. The problem may occur in split calculation
	double v0, vf, a0, af, j0, jf, s0, sf;
	v0 = conditions.start.speed;
	vf = conditions.end.speed;
	a0 = conditions.start.acceleration;
	af = conditions.end.acceleration;
	j0 = conditions.start.jerk;
	jf = conditions.end.jerk;
	s0 = conditions.start.split;
	sf = conditions.end.split;
	//cout << j0 << " " << jf << " " << s0 << " " << sf << endl;

	// Function calculates matrix A based on parametric times and waypoints.
	if(m == 3)
	{
		// Also a bit different coefficients because we have to stitch two
		// 7th order polynomials
		A(0,0) = (70.0/(t[2-2]*t[3-2]))*(pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 3)*(x[2-1] - x[3-1])) - 
			40.0*pow(t[3-2], 2)*v0 + 40.0*pow(t[2-2], 2)*vf - 
			10.0*t[2-2]*pow(t[3-2], 2)*a0 - 10.0*pow(t[2-2], 2)*t[3-2]*af + 
			(4.0/3.0)*pow(t[2-2], 2)*pow(t[3-2], 2)*(-j0 + jf) - 
			(1.0/12.0)*pow(t[2-2], 2)*pow(t[3-2], 2)*(t[2-2]*s0 + t[3-2]*sf);

		A(0,1) = (210.0/(t[2-2]*t[3-2]))*(pow(t[3-2], 4)*(x[2-1] - x[1-1]) + 
			pow(t[2-2], 4)*(x[3-1] - x[2-1])) - 
			130.0*(pow(t[3-2], 3)*v0 + pow(t[2-2], 3)*vf) + 
			35.0*t[2-2]*t[3-2]*(pow(t[2-2], 2)*af - pow(t[3-2], 2)*a0) - 
			5.0*pow(t[2-2], 2)*pow(t[3-2], 2)*(t[3-2]*j0 + t[2-2]*jf) + 
			(1.0/3.0)*pow(t[2-2], 3)*pow(t[3-2], 3)*(-s0 + sf);

		//cout << "tu2" << endl;

	}
	else if(m >= 4)
	{
		//cout << "M >=4, matrix A" << endl;
		// Add the first and last segments, these are corresponding to
		// k=1, k=m-1 for upper and lower part of matrix. Everything will be
		// done here in one loop
		// Upper part of matrix, equations based on continuous x'''
		A(0,0) = (10.0/(t[2-2]*t[3-2]))*(7.0*pow(t[3-2], 3)*(x[2-1] - x[1-1]) + 
			2.0*pow(t[2-2], 3)*(x[2-1] - x[3-1])) - 
			40.0*pow(t[3-2], 2)*v0 - 
			10.0*t[2-2]*pow(t[3-2], 2)*a0 - 
			(4.0/3.0)*pow(t[2-2], 2)*pow(t[3-2], 2)*j0 - 
			(1.0/12.0)*pow(t[2-2], 3)*pow(t[3-2], 2)*s0;

		A(0, m-2-1) = (10.0/(t[m-1-2]*t[m-2]))*(2.0*pow(t[m-2], 3)*(x[m-1-1] - x[m-2-1]) + 
			7.0*pow(t[m-1-2], 3)*(x[m-1-1] - x[m-1])) + 
			40.0*pow(t[m-1-2], 2)*vf - 
			10.0*pow(t[m-1-2], 2)*t[m-2]*af + 
			(4.0/3.0)*pow(t[m-1-2], 2)*pow(t[m-2], 2)*jf - 
			(1.0/12.0)*pow(t[m-1-2], 2)*pow(t[m-2], 3)*sf;

		// Lower part of matrix equations based on x''''
		A(0, (1-1)+m-2) = (30.0/(t[2-2]*t[3-2]))*(7.0*pow(t[3-2], 4)*(x[2-1] - x[1-1]) 
			+ pow(t[2-2], 4)*(x[3-1] - x[2-1])) - 
			130.0*pow(t[3-2], 3)*v0 - 
			35.0*t[2-2]*pow(t[3-2], 3)*a0 - 
			5.0*pow(t[2-2], 2)*pow(t[3-2], 3)*j0 - 
			(1.0/3.0)*pow(t[2-2], 3)*pow(t[3-2], 3)*s0;

		A(0, (m-2-1)+m-2) = (30.0/(t[m-1-2]*t[m-2]))*(pow(t[m-2], 4)*(x[m-1-1] - x[m-2-1]) + 
			7.0*pow(t[m-1-2], 4)*(x[m-1] - x[m-1-1])) - 
			130.0*pow(t[m-1-2], 3)*vf + 
			35.0*pow(t[m-1-2], 3)*t[m-2]*af - 
			5.0*pow(t[m-1-2], 3)*pow(t[m-2], 2)*jf + 
			(1.0/3.0)*pow(t[m-1-2], 3)*pow(t[m-2], 3)*sf;

		// Now all the other members. Lower part of matrix is m-2 members 
		// "ahead" upper part so there will be offset of (m-2) on indexes
		// regarding upper part of matrix
		for(int k=2; k<=(m-3); k++)
		{
			// Upper equation
			A(0, k-1) = (20.0/(t[k+1-2]*t[k+2-2]))*(
				pow(t[k+2-2], 3)*(x[k+1-1] - x[k-1]) + 
				pow(t[k+1-2], 3)*(x[k+1-1] - x[k+2-1]));

			// Lower equation
			A(0, k-1+m-2) = (30.0/(t[k+1-2]*t[k+2-2]))*(
				pow(t[k+2-2], 4)*(x[k+1-1] - x[k-1]) + 
				pow(t[k+1-2], 4)*(x[k+2-1] - x[k+1-1]));
		}
	}
}

void trajectoryPlanning::calculateCoefficientsHoCook57(
	std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
	std::vector<double> x, std::vector<double> t, 
	DerivativeConditionsStartEnd conditions)
{
	// Getting the information about start and end values of derivatives. The
	// idea is to be able to replan segments and for that we need to continue
	// speed, acceleration and jerk. The problem may occur in split calculation
	double v0, vf, a0, af, j0, jf, s0, sf;
	v0 = conditions.start.speed;
	vf = conditions.end.speed;
	a0 = conditions.start.acceleration;
	af = conditions.end.acceleration;
	j0 = conditions.start.jerk;
	jf = conditions.end.jerk;
	s0 = conditions.start.split;
	sf = conditions.end.split;

	//cout << v0 << ' ' << a0 << ' ' << j0 << ' ' << vf << ' ' << af << ' ' << jf << endl;

	// Function used to calculate polynomial coefficients for one axis at
	// the time. 

	// Get how many waypoints are in trajectory
	int m = x.size();

	// First segment is 7th order so 8 coeffs are needed. Starting jerk is set
	// to 0 here
	getSplineCoefficientsOrder7JerkSplitStart(x[0], x[1], v0, D(0,0), a0, D(0, m-2), 
		j0, s0, t[0], coefficients[0]);

	// Now the middle coefficients, they are all 4. order polynoms with
	// acceleration condition at start.
	//           k<(m-3) is equivalent
	for(int k=2; k<=(m-2); k++)
	{
		getSplineCoefficientsOrder5(x[k-1], x[k], D(0, k-2), D(0, k-1), 
			D(0, k-2+m-2), D(0, k-1+m-2), t[k+1-2], coefficients[k-1]);
	}

	// And the last segment is order 7 with 0 jerk at the end
	getSplineCoefficientsOrder7JerkSplitEnd(x[m-1-1], x[m-1], D(0, m-2-1), vf, 
		D(0, (m-2-1)+m-2), af, jf, sf, t[m-2], coefficients[m-2]);
}


void trajectoryPlanning::waypointCallback(
	const mav_path_trajectory::WaypointArray &msg)
{
	TrajectoryDerivativeConditions conditions;
	DerivativeConditions tempCond;
	tempCond.speed = 0.0;
	tempCond.acceleration = 0.0;
	tempCond.jerk = 0.0;
	tempCond.split = 0.0;
	conditions.x.start = tempCond;
	conditions.x.end = tempCond;
	conditions.y.start = tempCond;
	conditions.y.end = tempCond;
	conditions.z.start = tempCond;
	conditions.z.end = tempCond;
	conditions.yaw.start = tempCond;
	conditions.yaw.end = tempCond;
	
	/*for(int i=0; i<tempTrajectory.position.size(); i++)
	{
		if(tempTrajectory.segment[i] == 2)
		{
			cout << "Position:" << endl << tempTrajectory.position[i] << endl;
			cout << "Speed:" << endl << tempTrajectory.speed[i] << endl;
			cout << "Acc:" << endl << tempTrajectory.acceleration[i] << endl;
			cout << "Jerk:" << endl << tempTrajectory.jerk[i] << endl;
			cout << "Split:" << endl << tempTrajectory.split[i] << endl;
			break;
		}
	}*/
	/*
	mav_path_trajectory::TrajectorySampled finalTrajectory;
	tempTrajectory = hoCook56(msg, conditions);
	int tempIndexSeg2 = 0;

	for(int i=0; i<tempTrajectory.position.size(); i++)
	{
		if(tempTrajectory.segment[i]==2) 
		{	
			tempIndexSeg2 = i-1;
			break;
		}
		else
		{
			finalTrajectory.position.push_back(tempTrajectory.position[i]);
			finalTrajectory.speed.push_back(tempTrajectory.speed[i]);
			finalTrajectory.acceleration.push_back(tempTrajectory.acceleration[i]);
			finalTrajectory.jerk.push_back(tempTrajectory.jerk[i]);
			finalTrajectory.split.push_back(tempTrajectory.split[i]);
		}
	}

	mav_path_trajectory::WaypointArray newWaypoints;
	for(int i=1; i<msg.waypoints.size(); i++)
	{
		newWaypoints.waypoints.push_back(msg.waypoints[i]);
	}

	conditions.x.start.speed = tempTrajectory.speed[tempIndexSeg2].x;
	conditions.x.start.acceleration = tempTrajectory.acceleration[tempIndexSeg2].x;
	conditions.x.start.jerk = tempTrajectory.jerk[tempIndexSeg2].x;
	conditions.y.start.speed = tempTrajectory.speed[tempIndexSeg2].y;
	conditions.y.start.acceleration = tempTrajectory.acceleration[tempIndexSeg2].y;
	conditions.y.start.jerk = tempTrajectory.jerk[tempIndexSeg2].y;

	usleep(1000000);
	clearTrajectorySampled(tempTrajectory);
	tempTrajectory = hoCook56(newWaypoints, conditions);

	for(int i=0; i<tempTrajectory.position.size(); i++)
	{
		finalTrajectory.position.push_back(tempTrajectory.position[i]);
		finalTrajectory.speed.push_back(tempTrajectory.speed[i]);
		finalTrajectory.acceleration.push_back(tempTrajectory.acceleration[i]);
		finalTrajectory.jerk.push_back(tempTrajectory.jerk[i]);
		finalTrajectory.split.push_back(tempTrajectory.split[i]);
	}
	trajectorySampledPub.publish(finalTrajectory);*/
	mav_path_trajectory::TrajectorySampled tempTrajectory;
	tempTrajectory = optimizedPlan(msg);
	cout << maxSpeed << " " << maxAcc << endl;
	trajectorySampledPub.publish(tempTrajectory);
	//int temp;
	//trajectorySampledPub.publish(hoCook57(msg, conditions, temp));
}

mav_path_trajectory::TrajectorySampled trajectoryPlanning::optimizedPlan(
		mav_path_trajectory::WaypointArray wp)
{
	// Final trajectory will be filled at the end of the function
	mav_path_trajectory::TrajectorySampled finalTrajectory;
	int k;

	// Initialize trajectory segment vector where optimized trajectory
	// will be stored. Initial value of isFixedFlag will be false since at the
	// beginning no segments are fixed.
	std::vector<TrajectorySegment> trajectorySegmentVector;
	TrajectorySegment tempTrajectorySegment;
	tempTrajectorySegment.isFixedFlag = false;
	for(int i=0; i<wp.waypoints.size()-1; i++)
	{
		trajectorySegmentVector.push_back(tempTrajectorySegment);
		trajectorySegmentVector[i].segmentNumber = i+1;
	}

	// At first trajectory is surely not optimized
	bool optimizedFlag = false;
	// We go through the trajectory while it is not optimized
	int iter = 0;
	while(!optimizedFlag)
	{
		iter++;
		// First find a part of trajectory that is not optimized. If first and
		// last segments are equal to -1 than the whole trajectory is optimized
		// and we must break the loop.
		int firstSegment, lastSegment;
		getFirstAndLastAvaliableSegment(trajectorySegmentVector, firstSegment, 
			lastSegment);
		if((firstSegment == -1) || (lastSegment == -1)) break;
		//cout << firstSegment << " " << lastSegment << endl;

		// Now that we have first and last segment we can create new waypoints
		// vector. Segments start at 1 so we have to subtract one from first
		// segment. Last segment is ok because index number of last waypoint
		// is that number. If there is nine waypoints, there will be 8 segments
		// and wp[8] is the last waypoint.
		mav_path_trajectory::WaypointArray waypointArrayChunk;
		for(int i=(firstSegment-1); i<=lastSegment; i++)
		{
			waypointArrayChunk.waypoints.push_back(wp.waypoints[i]);
		}
		//cout << firstSegment << " " << lastSegment << endl;
		
		
		// Now we can set initial and final conditions.
		TrajectoryDerivativeConditions conditions;
		setTrajectoryDerivativeConditions(trajectorySegmentVector, firstSegment, 
			lastSegment, conditions);
		//printConditions(conditions);
		

		// Now we have everything we need to plan the trajectory
		mav_path_trajectory::TrajectorySampled trajectoryChunk;
		int scalingSegmentID;
		trajectoryChunk = hoCook57(waypointArrayChunk, conditions, 
			scalingSegmentID);

		// Go through trajectory and fixate segments that have to be fixed
		fixateAndSampleTrajectorySegments(trajectorySegmentVector, 
			trajectoryChunk, firstSegment, lastSegment, scalingSegmentID);

		// Go through all trajectory segments and check if all are true. If
		// they are set optimized flag to true
		bool tempFlag = true;
		for(int i=0; i<trajectorySegmentVector.size(); i++)
		{
			tempFlag = tempFlag & trajectorySegmentVector[i].isFixedFlag;
			//cout << "Segment " << i+1 << ": size=" << 
			//	trajectorySegmentVector[i].trajectorySampled.position.size() <<
			//	" Flag=" << trajectorySegmentVector[i].isFixedFlag << endl;
		}
		//cout << "OptimizedFlag: " << tempFlag << endl;
		optimizedFlag = tempFlag;
		//cout << iter << endl;
		//trajectorySampledPub.publish(trajectoryChunk);
		//cin.ignore();
	}

	// Fill in final trajectory
	for(int i=0; i<trajectorySegmentVector.size(); i++)
	{
		for(int j=0; j<trajectorySegmentVector[i].trajectorySampled.position.size(); j++)
		{
			finalTrajectory.position.push_back(trajectorySegmentVector[i].trajectorySampled.position[j]);
			finalTrajectory.speed.push_back(trajectorySegmentVector[i].trajectorySampled.speed[j]);
			finalTrajectory.acceleration.push_back(trajectorySegmentVector[i].trajectorySampled.acceleration[j]);
			finalTrajectory.jerk.push_back(trajectorySegmentVector[i].trajectorySampled.jerk[j]);
			finalTrajectory.split.push_back(trajectorySegmentVector[i].trajectorySampled.split[j]);
			finalTrajectory.yawPos.push_back(trajectorySegmentVector[i].trajectorySampled.yawPos[j]);
			finalTrajectory.yawSpeed.push_back(trajectorySegmentVector[i].trajectorySampled.yawSpeed[j]);
			finalTrajectory.yawAcc.push_back(trajectorySegmentVector[i].trajectorySampled.yawAcc[j]);
			finalTrajectory.yawJerk.push_back(trajectorySegmentVector[i].trajectorySampled.yawJerk[j]);
			finalTrajectory.yawSplit.push_back(trajectorySegmentVector[i].trajectorySampled.yawSplit[j]);
			finalTrajectory.segment.push_back(trajectorySegmentVector[i].trajectorySampled.segment[j]);
		}
		//cout << trajectorySegmentVector[i].trajectorySampled.position.size() 
		//	<< " " << trajectorySegmentVector[i].isFixedFlag << " " <<
		//	trajectorySegmentVector[i].trajectorySampled.segment[0] << endl;
	}

	cout << "final: " << finalTrajectory.jerk.size() << endl;

	return finalTrajectory;
}

void trajectoryPlanning::getFirstAndLastAvaliableSegment(
	std::vector<TrajectorySegment> trajectorySegments, int &firstSegment, 
	int &lastSegment)
{
	firstSegment = -1;
	lastSegment = -1;
	int segmentsCount = trajectorySegments.size();

	// Go through trajectory and find the first segment that is not fixed
	for(int i=0; i<segmentsCount; i++)
	{
		if(trajectorySegments[i].isFixedFlag == false)
		{
			firstSegment = i+1;
			break;
		}
	}
	// Now if first segment is still equal to -1 trajectory is optimized and we
	// return from function
	if(firstSegment == -1) return;

	// Go from first segment to the end and find fixed segment
	for(int i=firstSegment-1; i<segmentsCount; i++)
	{
		if(trajectorySegments[i].isFixedFlag == true)
		{	
			// When we find the first segment that is fixed we stop there and
			// set lastSegment=i because it should be i-1+1 where i-1 is 
			// former segment and +1 comes from the fact that we start counting
			// segments from 1 and not from 0.
			lastSegment = i;
			break;
		}

		if(i==(segmentsCount-1))
		{
			// One possible situations is at the beginning. First segment will
			// be false and therefore firstSegment=1 but all other segments
			// will also be false. That's why upper condition won't find us the
			// last segment and in that case this condition will say that all
			// segments are false so we have to take last segment.
			lastSegment = i+1;
			break;
		}
	}
}

void trajectoryPlanning::setTrajectoryDerivativeConditions(std::vector<TrajectorySegment> 
	trajectorySegments, int firstSegment, int lastSegment, 
	TrajectoryDerivativeConditions &conditions)
{
	//cout << "FirstSegment: " << firstSegment << " LastSegment: " << lastSegment <<
	//	" Size: " << trajectorySegments.size() << endl;
	// If first segment is equal to 1 than we are at the beginning of the
	// trajectory and all initial conditions are zero.
	if(firstSegment == 1)
	{
		DerivativeConditions tempZeroCond;
		tempZeroCond.speed = 0.0;
		tempZeroCond.acceleration = 0.0;
		tempZeroCond.jerk = 0.0;
		tempZeroCond.split = 0.0;
		conditions.x.start = tempZeroCond;
		conditions.y.start = tempZeroCond;
		conditions.z.start = tempZeroCond;
		conditions.yaw.start = tempZeroCond;
	}
	else if(firstSegment == -1)
	{
		cout << "First segment = -1. Something went terribly wrong." << endl;
	}
	else
	{
		// Here we have to take end conditions from the previous segment. Since
		// segmentCount starts at 1 trSegments[1] is actually second segment
		// and we have to access the end of the first segment. If first segment
		// is 2 that we have to access end of trajectory[0] so we have -2 as
		// indexing offset
		int end = trajectorySegments[firstSegment-2].trajectorySampled.position.size() - 1;
		// X-axis
		conditions.x.start.speed = 
			trajectorySegments[firstSegment-2].trajectorySampled.speed[end].x;
		conditions.x.start.acceleration = 
			trajectorySegments[firstSegment-2].trajectorySampled.acceleration[end].x;
		conditions.x.start.jerk = 
			trajectorySegments[firstSegment-2].trajectorySampled.jerk[end].x;
		conditions.x.start.split = 
			trajectorySegments[firstSegment-2].trajectorySampled.split[end].x;

		// Y-axis
		conditions.y.start.speed = 
			trajectorySegments[firstSegment-2].trajectorySampled.speed[end].y;
		conditions.y.start.acceleration = 
			trajectorySegments[firstSegment-2].trajectorySampled.acceleration[end].y;
		conditions.y.start.jerk = 
			trajectorySegments[firstSegment-2].trajectorySampled.jerk[end].y;
		conditions.y.start.split = 
			trajectorySegments[firstSegment-2].trajectorySampled.split[end].y;

		// Z-axis
		conditions.z.start.speed = 
			trajectorySegments[firstSegment-2].trajectorySampled.speed[end].z;
		conditions.z.start.acceleration = 
			trajectorySegments[firstSegment-2].trajectorySampled.acceleration[end].z;
		conditions.z.start.jerk = 
			trajectorySegments[firstSegment-2].trajectorySampled.jerk[end].z;
		conditions.z.start.split = 
			trajectorySegments[firstSegment-2].trajectorySampled.split[end].z;

		// YAW-axis
		conditions.yaw.start.speed = 
			trajectorySegments[firstSegment-2].trajectorySampled.yawSpeed[end];
		conditions.yaw.start.acceleration = 
			trajectorySegments[firstSegment-2].trajectorySampled.yawAcc[end];
		conditions.yaw.start.jerk = 
			trajectorySegments[firstSegment-2].trajectorySampled.yawJerk[end];
		conditions.yaw.start.split = 
			trajectorySegments[firstSegment-2].trajectorySampled.yawSplit[end];
	}

	// Now for the last segment in trajectory chunk
	if(lastSegment == trajectorySegments.size())
	{
		DerivativeConditions tempZeroCond;
		tempZeroCond.speed = 0.0;
		tempZeroCond.acceleration = 0.0;
		tempZeroCond.jerk = 0.0;
		tempZeroCond.split = 0.0;
		conditions.x.end = tempZeroCond;
		conditions.y.end = tempZeroCond;
		conditions.z.end = tempZeroCond;
		conditions.yaw.end = tempZeroCond;
	}
	else if(lastSegment == -1)
	{
		cout << "Last segment = -1. Something terribly went wrong." << endl;
	}
	else
	{
		// Here we are taking initial conditions from the next segment.
		// X-axis
		conditions.x.end.speed = 
			trajectorySegments[lastSegment].trajectorySampled.speed[0].x;
		conditions.x.end.acceleration = 
			trajectorySegments[lastSegment].trajectorySampled.acceleration[0].x;
		conditions.x.end.jerk = 
			trajectorySegments[lastSegment].trajectorySampled.jerk[0].x;
		conditions.x.end.split = 
			trajectorySegments[lastSegment].trajectorySampled.split[0].x;

		// Y-axis
		conditions.y.end.speed = 
			trajectorySegments[lastSegment].trajectorySampled.speed[0].y;
		conditions.y.end.acceleration = 
			trajectorySegments[lastSegment].trajectorySampled.acceleration[0].y;
		conditions.y.end.jerk = 
			trajectorySegments[lastSegment].trajectorySampled.jerk[0].y;
		conditions.y.end.split = 
			trajectorySegments[lastSegment].trajectorySampled.split[0].y;

		// Z-axis
		conditions.z.end.speed = 
			trajectorySegments[lastSegment].trajectorySampled.speed[0].z;
		conditions.z.end.acceleration = 
			trajectorySegments[lastSegment].trajectorySampled.acceleration[0].z;
		conditions.z.end.jerk = 
			trajectorySegments[lastSegment].trajectorySampled.jerk[0].z;
		conditions.z.end.split = 
			trajectorySegments[lastSegment].trajectorySampled.split[0].z;

		// YAW-axis
		conditions.yaw.end.speed = 
			trajectorySegments[lastSegment].trajectorySampled.yawSpeed[0];
		conditions.yaw.end.acceleration = 
			trajectorySegments[lastSegment].trajectorySampled.yawAcc[0];
		conditions.yaw.end.jerk = 
			trajectorySegments[lastSegment].trajectorySampled.yawJerk[0];
		conditions.yaw.end.split = 
			trajectorySegments[lastSegment].trajectorySampled.yawSplit[0];
	}
}

void trajectoryPlanning::fixateAndSampleTrajectorySegments(
	std::vector<TrajectorySegment> &trajectorySegments, 
	mav_path_trajectory::TrajectorySampled trajectory, int firstSegment, int lastSegment,
	int scalingSegmentID)
{
	// First initialize extra variables(flags) to keep track if some segments
	// are fixated already. segP are previous segments, segN are next segments
	int segP2, segP1, seg, segN1, segN2;
	bool segP2f, segP1f, segf, segN1f, segN2f;

	// seg is actually offseted by first segment so we have to take that into
	// account.
	seg = scalingSegmentID + firstSegment - 1;
	//cout << "seg: " << seg << endl;
	segP2 = seg - 2;
	segP1 = seg - 1;
	segN1 = seg + 1;
	segN2 = seg + 2;
	//cout << trajectorySegments.size() << endl;
	//cout << segP2 << " " << segP1 << " " << segN1 << " " << segN2 << endl;

	// If previous segments are not in range(for instance if we optimize for
	// second segment) we have to set their scaled flags to true.
	if(segP2 < 1) segP2f = true;
	else segP2f = trajectorySegments[segP2-1].isFixedFlag;
	if(segP1 < 1) segP1f = true;
	else segP1f = trajectorySegments[segP1-1].isFixedFlag;
	if(segN1 > trajectorySegments.size()) segN1f = true;
	else segN1f = trajectorySegments[segN1-1].isFixedFlag;
	if(segN2 > trajectorySegments.size()) segN2f = true;
	else segN2f = trajectorySegments[segN2-1].isFixedFlag;

	//cout << "SegP2 segP1 seg SegN1 segN2" << endl;
	//cout << segP2 << " " << segP1 << " " << seg << " " << segN1 << " " << segN2 << endl;
	//cout << segP2f << " " << segP1f << " " << segf << " " << segN1f << " " << segN2f << endl;

	// Now we can find out which segments have to be fixated. We can't just 
	// fixate segment seg because a following situation may happen:
	// seg is fixated, seg+2 is fixated, seg+1 is not fixated. In next
	// iteration seg+1 will still remain unfixated but it's neighbours will
	// be fixated. We want to avoid that situation because minimum number of
	// segments that Ho-Cook can handle is two and if we let one segment slip
	// we have to introduce higher order polynomial in order to keep up with
	// all conditions.
	// Here we introduce more extra variables that will be set according to the
	// segments that have to be fixated.
	bool segP2fix, segP1fix, segfix, segN1fix, segN2fix;
	segP2fix = false;
	segP1fix = false;
	segfix = false;
	segN1fix = false;
	segN2fix = false;

	if(segP2f == false && segP1f == false && segN1f == false && segN2f == false)
	{
		// All sorrounding segments are not fixated which means that only
		// middle segment has to be fixed.
		segfix = true;
	}
	else if(segP2f == false && segP1f == true && segN1f == false && segN2f == false)
	{
		// Only segP1 is fixated so we have to fixate only middle segment
		// because segN1 and segN2 are still avaliable
		segfix = true;
	}
	else if(segP2f == false && segP1f == false && segN1f == true && segN2f == false)
	{
		// Similar situation as up but segP1 and segP2 are not fixated
		segfix = true;
	}
	else if(segP2f == true && segP1f == false && segN1f == false && segN2f == false)
	{
		// This has more complications that others. Here we enough room for 
		// segN1 and segN2 to be free but segP1 has to be fixated because
		// otherwise it will be left alone
		segP1fix = true;
		segfix = true;
	}
	else if(segP2f == true && segP1f == true && segN1f == false && segN2f == false)
	{
		// Previous segments are fixated, next two are not so only middle has
		// to be fixated
		segfix = true;
	}
	else if(segP2f == true && segP1f == false && segN1f == true && segN2f == false)
	{
		// SegN1 is fixated so we have to fixate segP1 because segP2 is fixated
		// and otherwise it will be left alone
		segP1fix = true;
		segfix = true;
	}
	else if(segP2f == false && segP1f == false && segN1f == false && segN2f == true)
	{
		// We have to fixate segN1 because segN2 is already fixated
		segfix = true;
		segN1fix = true;
	}
	else if(segP2f == false && segP1f == true && segN1f == false && segN2f == true)
	{
		// Similar as up one, but it has previous segment fixated too
		segfix = true;
		segN1fix = true;
	}
	else if(segP2f == false && segP1f == false && segN1f == true && segN2f == true)
	{
		// segN1 and segN2 are fixated so only middle one has to be fixated
		segfix = true;
	}
	else if(segP2f == true && segP1f == false && segN1f == false && segN2f == true)
	{
		// Both segP2 and segN2 are fixated so we have to fixate inner three
		// segments because otherwise some will be left alone
		segP1fix = true;
		segfix = true;
		segN1fix = true;
	}
	else if(segP2f == true && segP1f == true && segN1f == false && segN2f == true)
	{
		// Only two segments (seg and segN1) are left free so both have to be
		// fixated
		segfix = true;
		segN1fix = true;
	}
	else if(segP2f == true && segP1f == false && segN1f == true && segN2f == true)
	{
		// Same as before but segP1 is free now
		segP1fix = true;
		segfix = true;
	}

	/*cout << "SegP2fix = " << segP2fix << endl;
	cout << "SegP1fix = " << segP1fix << endl;
	cout << "Segfix = " << segfix << endl;
	cout << "SegN1fix = " << segN1fix << endl;
	cout << "SegN2fix = " << segN2fix << endl;*/

	// At this point we have all necessary flags that tell us which segments
	// have to be fixated and which don't. Now we can take sampled trajectory
	// and set it in segmentVector and also set the isFixedFlag to true.
	if(segP2fix == true)
	{
		trajectorySegments[segP2-1].isFixedFlag = true;
		for(int i=0; i<trajectory.position.size(); i++)
		{
			if(trajectory.segment[i] == (segP2 - firstSegment + 1))
			{
				trajectorySegments[segP2-1].trajectorySampled.position.push_back(trajectory.position[i]);
				trajectorySegments[segP2-1].trajectorySampled.speed.push_back(trajectory.speed[i]);
				trajectorySegments[segP2-1].trajectorySampled.acceleration.push_back(trajectory.acceleration[i]);
				trajectorySegments[segP2-1].trajectorySampled.jerk.push_back(trajectory.jerk[i]);
				trajectorySegments[segP2-1].trajectorySampled.split.push_back(trajectory.split[i]);
				trajectorySegments[segP2-1].trajectorySampled.yawPos.push_back(trajectory.yawPos[i]);
				trajectorySegments[segP2-1].trajectorySampled.yawSpeed.push_back(trajectory.yawSpeed[i]);
				trajectorySegments[segP2-1].trajectorySampled.yawAcc.push_back(trajectory.yawAcc[i]);
				trajectorySegments[segP2-1].trajectorySampled.yawJerk.push_back(trajectory.yawJerk[i]);
				trajectorySegments[segP2-1].trajectorySampled.yawSplit.push_back(trajectory.yawSplit[i]);
				trajectorySegments[segP2-1].trajectorySampled.segment.push_back(trajectory.segment[i]+firstSegment-1);
			}
		}
	}

	if(segP1fix == true)
	{
		trajectorySegments[segP1-1].isFixedFlag = true;
		for(int i=0; i<trajectory.position.size(); i++)
		{
			if(trajectory.segment[i] == (segP1 - firstSegment + 1))
			{
				trajectorySegments[segP1-1].trajectorySampled.position.push_back(trajectory.position[i]);
				trajectorySegments[segP1-1].trajectorySampled.speed.push_back(trajectory.speed[i]);
				trajectorySegments[segP1-1].trajectorySampled.acceleration.push_back(trajectory.acceleration[i]);
				trajectorySegments[segP1-1].trajectorySampled.jerk.push_back(trajectory.jerk[i]);
				trajectorySegments[segP1-1].trajectorySampled.split.push_back(trajectory.split[i]);
				trajectorySegments[segP1-1].trajectorySampled.yawPos.push_back(trajectory.yawPos[i]);
				trajectorySegments[segP1-1].trajectorySampled.yawSpeed.push_back(trajectory.yawSpeed[i]);
				trajectorySegments[segP1-1].trajectorySampled.yawAcc.push_back(trajectory.yawAcc[i]);
				trajectorySegments[segP1-1].trajectorySampled.yawJerk.push_back(trajectory.yawJerk[i]);
				trajectorySegments[segP1-1].trajectorySampled.yawSplit.push_back(trajectory.yawSplit[i]);
				trajectorySegments[segP1-1].trajectorySampled.segment.push_back(trajectory.segment[i]+firstSegment-1);
			}
		}
	}

	if(segfix == true)
	{
		trajectorySegments[seg-1].isFixedFlag = true;
		for(int i=0; i<trajectory.position.size(); i++)
		{
			if(trajectory.segment[i] == (seg - firstSegment + 1))
			{
				trajectorySegments[seg-1].trajectorySampled.position.push_back(trajectory.position[i]);
				trajectorySegments[seg-1].trajectorySampled.speed.push_back(trajectory.speed[i]);
				trajectorySegments[seg-1].trajectorySampled.acceleration.push_back(trajectory.acceleration[i]);
				trajectorySegments[seg-1].trajectorySampled.jerk.push_back(trajectory.jerk[i]);
				trajectorySegments[seg-1].trajectorySampled.split.push_back(trajectory.split[i]);
				trajectorySegments[seg-1].trajectorySampled.yawPos.push_back(trajectory.yawPos[i]);
				trajectorySegments[seg-1].trajectorySampled.yawSpeed.push_back(trajectory.yawSpeed[i]);
				trajectorySegments[seg-1].trajectorySampled.yawAcc.push_back(trajectory.yawAcc[i]);
				trajectorySegments[seg-1].trajectorySampled.yawJerk.push_back(trajectory.yawJerk[i]);
				trajectorySegments[seg-1].trajectorySampled.yawSplit.push_back(trajectory.yawSplit[i]);
				trajectorySegments[seg-1].trajectorySampled.segment.push_back(trajectory.segment[i]+firstSegment-1);
			}
		}
	}

	if(segN1fix == true)
	{
		trajectorySegments[segN1-1].isFixedFlag = true;
		for(int i=0; i<trajectory.position.size(); i++)
		{
			if(trajectory.segment[i] == (segN1 - firstSegment + 1))
			{
				trajectorySegments[segN1-1].trajectorySampled.position.push_back(trajectory.position[i]);
				trajectorySegments[segN1-1].trajectorySampled.speed.push_back(trajectory.speed[i]);
				trajectorySegments[segN1-1].trajectorySampled.acceleration.push_back(trajectory.acceleration[i]);
				trajectorySegments[segN1-1].trajectorySampled.jerk.push_back(trajectory.jerk[i]);
				trajectorySegments[segN1-1].trajectorySampled.split.push_back(trajectory.split[i]);
				trajectorySegments[segN1-1].trajectorySampled.yawPos.push_back(trajectory.yawPos[i]);
				trajectorySegments[segN1-1].trajectorySampled.yawSpeed.push_back(trajectory.yawSpeed[i]);
				trajectorySegments[segN1-1].trajectorySampled.yawAcc.push_back(trajectory.yawAcc[i]);
				trajectorySegments[segN1-1].trajectorySampled.yawJerk.push_back(trajectory.yawJerk[i]);
				trajectorySegments[segN1-1].trajectorySampled.yawSplit.push_back(trajectory.yawSplit[i]);
				trajectorySegments[segN1-1].trajectorySampled.segment.push_back(trajectory.segment[i]+firstSegment-1);
			}
		}
	}

	if(segN2fix == true)
	{
		trajectorySegments[segN2-1].isFixedFlag = true;
		for(int i=0; i<trajectory.position.size(); i++)
		{
			if(trajectory.segment[i] == (segN2 - firstSegment + 1))
			{
				trajectorySegments[segN2-1].trajectorySampled.position.push_back(trajectory.position[i]);
				trajectorySegments[segN2-1].trajectorySampled.speed.push_back(trajectory.speed[i]);
				trajectorySegments[segN2-1].trajectorySampled.acceleration.push_back(trajectory.acceleration[i]);
				trajectorySegments[segN2-1].trajectorySampled.jerk.push_back(trajectory.jerk[i]);
				trajectorySegments[segN2-1].trajectorySampled.split.push_back(trajectory.split[i]);
				trajectorySegments[segN2-1].trajectorySampled.yawPos.push_back(trajectory.yawPos[i]);
				trajectorySegments[segN2-1].trajectorySampled.yawSpeed.push_back(trajectory.yawSpeed[i]);
				trajectorySegments[segN2-1].trajectorySampled.yawAcc.push_back(trajectory.yawAcc[i]);
				trajectorySegments[segN2-1].trajectorySampled.yawJerk.push_back(trajectory.yawJerk[i]);
				trajectorySegments[segN2-1].trajectorySampled.yawSplit.push_back(trajectory.yawSplit[i]);
				trajectorySegments[segN2-1].trajectorySampled.segment.push_back(trajectory.segment[i]+firstSegment-1);
			}
		}
	}

}

void printConditions(TrajectoryDerivativeConditions c)
{	

	cout << "Start: " << endl;
	cout << "     Speed Acc Jerk" << endl;
	cout << "  x: " << c.x.start.speed << " " << c.x.start.acceleration << " " << c.x.start.jerk << endl;
	cout << "  y: " << c.y.start.speed << " " << c.y.start.acceleration << " " << c.y.start.jerk << endl;
	cout << "  z: " << c.z.start.speed << " " << c.z.start.acceleration << " " << c.z.start.jerk << endl;
	cout << "yaw: " << c.yaw.start.speed << " " << c.yaw.start.acceleration << " " << c.yaw.start.jerk << endl;

	cout << "End: " << endl;
	cout << "       Speed           Acc         Jerk" << endl;
	cout << "  x: " << c.x.end.speed << " " << c.x.end.acceleration << " " << c.x.end.jerk << endl;
	cout << "  y: " << c.y.end.speed << " " << c.y.end.acceleration << " " << c.y.end.jerk << endl;
	cout << "  z: " << c.z.end.speed << " " << c.z.end.acceleration << " " << c.z.end.jerk << endl;
	cout << "yaw: " << c.yaw.end.speed << " " << c.yaw.end.acceleration << " " << c.yaw.end.jerk << endl;
}