#include <mav_path_trajectory/mav_mission_planner.h>

MissionPlanner::MissionPlanner(void)
{
    mapReadFlag = false; //To wait while the map is processed

    path = NULL;

    map2D = NULL;

    map2Dframeid = "/map";

    nhPrivate = ros::NodeHandle("~");

    nhPrivate.param("max_quadrotor_speed", maxQuadrotorSpeed, double(1.5));
    nhPrivate.param("max_quadrotor_acceleration", maxQuadrotorAcceleration, double(2.0));
    nhPrivate.param("trajectory_sampling_frequency", trajectoryRate, int(100));
    nhPrivate.param("node_frequency", rate, int(20));
    nhPrivate.param("minimum_segment_length", minSegmentLength, double(0.2));
    nhPrivate.param("maximum_trajectory_offset", maxTrajectoryOffset, double(0.1));
    nhPrivate.param("planning_time_out", planningTimeOut, double(10));
    
    nhPrivate.param("horizontClearance_vmax",hclear_vmax,double(0.2));
    nhPrivate.param("horizontClearance_Rmax",hclear_rmax,double(0.6));
    nhPrivate.param("horizontClearance_Rmin",hclear_rmin,double(0.4));
    nhPrivate.param("horizontClearance_Fimax",hclear_fimax,double(3.14/3));
    nhPrivate.param("horizontClearance_Fimin",hclear_fimin,double(-3.14/3));
    nhPrivate.param("horizontClearance_Gain",hclear_gain,double(0.5));
    nhPrivate.param("joyFlag",joystickEnabled,bool(false));
    nhPrivate.param("joy_headingREFScale", joy_headingREFScale, double(1.0));
    nhPrivate.param("joy_velocityREFScale", joy_velocityREFScale, double(1.0));

    nhPrivate.param("octomap_search_depth", octomap_depth,int(16));
    // Map boundaries
    nhPrivate.param("map_min_x", rrtMapMinX, double(-5.0));
    nhPrivate.param("map_max_x", rrtMapMaxX, double( 5.0));
    nhPrivate.param("map_min_y", rrtMapMinY, double(-5.0));
    nhPrivate.param("map_max_y", rrtMapMaxY, double( 5.0));
    nhPrivate.param("map_min_z", rrtMapMinZ, double(-5.0));
    nhPrivate.param("map_max_z", rrtMapMaxZ, double( 5.0));


    nhPrivate.param("typeUAV",uav_type,string("arducopter"));
    ROS_INFO("Started controller for: ");
    ROS_INFO("%s", uav_type.c_str());
    ugv2DOffset, ugvTakeoffLandHeight;
    nhPrivate.param("ugv_offset_2D", ugv2DOffset, double(0.4));
    nhPrivate.param("ugv_takeoff_land_height", ugvTakeoffLandHeight, double(0.75));

    assessPathService = n.advertiseService("assess_path", &MissionPlanner::assess_path, this);

    getLiftPointsService = n.advertiseService("get_lift_points", &MissionPlanner::get_lift_points, this);
    planTakeoffAndLandPointsService = n.advertiseService("plan_takeoff_and_land_points", 
      &MissionPlanner::PlanTakeoffAndLandPoints, this);
    getTakeoffAndLandPointsService = n.advertiseService("get_takeoff_and_land_points", 
      &MissionPlanner::GetTakeoffAndLandPoints, this);
    getPathService = n.advertiseService("get_path", 
      &MissionPlanner::GetPath, this);

    trajectoryPub = n.advertise<mav_path_trajectory::TrajectorySampled>("trajectory", 1);
    pointPub = n.advertise<geometry_msgs::PoseArray>("points",1);

    waypointPositionService = n.advertiseService("positionWaypoint", &MissionPlanner::WaypointPositionCallback, this);
    waypointOrientationService = n.advertiseService("orientationWaypoint", &MissionPlanner::WaypointOrientationCallback, this);

    trajectoryPlanner = trajectoryPlanning();
    trajectoryPlanner.setTrajectorySamplingFrequency(trajectoryRate);
    trajectoryPlanner.setArducopterMaxSpeed(maxQuadrotorSpeed);
    trajectoryPlanner.setArducopterMaxAcc(maxQuadrotorAcceleration);

    stepLength = 0.1;
    distanceFromObstacle = 1;

    // NEO adaptation
    neoTrajectoryPub = n.advertise<trajectory_msgs::MultiDOFJointTrajectory>(
      "/euroc3/command/trajectory", 1);
    waypointOrientationService = n.advertiseService("positionWaypointArray", 
      &MissionPlanner::WaypointArrayPositionCallback, this);
    markerArrayPub = n.advertise<visualization_msgs::MarkerArray>(
      "markerArray", 1);
    markerPub = n.advertise<visualization_msgs::Marker>(
      "marker", 1);
    pathPub = n.advertise<nav_msgs::Path>("rrtPath", 1);
    
    //Testing purposes
    textPub =  n.advertise<visualization_msgs::Marker>(
      "text", 1);
    pointCheckService = n.advertiseService("pointCheckService", 
      &MissionPlanner::pointCheckCallback, this);
    
    emptyService = n.advertiseService("SaveOctomap",&MissionPlanner::emptyCallback,this);
    
    //OCTOMAP additions
    octomapSub=n.subscribe("/octomap_binary", 1000, &MissionPlanner::octomapCallback,this);
    
    sub2DMap = n.subscribe("/alpha/map", 1, &MissionPlanner::mapReceivedCallback, this);
    
    //JOY additions
    joySub = n.subscribe("/joy",1,&MissionPlanner::joyCallback,this);
    
    //Overloaded sbuscriber - different pose callbacks depending if you use arducopter (simulation) or neo, please set parameter uav_type (default = arducopter)
    if (uav_type == "arducopter") poseSub = n.subscribe("ground_truth/pose",1,static_cast<void (MissionPlanner::*)(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr&)>(&MissionPlanner::poseCallback),this);
    else poseSub = n.subscribe("msf_core/odometry",1,static_cast<void (MissionPlanner::*)(const nav_msgs::Odometry::ConstPtr&)>(&MissionPlanner::poseCallback),this);
}

MissionPlanner::MissionPlanner(string filespec)
{
    mapReadFlag = false; //To wait while the map is processed
    read_binvox(filespec);

    path = NULL;
    map2D = NULL;
    map2Dframeid = "/map";

    nhPrivate = ros::NodeHandle("~");

    nhPrivate.param("max_quadrotor_speed", maxQuadrotorSpeed, double(1.5));
    nhPrivate.param("max_quadrotor_acceleration", maxQuadrotorAcceleration, double(2.0));
    nhPrivate.param("trajectory_sampling_frequency", trajectoryRate, int(100));
    nhPrivate.param("node_frequency", rate, int(20));
    nhPrivate.param("minimum_segment_length", minSegmentLength, double(0.2));
    nhPrivate.param("maximum_trajectory_offset", maxTrajectoryOffset, double(0.03));
    nhPrivate.param("planning_time_out", planningTimeOut, double(10));

    assessPathService = n.advertiseService("assess_path", &MissionPlanner::assess_path, this);
    getLiftPointsService = n.advertiseService("get_lift_points", &MissionPlanner::get_lift_points, this);
    getTakeoffAndLandPointsService = n.advertiseService("get_takeoff_and_land_points", 
      &MissionPlanner::GetTakeoffAndLandPoints, this);
    getPathService = n.advertiseService("get_path", 
      &MissionPlanner::GetPath, this);

    trajectoryPub = n.advertise<mav_path_trajectory::TrajectorySampled>("trajectory", 1);
    pointPub = n.advertise<geometry_msgs::PoseArray>("points",1);

    waypointPositionService = n.advertiseService("positionWaypoint", &MissionPlanner::WaypointPositionCallback, this);
    waypointOrientationService = n.advertiseService("orientationWaypoint", &MissionPlanner::WaypointOrientationCallback, this);

    trajectoryPlanner = trajectoryPlanning();
    trajectoryPlanner.setTrajectorySamplingFrequency(trajectoryRate);
    trajectoryPlanner.setArducopterMaxSpeed(maxQuadrotorSpeed);
    trajectoryPlanner.setArducopterMaxAcc(maxQuadrotorAcceleration);

    stepLength = 0.1;
    distanceFromObstacle = 1;

    // NEO adaptation
    neoTrajectoryPub = n.advertise<trajectory_msgs::MultiDOFJointTrajectory>(
      "/euroc3/command/trajectory", 1);
    sendCircleRadius = n.advertiseService("circleRadius", 
      &MissionPlanner::SendCircleRadiusCallback, this);
    markerArrayPub = n.advertise<visualization_msgs::MarkerArray>(
      "markerArray", 1);
    markerPub = n.advertise<visualization_msgs::Marker>(
      "marker", 1);
    //This is for testing purposes
    textPub =  n.advertise<visualization_msgs::Marker>(
      "text", 1);
    pathPub = n.advertise<nav_msgs::Path>("rrtPath", 1);


}

void MissionPlanner::run(void)
{
    ros::Rate loop_rate(rate);
    int joy_rate_index = rate/5; //JOY works at 5Hz
    int joy_rate_counter = 0;
    tf::Transform transform;
    static tf::TransformBroadcaster br;

    while(ros::ok())
    {
      ros::spinOnce();
      if(joystickEnabled)
      {
        joy_rate_counter++;
        if (joy_rate_counter > (joy_rate_index-1))
        {
          if (abs(MissionPlanner::joy_velocityREF)>0.005 || abs(MissionPlanner::joy_slideREF)>0.15 || abs(MissionPlanner::joy_headingREF)>0.01)
          {
            joy_rate_counter = 0;
            MissionPlanner::CalculateNextJoyWaypoint();
          }
        }
      }

      tf::Vector3 vect = tf::Vector3(map2DoriginX, map2DoriginY, 0.0);
      tf::Quaternion quat = tf::Quaternion(0, 0, 0, 1);
  
      transform.setOrigin(vect);
      transform.setRotation(quat);
  
      br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), map2Dframeid, "alpha/map"));

      loop_rate.sleep();
    }
}

void MissionPlanner::joyCallback(const sensor_msgs::JoyConstPtr& msg)
{
  // We want to fly UAV always looking in front of the cameras
  // Therefore we move it forward/backward and turn left/right
  //
  MissionPlanner::joy_velocityREF = msg->axes[4];
  MissionPlanner::joy_headingREF = msg->axes[0];
  MissionPlanner::joy_slideREF = msg->axes[3];
  
}

void MissionPlanner::poseCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  // This function subscribes to NEO odometry msg to detect the position of the UAV
  // It takes quaternions and transforms it to Euler which is used in the planner (yaw angle)
    MissionPlanner::current_pose_x = msg->pose.pose.position.x;
    MissionPlanner::current_pose_y = msg->pose.pose.position.y;
    MissionPlanner::current_pose_z = msg->pose.pose.position.z;
    //orientation data retrieval in quaternion
    double qx = msg->pose.pose.orientation.x;
    double qy = msg->pose.pose.orientation.y;
    double qz = msg->pose.pose.orientation.z;
    double qw = msg->pose.pose.orientation.w;

    //transformation from quaternion to euler
    MissionPlanner::current_imu_roll = atan2(2 * (qw * qx + qy * qz), qw * qw - qx * qx - qy * qy + qz * qz);
    MissionPlanner::current_imu_pitch = -asin(2 * (qx * qz - qw * qy));
    MissionPlanner::current_imu_yaw = atan2(2 * (qw * qz + qx * qy), qw * qw + qx * qx - qy * qy - qz * qz);
}

void MissionPlanner::poseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg)
{
  // This function subscribes to Arducopter pose msg to detect the position of the UAV in Gazebo
  // It takes quaternions and transforms it to Euler which is used in the planner (yaw angle)
    MissionPlanner::current_pose_x = msg->pose.pose.position.x;
    MissionPlanner::current_pose_y = msg->pose.pose.position.y;
    MissionPlanner::current_pose_z = msg->pose.pose.position.z;
    //ROS_INFO("x= %f y= %f z= %f \n",current_pose_x,current_pose_y,current_pose_z);
    //orientation data retrieval in quaternion
    double qx = msg->pose.pose.orientation.x;
    double qy = msg->pose.pose.orientation.y;
    double qz = msg->pose.pose.orientation.z;
    double qw = msg->pose.pose.orientation.w;

    //transformation from quaternion to euler
    MissionPlanner::current_imu_roll = atan2(2 * (qw * qx + qy * qz), qw * qw - qx * qx - qy * qy + qz * qz);
    MissionPlanner::current_imu_pitch = -asin(2 * (qx * qz - qw * qy));
    MissionPlanner::current_imu_yaw = atan2(2 * (qw * qz + qx * qy), qw * qw + qx * qx - qy * qy - qz * qz);
}

bool MissionPlanner::pointCheckCallback(mav_path_trajectory::IsPointValid::Request& req, mav_path_trajectory::IsPointValid::Response& res)
{
  //This is a service request to verify certain point (service is depricated, used for testing purposes only)
    //ob::StateSpacePtr space(new ob::SE3StateSpace());
    //ob::ScopedState<ob::SE3StateSpace> position(space);
    //position->setXYZ(req.point.linear.x,req.point.linear.y,req.point.linear.z);
    geometry_msgs::PoseStamped temp2DState;

      temp2DState.pose.position.x = req.point.linear.x;
      temp2DState.pose.position.y = req.point.linear.y;
 
    res.status=is2DPointValid(temp2DState, 0.0, 0.0); //isStateValid(tempState(), 13);
    return res.status;
}

bool MissionPlanner::get_lift_points(mav_path_trajectory::GetLiftPoints::Request &req, mav_path_trajectory::GetLiftPoints::Response &res)
{
  ob::StateSpacePtr space(new ob::SE3StateSpace());

  ob::ScopedState<ob::SE3StateSpace> start(space);
  ob::ScopedState<ob::SE3StateSpace> goal(space);

  start->setXYZ(req.start.x, req.start.y, 0);
  goal->setXYZ(req.end.x, req.end.y, 0);
  start->rotation().setAxisAngle(0, 0, 0, 1);
  goal->rotation().setAxisAngle(0, 0, 0, 1);

  double Ax, Ay, Az, Bx, By, Bz, pTx, pTy, pTz, dAB, tPoint;
  bool pointValid, secondPointFound, firstPointFound;

  firstPointFound = false;
  secondPointFound = false;

  Ax = req.start.x;
  Ay = req.start.y;
  Az = 0.05;
  Bx = req.end.x;
  By = req.end.y;
  Bz = 0.05;

  dAB = sqrt(pow((Bx-Ax),2)+ pow((By-Ay),2) + pow((Bz-Az),2));

  if (!isStateValid(start()) || !isStateValid(goal()))
  {
    res.status = 0;
  }
  else
  {
    double step = 1./(dAB/stepLength);
    for (double t=0; t<=(1 + step/10.);t+=step)
    {
      pTx = (Bx - Ax)*t + Ax;
      pTy = (By - Ay)*t + Ay;
      pTz = (Bz - Az)*t + Az;

      ob::ScopedState<ob::SE3StateSpace> point(space);

      point->setXYZ(pTx, pTy, pTz);
      point->rotation().setAxisAngle(0, 0, 0, 1);

      pointValid = isStateValid(point());

      if (!pointValid && !firstPointFound)
      {
        firstPointFound = true;
        tPoint = t - step*(distanceFromObstacle/stepLength);
        if (tPoint<0) tPoint = 0;
        res.t1.x = (Bx - Ax)*tPoint + Ax;
        res.t1.y = (By - Ay)*tPoint + Ay;
        res.t1.z = (Bz - Az)*tPoint + Az;
      }

      if (pointValid && firstPointFound && !secondPointFound)
      {
        secondPointFound = true;
        tPoint = t + step*(distanceFromObstacle/stepLength - 1);
        if (tPoint>1) tPoint = 1;
        res.t2.x = (Bx - Ax)*tPoint + Ax;
        res.t2.y = (By - Ay)*tPoint + Ay;
        res.t2.z = (Bz - Az)*tPoint + Az;
        break;
      }
    }

    if (firstPointFound && secondPointFound) res.status = 1;
    else res.status = 2;
  }
  

  return true;
}

bool MissionPlanner::GetTakeoffAndLandPoints(
  mav_path_trajectory::GetTakeoffAndLandPoints::Request &req, 
    mav_path_trajectory::GetTakeoffAndLandPoints::Response &res)
{
  for (int i=0; i<takeoffPoints.size(); i++) res.takeoffPoints.push_back(takeoffPoints[i]);
  for (int i=0; i<landPoints.size(); i++) res.landPoints.push_back(landPoints[i]);
  for (int i=0; i<obstacleHeight.size(); i++) res.obstacleHeight.push_back(obstacleHeight[i]);

  res.grabPackageTakeoffPoint = grabPackageTakeoffPoint;

  res.goal = takeoffAndLAndPointGoal;

  return true;
}

bool MissionPlanner::PlanTakeoffAndLandPoints(
  mav_path_trajectory::GetTakeoffAndLandPoints::Request &req, 
    mav_path_trajectory::GetTakeoffAndLandPoints::Response &res)
{
  double dt = 0;
    bool plan_flag = true;
    bool startValid, endValid;
    ob::PlannerStatus solved;
    //planner setup

    geometry_msgs::Vector3 StartPoint, EndPoint;

    landPoints.clear();
    takeoffPoints.clear();
    obstacleHeight.clear();

    takeoffAndLAndPointGoal = req.goal;

    StartPoint.x = req.start.position.x;
    StartPoint.y = req.start.position.y;
    StartPoint.z = req.start.position.z;
    EndPoint.x = req.goal.position.x;
    EndPoint.y = req.goal.position.y;
    EndPoint.z = req.goal.position.z;

    //create an SE3 state space
    ob::StateSpacePtr space(new ob::SE3StateSpace());

    //set lower and upper bounds
    ob::RealVectorBounds bounds(3);

    //bounds.setLow(0, -2.75);
    //bounds.setHigh(0, 1.34);
    //bounds.setLow(1, -3.35);
    //bounds.setHigh(1, 0.62);
    //bounds.setLow(2, 0.1);
    //bounds.setHigh(2, 1.1);
    bounds.setLow( 0, rrtMapMinX);
    bounds.setHigh(0, rrtMapMaxX);
    bounds.setLow( 1, rrtMapMinY);
    bounds.setHigh(1, rrtMapMaxY);
    bounds.setLow( 2, rrtMapMinZ);
    bounds.setHigh(2, rrtMapMaxZ);
    // HACK: set bounds for z axis from request start z
    //bounds.setLow( 2, req.start.position.z);
    //bounds.setHigh(2, req.start.position.z);

    space->as<ob::SE3StateSpace>()->setBounds(bounds);

    //create a simple setup object
    og::SimpleSetup ss(space);
    ss.setStateValidityChecker(boost::bind(&MissionPlanner::isStateValid_withRadius, this, _1));
    //space->as<ob::SE3StateSpace>()->setLongestValidSegmentFraction(0.01/space->as<ob::SE3StateSpace>()->getMaximumExtent());
    //space->as<ob::SE3StateSpace>()->setLongestValidSegmentFraction(0.5*_octomap->getResolution()/space->as<ob::SE3StateSpace>()->getMaximumExtent());
    space->as<ob::SE3StateSpace>()->setLongestValidSegmentFraction(0.01/space->as<ob::SE3StateSpace>()->getMaximumExtent());

    // (optionally) set planner
    ob::SpaceInformationPtr si;
    si = ss.getSpaceInformation();

    space_ = si->getStateSpace();
    //ob::OptimizationObjective optimizationObjective(si);

    ob::PlannerPtr planner(new og::RRTstar(si));
    planner->as<og::RRTstar>()->setPrune(true);
    planner->as<og::RRTstar>()->setGoalBias(0.05);
    planner->as<og::RRTstar>()->setRange(5.0);

    planner->as<og::RRTstar>()->setPruneStatesImprovementThreshold(0.1);
    ss.setPlanner(planner);

    ob::ScopedState<ob::SE3StateSpace> start(space);
    //we can pick a random start state
    // ... or set specific values
    start->setXYZ(StartPoint.x, StartPoint.y, StartPoint.z);
    start->rotation().setAxisAngle(0, 0, 0, 1);

    ob::ScopedState<ob::SE3StateSpace> goal(space);
    //we can pick a random goal state
    // ... or set specific values
    goal->setXYZ(EndPoint.x, EndPoint.y, EndPoint.z);
    goal->rotation().setAxisAngle(0, 0, 0, 1);

    ss.setStartAndGoalStates(start, goal);

    checkStart = StartPoint;
    checkEnd = EndPoint;

    startValid = isStateValid_withRadius(start());
    endValid = isStateValid_withRadius(goal());
    //cout << EndPoint << endl;

    if (!startValid || !endValid)
    {
        if (!startValid) ROS_WARN("Invalid start point.");
        if (!endValid) ROS_WARN("Invalid end point.");
        return -1;
    }

    while(plan_flag)
    {
        dt += 0.3;
        solved = ss.solve(dt);
        if (dt > 10) break;
        if (solved)
        {
            path = &ss.getSolutionPath();
            pt_num = path->getStateCount();
            ob::State *end_point = path->getState(pt_num-1);
            plan_flag = !(abs(end_point->as<ob::SE3StateSpace::StateType>()->getX() - EndPoint.x) < 0.05 && abs(end_point->as<ob::SE3StateSpace::StateType>()->getY() - EndPoint.y) < 0.05 &&
                abs(end_point->as<ob::SE3StateSpace::StateType>()->getZ() - EndPoint.z) < 0.05);
        }
    }
    if (plan_flag)
    {
        ROS_WARN("Path plan failed!");
        return -1;
    }

    // Publish path
    nav_msgs::Path visPath;
    visPath.header.stamp = ros::Time::now();
    visPath.header.frame_id = "world";
    for(int i=0; i<pt_num; i++)
    {
      geometry_msgs::PoseStamped tempPoseStamped;
      tempPoseStamped.pose.orientation.w = 1.0;
      tempPoseStamped.pose.position.x = path->getState(i)->as<ob::SE3StateSpace::StateType>()->getX();
      tempPoseStamped.pose.position.y = path->getState(i)->as<ob::SE3StateSpace::StateType>()->getY();
      tempPoseStamped.pose.position.z = path->getState(i)->as<ob::SE3StateSpace::StateType>()->getZ()*0;
      visPath.poses.push_back(tempPoseStamped);
    }
    pathPub.publish(visPath);

    
    takeoffAndLAndPointGoal.orientation = tf::createQuaternionMsgFromYaw( atan2(path->getState(pt_num-1)->as<ob::SE3StateSpace::StateType>()->getY() - 
      path->getState(pt_num-2)->as<ob::SE3StateSpace::StateType>()->getY(), path->getState(pt_num-1)->as<ob::SE3StateSpace::StateType>()->getX() - 
      path->getState(pt_num-2)->as<ob::SE3StateSpace::StateType>()->getX()));   


    // At this point path is planned for UAV. Now we have to check that pat for
    // UGV, that means z=0.02 for isStateValid check
    double sampleStep = 0.1;//_octomap->getResolution();
    int sampleOffset = ceil(ugv2DOffset/sampleStep);
    cout << "Sample step: " << sampleStep << " Sample offset: " << sampleOffset << endl;
    std::vector<geometry_msgs::Vector3> sampledPath;
    geometry_msgs::Vector3 tempVector3;

    ob::ScopedState<ob::SE3StateSpace> tempState(space);
    tempState->rotation().setAxisAngle(0, 0, 0, 1);
    tempState->setXYZ(0,0,0);
    //cout << pt_num << endl;

    for(int i=0; i<pt_num-1; i++)
    {
      // We will sample path with sampleStep and search for points that
      // are hitting the obstacle. After first obstacle hit we will take
      // a point that is behind the first hitpoint for sampleOffset
      // points and use that as takeoff point. After that we sample again
      // and find the first point after obstacle. We then sample a bit
      // further for sampleOffset-1 and take that point as landing.
      double Dx = path->getState(i+1)->as<ob::SE3StateSpace::StateType>()->getX() - 
        path->getState(i)->as<ob::SE3StateSpace::StateType>()->getX();
      double Dy = path->getState(i+1)->as<ob::SE3StateSpace::StateType>()->getY() - 
        path->getState(i)->as<ob::SE3StateSpace::StateType>()->getY();
      double D = sqrt(Dx*Dx + Dy*Dy);

      int n = int(floor(D/sampleStep));
      //cout << n << endl;

      if (n>0)
      {
        double dx = Dx/double(n);
        double dy = Dy/double(n);

        for(int j=0; j<n; j++)
        {
          tempVector3.x = 
            path->getState(i)->as<ob::SE3StateSpace::StateType>()->getX() + 
              double(j)*dx;

          tempVector3.y = 
            path->getState(i)->as<ob::SE3StateSpace::StateType>()->getY() + 
              double(j)*dy;
          tempVector3.z = 0.25;
          sampledPath.push_back(tempVector3);
        }
      }
    }
    // Add last point to sampled path
    tempVector3.x = path->getState(pt_num-1)->as<ob::SE3StateSpace::StateType>()->getX();
    tempVector3.y = path->getState(pt_num-1)->as<ob::SE3StateSpace::StateType>()->getY();
    tempVector3.z = 0.25;
    sampledPath.push_back(tempVector3);

    //cout << path->length() << ", " << sampledPath.size() << endl;
    bool inObstacleFlag = false;

    for(int i=0; i<sampledPath.size(); i++)
    {
      // Create state
      tempState->setXYZ(sampledPath[i].x, sampledPath[i].y, sampledPath[i].z);
      //added for 2DMap
      geometry_msgs::PoseStamped temp2DState;

      temp2DState.pose.position.x = sampledPath[i].x;
      temp2DState.pose.position.y = sampledPath[i].y;

      bool isValidFlag = is2DPointValid(temp2DState, 0.0, 0.0); //isStateValid(tempState(), 13);
      
      if ((isValidFlag==true && inObstacleFlag==false) || 
        (isValidFlag==false && inObstacleFlag==true))
      {
        // Do nothing, continue searching for obstacle. 
      }

      else if (isValidFlag==false && inObstacleFlag==false)
      {
        // Obstacle is hit for the first time
        inObstacleFlag = true;
        // Take point that is sample offset points before we hit the obstacle
        // for the first time.
        int a = i-sampleOffset;
        if (a<0) a=0;
        // Create takeoff point
        geometry_msgs::Pose tempTakeoff;
        tempTakeoff.position.x = sampledPath[a].x; //tempState->getX();
        tempTakeoff.position.y = sampledPath[a].y; //tempState->getY();
        tempTakeoff.position.z = ugvTakeoffLandHeight;

        while(a>0)
        {
          temp2DState.pose.position.x = sampledPath[a].x;
          temp2DState.pose.position.y = sampledPath[a].y;
          if(is2DPointValid(temp2DState, ugv2DOffset, ugv2DOffset)) break;
          a--;
        }
        tempTakeoff.position.x = sampledPath[a].x;
        tempTakeoff.position.y = sampledPath[a].y;

        tempTakeoff.orientation.w = 1;
        if (a-1>=0)
        {
          float yaw;
          yaw = atan2(sampledPath[a].y - sampledPath[a-1].y, sampledPath[a].x - sampledPath[a-1].x);
          tempTakeoff.orientation = tf::createQuaternionMsgFromYaw( yaw );       
        }
        takeoffPoints.push_back(tempTakeoff);

        // TODO: obstacle height
      }

      else if (isValidFlag==true && inObstacleFlag==true)
      {
        // Exiting obstacle 
        inObstacleFlag = false;
        int a = i+sampleOffset-1;
        if (a>(sampledPath.size()-1)) a = sampledPath.size()-1;
        // Create land point
        geometry_msgs::Pose tempLand;
        tempLand.position.x = sampledPath[a].x; //tempState->getX();
        tempLand.position.y = sampledPath[a].y; //tempState->getY();
        tempLand.position.z = ugvTakeoffLandHeight;

        while(a<(sampledPath.size()-1))
        {
          temp2DState.pose.position.x = sampledPath[a].x;
          temp2DState.pose.position.y = sampledPath[a].y;
          if(is2DPointValid(temp2DState, ugv2DOffset, ugv2DOffset)) break;
          a++;
        }
        tempLand.position.x = sampledPath[a].x;
        tempLand.position.y = sampledPath[a].y;
        tempLand.orientation.w = 1;
        if (a-1>=0)
        {
          float yaw;
          yaw = atan2(sampledPath[a].y - sampledPath[a-1].y, sampledPath[a].x - sampledPath[a-1].x);
          tempLand.orientation = tf::createQuaternionMsgFromYaw( yaw );       
        }

        landPoints.push_back(tempLand);
      }
      if (inObstacleFlag==true)
      {
        //cout << sampledPath[i] << endl << endl;
      }
    }

  // Find grabPackageTakeoffPoint
  for(int i=sampledPath.size()-1; i>=0; i--)
  {
    double distToGoal2D = sqrt(
      pow(sampledPath[i].x - sampledPath[sampledPath.size()-1].x, 2.0) + 
      pow(sampledPath[i].y - sampledPath[sampledPath.size()-1].y, 2.0));
    if(distToGoal2D > ugv2DOffset)
    {
      geometry_msgs::PoseStamped temp2DState;
      temp2DState.pose.position.x = sampledPath[i].x;
      temp2DState.pose.position.y = sampledPath[i].y;
      if(is2DPointValid(temp2DState, ugv2DOffset, ugv2DOffset))
      {
        grabPackageTakeoffPoint.position.x = temp2DState.pose.position.x;
        grabPackageTakeoffPoint.position.y = temp2DState.pose.position.y;
        grabPackageTakeoffPoint.position.z = ugvTakeoffLandHeight;
        double dx = sampledPath[i+1].x - sampledPath[i].x;
        double dy = sampledPath[i+1].y - sampledPath[i].y;
        grabPackageTakeoffPoint.orientation = tf::createQuaternionMsgFromYaw(
          atan2(dy, dx));
        break;
      }
    }
  }
  res.takeoffPoints = takeoffPoints;
  res.landPoints = landPoints;
  // Remove takeoff and land points if the point is close to them
  cout << grabPackageTakeoffPoint << endl;
  for(int i=0; i<takeoffPoints.size(); i++)
  {
    double distToTakeoff = sqrt(
      pow(takeoffPoints[i].position.x - grabPackageTakeoffPoint.position.x, 2.0) + 
      pow(takeoffPoints[i].position.y - grabPackageTakeoffPoint.position.y, 2.0));

    if(distToTakeoff < 0.05)
    {
      //Remove takeoff and land point
      takeoffPoints.erase(takeoffPoints.begin()+i);
      landPoints.erase(landPoints.begin()+i);
      break;
    }
  }

  // Visualization markers
  visualization_msgs::Marker marker;
  marker.header.stamp = ros::Time::now();
  marker.header.frame_id = std::string("world");
  marker.id = 1;
  marker.ns = "markeri";
  marker.type = 7; //Sphere list
  marker.action = 0;
  marker.pose.orientation.w = 1;
  geometry_msgs::Vector3 tempScale;
  tempScale.x = 0.05;
  tempScale.y = 0.05;
  tempScale.z = 0.05;
  marker.scale = tempScale;
  std_msgs::ColorRGBA tempColorRGBA;
  tempColorRGBA.b = 1;
  tempColorRGBA.a = 1;
  marker.lifetime = ros::Duration(10000);
  marker.color.b = 1;
  marker.color.a = 1;

  tempColorRGBA.r = 1;
  tempColorRGBA.b = 0;
  tempColorRGBA.a = 1;
  /*for(int i=0; i<sampledPath.size(); i++)
  {
    geometry_msgs::Point tempVisPoint;
    tempVisPoint.x = sampledPath[i].x;
    tempVisPoint.y = sampledPath[i].y;
    tempVisPoint.z = 0;
    marker.points.push_back(tempVisPoint);
    marker.colors.push_back(tempColorRGBA);
  }*/

  tempColorRGBA.r = 0;
  tempColorRGBA.b = 1;
  tempColorRGBA.a = 1;
  for(int i=0; i<takeoffPoints.size(); i++)
  {
    geometry_msgs::Point tmpVisPoint;
    tmpVisPoint.x = takeoffPoints[i].position.x;
    tmpVisPoint.y = takeoffPoints[i].position.y;
    tmpVisPoint.z = 0.02;
    marker.points.push_back(tmpVisPoint);
    tmpVisPoint.x = landPoints[i].position.x;
    tmpVisPoint.y = landPoints[i].position.y;
    tmpVisPoint.z = 0.02;
    marker.points.push_back(tmpVisPoint);
    marker.colors.push_back(tempColorRGBA);
  }
  geometry_msgs::Point tmpVisPoint;
  tmpVisPoint.x = grabPackageTakeoffPoint.position.x;
  tmpVisPoint.y = grabPackageTakeoffPoint.position.y;
  tmpVisPoint.z = 0.02;
  tempColorRGBA.r = 0;
  tempColorRGBA.b = 0;
  tempColorRGBA.g = 1;
  marker.points.push_back(tmpVisPoint);
  marker.colors.push_back(tempColorRGBA);

  markerPub.publish(marker);

  /*tempState->setXYZ(1.25265, 0.49447, 0.02);
  cout << isStateValid(tempState()) << endl;
  tempState->setXYZ(1.25265, 0.49447, 1.0);
  cout << isStateValid(tempState()) << endl;*/

  return true;
}

void MissionPlanner::mapReceivedCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg)
{
  map2DWidth =  msg->info.width;
  map2DHeight = msg->info.height;
  map2Dframeid = msg->header.frame_id;

  map2DResolution = msg->info.resolution; // [m/cell]

  map2DoriginX = msg->info.origin.position.x;
  map2DoriginY = msg->info.origin.position.y;

  map2D = new double[map2DWidth * map2DHeight * sizeof(double)];

  for (int i = 0; i < map2DWidth * map2DHeight; i++)
  {
    if (msg->data[i] != 0) // (msg->data[i] == 100 || msg->data[i] == -1)  -> occupied or unknown cell in OccupancyGrid message
      map2D[i] = -1;  // occupied cell in pathPlanner's map representation
    else            // (msg->data[i] == 0) -> unoccupied cell in OccupancyGrid message
      map2D[i] = 1;   // unoccupied cell in pathPlanner's map representation
  }

  ROS_INFO("Map received a %d X %d map @ %.3f m/pix", map2DWidth, map2DHeight, map2DResolution);
}

bool MissionPlanner::is2DPointValid(geometry_msgs::PoseStamped &pose, 
  double ugvOffsetX, double ugvOffsetY)
{
  int mapindex;
  if (map2D==NULL) return false;

  geometry_msgs::PoseStamped tpose;
  for(double x=-ugvOffsetX; x<=ugvOffsetX; x+=map2DResolution)
  {
    for(double y=-ugvOffsetY; y<=ugvOffsetY; y+=map2DResolution)
    { 
      tpose.pose.position.x = pose.pose.position.x - map2DoriginX + x;
      tpose.pose.position.y = pose.pose.position.y - map2DoriginY + y;
      mapindex = (int)(tpose.pose.position.x/map2DResolution) + (int)((tpose.pose.position.y/map2DResolution))*map2DWidth;

      if (map2D[mapindex] == -1) return false;
    }
  }
  return true;

  /* geometry_msgs::PoseStamped tpose;
  if (map2D!=NULL)
  {
    
    tpose.pose.position.x = pose.pose.position.x - map2DoriginX;
    tpose.pose.position.y = pose.pose.position.y - map2DoriginY;
    mapindex = (int)(tpose.pose.position.x/map2DResolution) + (int)((tpose.pose.position.y/map2DResolution))*map2DWidth;

    if (map2D[mapindex] == -1) return false;
    else return true;
  }
  else return false;*/
}

bool MissionPlanner::collisionCheck(mav_path_trajectory::TrajectorySampled generatedTrajectory)
{
  bool trajectoryPointValid;

  for (int i=0; i<generatedTrajectory.position.size(); i++)
  {
    ob::ScopedState<ob::SE3StateSpace> trajectoryPoint(space_);
    trajectoryPoint->setXYZ(generatedTrajectory.position[i].x, generatedTrajectory.position[i].y, generatedTrajectory.position[i].z);
    trajectoryPoint->rotation().setAxisAngle(0, 0, 0, 1);

    trajectoryPointValid = isStateValid_withRadius(trajectoryPoint());

    if (!trajectoryPointValid)
    {
      int segment = generatedTrajectory.segment[i];
      geometry_msgs::Pose middlePoint;
      middlePoint.position.x = (waypointArray.waypoints[segment].position.x + waypointArray.waypoints[segment-1].position.x)/2.;
      middlePoint.position.y = (waypointArray.waypoints[segment].position.y + waypointArray.waypoints[segment-1].position.y)/2.;
      middlePoint.position.z = (waypointArray.waypoints[segment].position.z + waypointArray.waypoints[segment-1].position.z)/2.;
      middlePoint.orientation.z = (waypointArray.waypoints[segment].orientation.z + waypointArray.waypoints[segment-1].orientation.z)/2.;
                  
      // std::vector<int>::iterator it;
      waypointArray.waypoints.insert(waypointArray.waypoints.begin()+segment,middlePoint);
      posearray.poses.insert(posearray.poses.begin()+segment,middlePoint);
      ROS_INFO("Adding new point to path on segment %d.(collision)", segment);
      ROS_INFO("Created %d. points", (int)++pt_num);
      return true;
    }
  }
  return false;
}

bool MissionPlanner::taylor(mav_path_trajectory::TrajectorySampled generatedTrajectory)
{
  double maxD, minD, Ax, Ay, Az, Bx, By, Bz, pTx, pTy, pTz, Tx, Ty, Tz, t;
  bool taylorFlag = false; //no need to generate new trajectory
  //taylor
  for (int i=1; i<=generatedTrajectory.segment[generatedTrajectory.segment.size()-1]; i++)
  {
    maxD = 0;
    Ax = waypointArray.waypoints[i-1].position.x;
    Ay = waypointArray.waypoints[i-1].position.y; 
    Az = waypointArray.waypoints[i-1].position.z;
    Bx = waypointArray.waypoints[i].position.x;
    By = waypointArray.waypoints[i].position.y;
    Bz = waypointArray.waypoints[i].position.z;

    double dAB = sqrt(pow((Bx-Ax),2)+ pow((By-Ay),2) + pow((Bz-Az),2));
    for (int j=0; j<generatedTrajectory.position.size(); j++)
    {
      if (i==generatedTrajectory.segment[j])
      {
        minD = 0;
        bool tFlag = false;

        Tx = generatedTrajectory.position[j].x;
        Ty = generatedTrajectory.position[j].y;
        Tz = generatedTrajectory.position[j].z;
        t = ((Bx-Ax)*(Tx-Ax) + (By-Ay)*(Ty-Ay) + (Bz-Az)*(Tz-Az))/(pow((Bx-Ax),2)+pow((By-Ay),2)+pow((Bz-Az),2));
        if (t<0)
        {
          t=0;
          tFlag = true;
        }
        else if (t>1) 
        {
          t=1;
          tFlag = true;
        }
        minD = pow((Tx-((Bx-Ax)*t+Ax)),2) + pow((Ty-((By-Ay)*t+Ay)),2) + pow((Tz-((Bz-Az)*t+Az)),2);
        /*if (tFlag && dAB<minSegmentLength)
          {
            minD = 0;
          }*/
        if (minD>maxD)
        {
          maxD = minD;
          if (tFlag)
          {
            pTx = (waypointArray.waypoints[i].position.x + waypointArray.waypoints[i-1].position.x)/2.;
            pTy = (waypointArray.waypoints[i].position.y + waypointArray.waypoints[i-1].position.y)/2.;
            pTz = (waypointArray.waypoints[i].position.z + waypointArray.waypoints[i-1].position.z)/2.;
          }
          else
          {
            pTx = (Bx - Ax)*t + Ax;
            pTy = (By - Ay)*t + Ay;
            pTz = (Bz - Az)*t + Az;
          }
        }
      }
    }

    if (sqrt(maxD)>maxTrajectoryOffset && dAB>minSegmentLength)
    {
      taylorFlag = true;
      geometry_msgs::Pose newPoint;
      newPoint.position.x = pTx;
      newPoint.position.y = pTy;
      newPoint.position.z = pTz;
      newPoint.orientation.z = (waypointArray.waypoints[i].orientation.z + waypointArray.waypoints[i-1].orientation.z)/2.;
      
      waypointArray.waypoints.insert(waypointArray.waypoints.begin()+i,newPoint);
      posearray.poses.insert(posearray.poses.begin()+i,newPoint);
      ROS_INFO("Adding new point to path on segment %d (taylor).", i);
      ROS_INFO("Created %d. points", (int)++pt_num);
    }
  }

  return taylorFlag;
}

geometry_msgs::Twist MissionPlanner::horizontClearance(double R0,double Rmax,double fimin,double fimax,double vmax)
{
  double v=0.0, vy = 0, fi=0.0;
   // Visualization markers

  visualization_msgs::Marker marker;
  marker.header.stamp = ros::Time::now();
  marker.header.frame_id = std::string("world");
  marker.id = 1;
  marker.ns = "markeri";
  marker.type = 7; //Sphere list 7
  marker.action = 0;
  marker.pose.orientation.w = 1;
  geometry_msgs::Vector3 tempScale;
  tempScale.x = 0.05;
  tempScale.y = 0.05;
  tempScale.z = 0.05;
  marker.scale = tempScale;
  std_msgs::ColorRGBA tempColorRGBA;
  tempColorRGBA.b = 1;
  tempColorRGBA.a = 1;
  marker.lifetime = ros::Duration(10);
  marker.color.b = 1;
  marker.color.g = 1;
  marker.color.a = 1;
  tempColorRGBA.r = 0;
  tempColorRGBA.b = 0.0;
  tempColorRGBA.a = 1;
  
  for(double i=R0;i<Rmax;i+=(Rmax-R0)/10)
  {
    for(double j=fimin;j<fimax;j+=(fimax-fimin)/20)
    {
      double cdf = cos(current_imu_yaw+j),sdf = sin(current_imu_yaw+j);
      point3d query(MissionPlanner::current_pose_x+cdf*i, MissionPlanner::current_pose_y+sdf*i, MissionPlanner::current_pose_z);
      geometry_msgs::Point Query; Query.x =MissionPlanner::current_pose_x+cdf*i;Query.y = MissionPlanner::current_pose_y+sdf*i; Query.z = MissionPlanner::current_pose_z;
      tempColorRGBA.b = 1.0;
      tempColorRGBA.r = 0.0;
      if(MissionPlanner::isPoint3dValid(query)==false)
      {

	v+=hclear_gain*exp(-(i-R0)/R0)*cos(j)*vmax;
	vy-=hclear_gain*exp(-(i-R0)/R0)*sin(j)*vmax;
	tempColorRGBA.b = 1-exp(-(i-R0)/R0) ;
	tempColorRGBA.r = exp(-(i-R0)/R0);
	fi+=sin(j);
      }
      marker.points.push_back(Query);
      marker.colors.push_back(tempColorRGBA);
    }
  }
  if (v>(1.5*vmax)) v = 1.5*vmax;
  else if (v<(-1.5*vmax)) v = -1.5*vmax;
  if (vy>(1.5*vmax)) vy = 1.5*vmax;
  else if (vy<(-1.5*vmax)) vy = -1.5*vmax;
  //ROS_INFO("Speed feedback= %f :: rotation feedback %f\n", v, fi);
  markerPub.publish(marker);
  geometry_msgs::Twist vel;
  vel.linear.x = v;
  vel.linear.y = vy;
  vel.angular.z = fi;
  marker.type = 9; //Text
  //Since no c++11 support -> need to use boost
  string txt = string("Speed = ") + boost::lexical_cast<std::string>(v)  + string(" | Rotation = ") + boost::lexical_cast<std::string>(fi);
  marker.text = txt;
  tempScale.x = 0.1;
  tempScale.y = 0.1;
  tempScale.z = 0.1;
  marker.scale = tempScale;
  marker.pose.position.x = current_pose_x+0.2; marker.pose.position.y = current_pose_y; marker.pose.position.z = current_pose_z;
  textPub.publish(marker);
  return vel;
}

void MissionPlanner::CalculateNextJoyWaypoint()
{
  geometry_msgs::Vector3 startingPoint;
  geometry_msgs::Vector3 goalPoint;
  geometry_msgs::Vector3 speedVector;
  geometry_msgs::Vector3 nullVector;
  mav_path_trajectory::TrajectorySampled generatedJoyTrajectory;
  int joy_rate_index;
  double joy_z = 0.99;
  if (uav_type == "arducopter") joy_rate_index = 20/5; //JOY works at 5Hz, trajectory is published at arducopter speed
  else joy_rate_index = 100/5;//JOY works at 5Hz, trajectory is published at 100Hz in NEO
   
  startingPoint.x = MissionPlanner::current_pose_x;
  startingPoint.y = MissionPlanner::current_pose_y;
  startingPoint.z = joy_z;
   //DX = v * DT
  //DX = 0.1, DT = 1/5Hz (200ms)
  //dt = 1/20Hz (50ms)

  double v = hclear_vmax*joy_velocityREF; //[5 cm/s]
  double vy = hclear_vmax*joy_slideREF/2; //we do not want to reward sliding only in emergency
  double fi = joy_headingREF;
  //ROS_INFO("Speed ref= %f :: rotation ref %f\n", v, fi);

  //goalPoint.x = MissionPlanner::current_pose_x;
  //goalPoint.y = MissionPlanner::current_pose_y-DX;

  goalPoint.z = joy_z;
  geometry_msgs::Twist steer = horizontClearance(hclear_rmin,hclear_rmax,hclear_fimin,hclear_fimax, hclear_vmax);
  if (steer.linear.x > v && v>0) v = 0;
  else if (steer.linear.x < v && v < 0) v = 0;
  else if (v == 0) v = 0;
  else v-=steer.linear.x;
  vy+=steer.linear.y;
  //fi-=.02*steer.angular.z;
  double DX = v*0.2;
  double DY = vy*0.2;
  speedVector.x = cos(MissionPlanner::current_imu_yaw)*v-sin(MissionPlanner::current_imu_yaw)*vy;
  speedVector.y = sin(MissionPlanner::current_imu_yaw)*v+cos(MissionPlanner::current_imu_yaw)*vy;
  //ROS_INFO("%f %f %f",v, v, fi);
//   bool result = MissionPlanner::isPoint3dValid(query);
  if (true)
  { 
  for(int i=0;i<joy_rate_index;i++)
  {
    goalPoint.x = MissionPlanner::current_pose_x+cos(MissionPlanner::current_imu_yaw)*(i+1)*DX/(joy_rate_index)-sin(MissionPlanner::current_imu_yaw)*(i+1)*DY/(joy_rate_index);
    goalPoint.y = MissionPlanner::current_pose_y+sin(MissionPlanner::current_imu_yaw)*(i+1)*DX/(joy_rate_index)+cos(MissionPlanner::current_imu_yaw)*(i+1)*DY/(joy_rate_index);
    goalPoint.z = 0.9;
    generatedJoyTrajectory.position.push_back(goalPoint);
    generatedJoyTrajectory.speed.push_back(speedVector);
    generatedJoyTrajectory.acceleration.push_back(nullVector);
    generatedJoyTrajectory.jerk.push_back(nullVector);
    generatedJoyTrajectory.split.push_back(nullVector);
    generatedJoyTrajectory.yawPos.push_back(current_imu_yaw + (i+1)*fi*0.06);
    generatedJoyTrajectory.yawSpeed.push_back(0.0);
    generatedJoyTrajectory.yawAcc.push_back(0.0);
    generatedJoyTrajectory.yawJerk.push_back(0.0);
    generatedJoyTrajectory.yawSplit.push_back(0.0);
  }
  }
  
  trajectory_msgs::MultiDOFJointTrajectory multiDOFTrajectory;
  trajectory_msgs::MultiDOFJointTrajectoryPoint currentTrajectoryPoint;
  geometry_msgs::Transform tempTransform;
  geometry_msgs::Twist tempVelocity;
  geometry_msgs::Twist tempAcc;
  for(int i=0; i<generatedJoyTrajectory.position.size(); i++)
  {
    // Position
    tempTransform.translation = generatedJoyTrajectory.position[i];

    // Orientation has to be quaternion
    tempTransform.rotation.x = 0.0;
    tempTransform.rotation.y = 0.0;
    tempTransform.rotation.z = sin(generatedJoyTrajectory.yawPos[i]/2.0);
    tempTransform.rotation.w = cos(generatedJoyTrajectory.yawPos[i]/2.0);

    // Velocity
    tempVelocity.linear = generatedJoyTrajectory.speed[i];
    tempVelocity.angular.z = generatedJoyTrajectory.yawSpeed[i];

    // Acceleration
    tempAcc.linear = generatedJoyTrajectory.acceleration[i];
    tempAcc.angular.z = generatedJoyTrajectory.yawAcc[i];

    // We also need ros Duration to specify when is certain point going
    // to take effect. We know trajectory sampling frequency so i'th point
    // will occur at i*samplingFreq after beginning
    ros::Duration tempDuration(i/double(trajectoryRate));

    // Now we have everything we need to create MultiDOFTrajectoryPoint
    currentTrajectoryPoint.transforms.clear();
    currentTrajectoryPoint.velocities.clear();
    currentTrajectoryPoint.accelerations.clear();
    currentTrajectoryPoint.transforms.push_back(tempTransform);
    currentTrajectoryPoint.velocities.push_back(tempVelocity);
    currentTrajectoryPoint.accelerations.push_back(tempAcc);
    currentTrajectoryPoint.time_from_start = tempDuration;

    // And points are stored in array
    multiDOFTrajectory.points.push_back(currentTrajectoryPoint);
    multiDOFTrajectory.joint_names.push_back(std::string("base_link"));
  }
  // MultiDOF trajectory is generated and we have to set it's header
  multiDOFTrajectory.header.stamp = ros::Time::now();// + ros::Duration(1);
  multiDOFTrajectory.header.frame_id = std::string("world");
  multiDOFTrajectory.header.seq = 0;
  // Publish data for NEO and Arducopter
  neoTrajectoryPub.publish(multiDOFTrajectory);
  trajectoryPub.publish(generatedJoyTrajectory);
}

bool MissionPlanner::WaypointPositionCallback(mav_path_trajectory::WaypointSrv::Request &req, mav_path_trajectory::WaypointSrv::Response &res)
{
  mav_path_trajectory::TrajectorySampled generatedTrajectory;
  bool collisionFlag = false;
  bool taylorFlag = false;
  bool planTimeOutFlag = true; 
  double begin, end;
  double cost = 1;

  while(cost>0 && planTimeOutFlag)
  {
    planTimeOutFlag = false;

    cost = plan(req.start, req.goal, true);
    res.cost = cost;

    begin = ros::Time::now().toSec();

    if (res.cost > 0)
    {
      do
      {
        do
        {

          end = ros::Time::now().toSec();

          if ((end-begin)>planningTimeOut)
          {
            planTimeOutFlag = true;
            ROS_WARN("Planning time out.");
            break;
          }

          // Changing orientation to be yaw at orientation.z for first and last point
          tf::Quaternion q;
          tf::quaternionMsgToTF(req.start.orientation, q);
          waypointArray.waypoints[0].orientation.z = tf::getYaw(q);
          tf::quaternionMsgToTF(req.goal.orientation, q);
          waypointArray.waypoints[waypointArray.waypoints.size() - 1].orientation.z = tf::getYaw(q);

          if (waypointArray.waypoints.size()==2)
          {
            geometry_msgs::Pose newPoint;

            newPoint.position.x = (waypointArray.waypoints[1].position.x + waypointArray.waypoints[0].position.x)/2.;
            newPoint.position.y = (waypointArray.waypoints[1].position.y + waypointArray.waypoints[0].position.y)/2.;
            newPoint.position.z = (waypointArray.waypoints[1].position.z + waypointArray.waypoints[0].position.z)/2.;
            newPoint.orientation.z = (waypointArray.waypoints[1].orientation.z + waypointArray.waypoints[0].orientation.z)/2.;
      
            waypointArray.waypoints.insert(waypointArray.waypoints.begin()+1,newPoint);
          }

          // Add yaw from rrt
          for(int i=1; i<waypointArray.waypoints.size()-1; i++)
          {
            double dx = waypointArray.waypoints[i].position.x - 
              waypointArray.waypoints[i-1].position.x;
            double dy = waypointArray.waypoints[i].position.y - 
              waypointArray.waypoints[i-1].position.y;
            if (sqrt(dy*dy + dx*dx) > 0.02)
            {
              waypointArray.waypoints[i].orientation.z = atan2(dy, dx);
            }
            else
            {
              waypointArray.waypoints[i].orientation.z = 
                waypointArray.waypoints[i-1].orientation.z;
            }
          }
          //waypointArray.waypoints[0].orientation.z = req.start.orientation.z;
          //waypointArray.waypoints[waypointArray.waypoints.size() - 1].orientation.z = 
          //  req.goal.orientation.z;

          generatedTrajectory = trajectoryPlanner.plan(waypointArray);

          collisionFlag = collisionCheck(generatedTrajectory); //false to turn collision and taylor off


        }while(collisionFlag);

        if (planTimeOutFlag) break;

        taylorFlag = taylor(generatedTrajectory);

      }while(taylorFlag);
    }
  }

  if (cost>0)
  {
    /* // Before generating trajectory override yaw. Use waypoints to determine
    // yaw angle for each segment and add it to generatedTrajectory
    std::vector<double> yawFromRRT;
    for(int i=0; i<waypointArray.waypoints.size()-1; i++)
    {
      // Determine yaw from difference in x and in y
      double dx = waypointArray.waypoints[i+1].position.x - 
        waypointArray.waypoints[i].position.x;
      double dy = waypointArray.waypoints[i+1].position.y - 
        waypointArray.waypoints[i].position.y;
      yawFromRRT.push_back(atan2(dy,dx));
    }

    // Fill yaw in generatedTrajectory, yawSpeed and yawAcc should be 0
    for(int i=0; i<generatedTrajectory.position.size(); i++)
    {
      // Segments go from 1 to n, hence the offset -1
      generatedTrajectory.yawPos[i] = yawFromRRT[
        generatedTrajectory.segment[i]-1];
      generatedTrajectory.yawSpeed[i] = 0;
      generatedTrajectory.yawAcc[i] = 0;
    }
    */

    // Override again. Let the uav turn during the whole trajectory.


    // Generated trajectory is of type TrajectorySampled. For Neo hexacopter
    // it has to be trajectory_msgs::MultiDOFJointTrajectory
    trajectory_msgs::MultiDOFJointTrajectory multiDOFTrajectory;
    trajectory_msgs::MultiDOFJointTrajectoryPoint currentTrajectoryPoint;
    geometry_msgs::Transform tempTransform;
    geometry_msgs::Twist tempVelocity;
    geometry_msgs::Twist tempAcc;
    for(int i=0; i<generatedTrajectory.position.size(); i++)
    {
      // Position
      tempTransform.translation = generatedTrajectory.position[i];

      // Orientation has to be quaternion
      tempTransform.rotation.x = 0.0;
      tempTransform.rotation.y = 0.0;
      tempTransform.rotation.z = sin(generatedTrajectory.yawPos[i]/2.0);
      tempTransform.rotation.w = cos(generatedTrajectory.yawPos[i]/2.0);

      // Velocity
      tempVelocity.linear = generatedTrajectory.speed[i];
      tempVelocity.angular.z = generatedTrajectory.yawSpeed[i];

      // Acceleration
      tempAcc.linear = generatedTrajectory.acceleration[i];
      tempAcc.angular.z = generatedTrajectory.yawAcc[i];

      // We also need ros Duration to specify when is certain point going
      // to take effect. We know trajectory sampling frequency so i'th point
      // will occur at i*samplingFreq after beginning
      ros::Duration tempDuration(i/double(trajectoryRate));

      // Now we have everything we need to create MultiDOFTrajectoryPoint
      currentTrajectoryPoint.transforms.clear();
      currentTrajectoryPoint.velocities.clear();
      currentTrajectoryPoint.accelerations.clear();
      currentTrajectoryPoint.transforms.push_back(tempTransform);
      currentTrajectoryPoint.velocities.push_back(tempVelocity);
      currentTrajectoryPoint.accelerations.push_back(tempAcc);
      currentTrajectoryPoint.time_from_start = tempDuration;

      // And points are stored in array
      multiDOFTrajectory.points.push_back(currentTrajectoryPoint);
      multiDOFTrajectory.joint_names.push_back(std::string("base_link"));
    }
    // MultiDOF trajectory is generated and we have to set it's header
    multiDOFTrajectory.header.stamp = ros::Time::now() + ros::Duration(1);
    multiDOFTrajectory.header.frame_id = std::string("world");
    multiDOFTrajectory.header.seq = 0;
    // Publish data
    neoTrajectoryPub.publish(multiDOFTrajectory);

    trajectoryPub.publish(generatedTrajectory);
    pointPub.publish(posearray);
    ROS_INFO("Trajectory generated and published.");
  }

  return true;
}

bool MissionPlanner::WaypointArrayPositionCallback(
  mav_path_trajectory::WaypointArraySrv::Request &req, 
  mav_path_trajectory::WaypointArraySrv::Response &res)
{
  bool taylorFlag = false;
  bool collisionFlag = false;
  bool planTimeOutFlag = true; 
  double cost = 1;
  double begin, end;
  mav_path_trajectory::TrajectorySampled generatedTrajectory;

  waypointArray.waypoints.clear();
  posearray.poses.clear();
  waypointArray.waypoints = req.waypoints.waypoints;
  posearray.header.frame_id="world";
  posearray.header.stamp = ros::Time::now();
  posearray.poses = waypointArray.waypoints;

  // Changing orientation to be yaw at orientation.z for first and last point
  tf::Quaternion q;
  for (int i = 0; i < waypointArray.waypoints.size(); i++)
  {
    tf::quaternionMsgToTF(waypointArray.waypoints[i].orientation, q);
    waypointArray.waypoints[i].orientation.z = tf::getYaw(q);
  }

  if (waypointArray.waypoints.size()==2)
  {
    geometry_msgs::Pose newPoint;

    newPoint.position.x = (waypointArray.waypoints[1].position.x + waypointArray.waypoints[0].position.x)/2.;;
    newPoint.position.y = (waypointArray.waypoints[1].position.y + waypointArray.waypoints[0].position.y)/2.;
    newPoint.position.z = (waypointArray.waypoints[1].position.z + waypointArray.waypoints[0].position.z)/2.;
    newPoint.orientation.z = (waypointArray.waypoints[1].orientation.z + waypointArray.waypoints[0].orientation.z)/2.;
    
    waypointArray.waypoints.insert(waypointArray.waypoints.begin()+1,newPoint);
  }
  generatedTrajectory = trajectoryPlanner.plan(waypointArray);

  // Get the number of points per segment
  std::vector<int> numberOfPointsPerSegment;
  for(int i=0; i<generatedTrajectory.segment[generatedTrajectory.segment.size()-1]; i++)
  {
    numberOfPointsPerSegment.push_back(0);
  }
  for(int i=0; i<generatedTrajectory.position.size(); i++)
  {
    numberOfPointsPerSegment[generatedTrajectory.segment[i]-1]++;
  }

  // Trajectory generated
  // Hack for NEO
  /*double qx = req.waypoints.waypoints[req.waypoints.waypoints.size()-1].orientation.x;
  double qy = req.waypoints.waypoints[req.waypoints.waypoints.size()-1].orientation.y;
  double qz = req.waypoints.waypoints[req.waypoints.waypoints.size()-1].orientation.z;
  double qw = req.waypoints.waypoints[req.waypoints.waypoints.size()-1].orientation.w;
  
  for(int i=0; i<generatedTrajectory.yawPos.size(); i++)
  {
    generatedTrajectory.yawPos[i] = atan2(2 * (qw * qz + qx * qy), 
      qw * qw + qx * qx - qy * qy - qz * qz);
    generatedTrajectory.yawSpeed[i] = 0.0;
    generatedTrajectory.yawAcc[i] = 0.0;
  }*/

  // If orientation.w of the first point is greater than 10.0 user specifies
  // automatic rotation calculation in direction of segments
  if (req.waypoints.waypoints[0].orientation.w > 10.0)
  {
    std::vector<double> yawWaypoints;
    // Create yaw from waypoints
    for(int i=1; i<waypointArray.waypoints.size(); i++)
    {
      double dx = waypointArray.waypoints[i].position.x - 
        waypointArray.waypoints[i-1].position.x;
      double dy = waypointArray.waypoints[i].position.y - 
        waypointArray.waypoints[i-1].position.y;
      yawWaypoints.push_back(atan2(dy, dx));
    }

    for(int i=0; i<generatedTrajectory.yawPos.size(); i++)
    {
      generatedTrajectory.yawPos[i] = yawWaypoints[generatedTrajectory.segment[i]-1];
    }
  }


  // Generated trajectory is of type TrajectorySampled. For Neo hexacopter
    // it has to be trajectory_msgs::MultiDOFJointTrajectory
    trajectory_msgs::MultiDOFJointTrajectory multiDOFTrajectory;
    trajectory_msgs::MultiDOFJointTrajectoryPoint currentTrajectoryPoint;
    geometry_msgs::Transform tempTransform;
    geometry_msgs::Twist tempVelocity;
    geometry_msgs::Twist tempAcc;
    for(int i=0; i<generatedTrajectory.position.size(); i++)
    {
      // Position
      tempTransform.translation = generatedTrajectory.position[i];

      // Orientation has to be quaternion
      tempTransform.rotation.x = 0.0;
      tempTransform.rotation.y = 0.0;
      tempTransform.rotation.z = sin(generatedTrajectory.yawPos[i]/2.0);
      tempTransform.rotation.w = cos(generatedTrajectory.yawPos[i]/2.0);

      // Velocity
      tempVelocity.linear = generatedTrajectory.speed[i];
      tempVelocity.angular.z = generatedTrajectory.yawSpeed[i];

      // Acceleration
      tempAcc.linear = generatedTrajectory.acceleration[i];
      tempAcc.angular.z = generatedTrajectory.yawAcc[i];

      // We also need ros Duration to specify when is certain point going
      // to take effect. We know trajectory sampling frequency so i'th point
      // will occur at i*samplingFreq after beginning
      ros::Duration tempDuration(i/double(trajectoryRate));

      // Now we have everything we need to create MultiDOFTrajectoryPoint
      currentTrajectoryPoint.transforms.clear();
      currentTrajectoryPoint.velocities.clear();
      currentTrajectoryPoint.accelerations.clear();
      currentTrajectoryPoint.transforms.push_back(tempTransform);
      currentTrajectoryPoint.velocities.push_back(tempVelocity);
      currentTrajectoryPoint.accelerations.push_back(tempAcc);
      currentTrajectoryPoint.time_from_start = tempDuration;

      // And points are stored in array
      multiDOFTrajectory.points.push_back(currentTrajectoryPoint);
      multiDOFTrajectory.joint_names.push_back(std::string("base_link"));
    }
    // MultiDOF trajectory is generated and we have to set it's header
    multiDOFTrajectory.header.stamp = ros::Time::now();// + ros::Duration(1);
    multiDOFTrajectory.header.frame_id = std::string("world");
    multiDOFTrajectory.header.seq = 0;
    // Publish data
    neoTrajectoryPub.publish(multiDOFTrajectory);

    trajectoryPub.publish(generatedTrajectory);
    pointPub.publish(posearray);
    ROS_INFO("Trajectory generated and published.");
    res.cost = 0;
  return true;
}

bool MissionPlanner::WaypointOrientationCallback(mav_path_trajectory::WaypointSrv::Request &req, mav_path_trajectory::WaypointSrv::Response &res)
{
    mav_path_trajectory::WaypointArray waypointOrientationArray;

    waypointOrientationArray.waypoints.push_back(req.start);

    waypointOrientationArray.waypoints.push_back(req.goal);

    trajectoryPub.publish(trajectoryPlanner.plan(waypointOrientationArray));
    ROS_INFO("Trajectory generated and published.");

    res.cost = 1;
    return true;
}

void MissionPlanner::octomapCallback(const octomap_msgs::Octomap::ConstPtr& msg)
{
  //This is a callback function for Octomap message
  MissionPlanner::_octomap = (octomap::OcTree*)octomap_msgs::binaryMsgToMap(*msg); //Here we parse binary msg data using binaryMsgToMap function
  //Uncomment this code if you want to save the reveived map
  //MissionPlanner::_octomap->writeBinary("savedmap.bt");

  ROS_INFO("Map Updated");
  ROS_INFO("Map depth %d \n",_octomap->getTreeDepth());
  ROS_INFO("Octomap bbx = %f %f %f \n", _octomap->getBBXMax().x(),_octomap->getBBXMax().y(),_octomap->getBBXMax().z());
  
  /*octomap::OcTree tempOctomap(0.02);
  for(float x=-2.3;x<2.3;x=x+0.02)
  {
    for(float y=-2;y<3.8;y=y+0.02)
    {
      for(float z=-0.1;z<1.2;z=z+0.02)
      {
	point3d Query(x,y,z);
	OcTreeNode* result = MissionPlanner::_octomap->search (Query);
	if (result != NULL) 
	{   
	  //point3d QueryTransform(y-1.7,-x+0.8,z);
    point3d QueryTransform(x,y,z);
	  if ( _octomap->isNodeOccupied(result)) tempOctomap.updateNode(QueryTransform,true);
	  else tempOctomap.updateNode(QueryTransform,false);
	}
      }
    }
  }

  tempOctomap.writeBinary("freestyle_online_sliced_notRotated.bt");
  ROS_INFO("Map Rotated and Saved");*/
  //_octomap->writeBinary("/home/euroc/freestyle_online.bt");
}

bool MissionPlanner::isPoint3dValid(point3d query)
{
  OcTreeNode* result = MissionPlanner::_octomap->search (query, 15);
  //Unknown area - map is such that you can go there

  if (result != NULL) 
  {   
   if ( _octomap->isNodeOccupied(result)) return false;
    else return true;
  }
  else 
  {
    //Unknown area - map is such that you can go there
    return true;
  } 
}

bool MissionPlanner::isStateValid(const ob::State *state)
{
  point3d query (state->as<ob::SE3StateSpace::StateType>()->getX(), state->as<ob::SE3StateSpace::StateType>()->getY(), state->as<ob::SE3StateSpace::StateType>()->getZ());
  OcTreeNode* result = MissionPlanner::_octomap->search (query, 15);
  if (result != NULL) 
  {   
    if ( _octomap->isNodeOccupied(result)) return false;
    else return true;
  }
  else 
  {
    //Unknown area - map is such that you can go there
    return true;
  } 
 }

bool MissionPlanner::isStateValid(const ob::State *state, int depth)
{
  point3d query (state->as<ob::SE3StateSpace::StateType>()->getX(), state->as<ob::SE3StateSpace::StateType>()->getY(), state->as<ob::SE3StateSpace::StateType>()->getZ());
  OcTreeNode* result = MissionPlanner::_octomap->search (query, depth);
  if (result != NULL) 
  {   
    if ( _octomap->isNodeOccupied(result)) return false;
    else return true;
  }
  else 
  {
    //Unknown area - map is such that you can go there
    return true;
  } 
}

bool MissionPlanner::isStateValid_withRadius(const ob::State *state)
{
  int depth = 16; int radius = 5; int radiusZ = 3;
  //depth = 15; radius = 2; radiusZ = 1;
  double offsetZ = 0.0;
  if (state->as<ob::SE3StateSpace::StateType>()->getZ() < 0.36) offsetZ = 0.36;
  for(int x=-radius;x<=radius;x++)
  {
    for(int y=-radius;y<=radius;y++)
    {
      for(int z=-radiusZ;z<=0;z++)
      {
	point3d query (state->as<ob::SE3StateSpace::StateType>()->getX()+x*_octomap->getResolution()*pow(2,16-depth), state->as<ob::SE3StateSpace::StateType>()->getY()+y*_octomap->getResolution()*pow(2,16-depth), state->as<ob::SE3StateSpace::StateType>()->getZ()+offsetZ+z*_octomap->getResolution()*pow(2,16-depth));
	OcTreeNode* result = MissionPlanner::_octomap->search (query,depth);
	if (result != NULL) 
	{   
	  if ( _octomap->isNodeOccupied(result)) return false;
	}
      }
    }
  }
  return true;
}

bool MissionPlanner::isStateValid_withRadius(const ob::State *state, int depth, int radius)
//Searches in Octomap depth, and builds a square cube of size 2*radius*_ocomap->getResolution
{
  double offsetZ = 0.0;
  if (state->as<ob::SE3StateSpace::StateType>()->getZ() < 0.36) offsetZ = 0.36;
  for(int x=-radius;x<=radius;x++)
  {
    for(int y=-radius;y<=radius;y++)
    {
      for(int z=0;z<=radius;z++)
      {
	point3d query (state->as<ob::SE3StateSpace::StateType>()->getX()+x*_octomap->getResolution()*pow(2,16-depth), state->as<ob::SE3StateSpace::StateType>()->getY()+y*_octomap->getResolution()*pow(2,16-depth), state->as<ob::SE3StateSpace::StateType>()->getZ()+offsetZ+z*_octomap->getResolution()*pow(2,16-depth));
	OcTreeNode* result = MissionPlanner::_octomap->search (query,depth);
	if (result != NULL) 
	{   
	  if ( _octomap->isNodeOccupied(result)) return false;
	}
      }
    }
  }
  return true;
}


double MissionPlanner::plan(geometry_msgs::Pose StartPointPose,
  geometry_msgs::Pose EndPointPose, bool genTrajectoryFlag, 
  bool useCustomConstraintsAxisZFlag, double zLow,
  double zHigh)
{
  // Check if distance between start and end point is within tolerance. If
  // it is then there is no need for planning. Same goes for yaw
  double positionTolerance = 0.01;
  double positionDistance = sqrt(
    pow(StartPointPose.position.x - EndPointPose.position.x, 2.0) + 
    pow(StartPointPose.position.y - EndPointPose.position.y, 2.0) + 
    pow(StartPointPose.position.z - EndPointPose.position.z, 2.0));
  // Angle setup
  double angleTolerance = 0.01;
  double angleDistance = abs(
    StartPointPose.orientation.z - EndPointPose.orientation.z);
  // Check tolerances
  if((positionTolerance > positionDistance) && 
    (angleTolerance > angleDistance))
  {
    //cout << positionDistance << " " << angleDistance << endl;
    ROS_WARN("Points too close to each other, path is not planned.");
    return -1.0; //max(positionDistance, angleDistance);
  }

  double dt = 0;
  bool plan_flag = true;
  bool startValid, endValid;
  ob::PlannerStatus solved;
  //planner setup

  geometry_msgs::Vector3 StartPoint, EndPoint;

  StartPoint.x = StartPointPose.position.x;
  StartPoint.y = StartPointPose.position.y;
  StartPoint.z = StartPointPose.position.z;
  EndPoint.x = EndPointPose.position.x;
  EndPoint.y = EndPointPose.position.y;
  EndPoint.z = EndPointPose.position.z;

  //create an SE3 state space
  ob::StateSpacePtr space(new ob::SE3StateSpace());

  //set lower and upper bounds
  ob::RealVectorBounds bounds(3);
  //bounds.setLow(0, -2.75);
  //bounds.setHigh(0, 1.34);
  //bounds.setLow(1, -3.35);
  //bounds.setHigh(1, 0.62);
  bounds.setLow( 0, rrtMapMinX);
  bounds.setHigh(0, rrtMapMaxX);
  bounds.setLow( 1, rrtMapMinY);
  bounds.setHigh(1, rrtMapMaxY);

  if (useCustomConstraintsAxisZFlag == true)
  {
    bounds.setLow(2, zLow);
    bounds.setHigh(2, zHigh);
    cout << "useCustomConstraintsAxisZFlag = True" << endl;
  }
  else
  {
    //bounds.setLow(2, 0.1); //before: 0.9
    //bounds.setHigh(2, 0.9); //before: 1.1
    bounds.setLow( 2, rrtMapMinZ);
    bounds.setHigh(2, rrtMapMaxZ);
    cout << "useCustomConstraintsAxisZFlag = False" << endl;
  }

  space->as<ob::SE3StateSpace>()->setBounds(bounds);

  //create a simple setup object
  og::SimpleSetup ss(space);
  ss.setStateValidityChecker(boost::bind(&MissionPlanner::isStateValid_withRadius, this, _1));
  //space->as<ob::SE3StateSpace>()->setLongestValidSegmentFraction(0.01/space->as<ob::SE3StateSpace>()->getMaximumExtent());
  space->as<ob::SE3StateSpace>()->setLongestValidSegmentFraction(0.01/space->as<ob::SE3StateSpace>()->getMaximumExtent());

  // (optionally) set planner
  ob::SpaceInformationPtr si;
  si = ss.getSpaceInformation();

  space_ = si->getStateSpace();
  //ob::OptimizationObjective optimizationObjective(si);

  ob::PlannerPtr planner(new og::RRTstar(si));
  planner->as<og::RRTstar>()->setPrune(false);
  planner->as<og::RRTstar>()->setGoalBias(0.05);
  planner->as<og::RRTstar>()->setRange(5.0);
  planner->as<og::RRTstar>()->setPruneStatesImprovementThreshold(0.1);
  ss.setPlanner(planner);

  ob::ScopedState<ob::SE3StateSpace> start(space);
  //we can pick a random start state
  // ... or set specific values
  start->setXYZ(StartPoint.x, StartPoint.y, StartPoint.z);
  start->rotation().setAxisAngle(0, 0, 0, 1);

  ob::ScopedState<ob::SE3StateSpace> goal(space);
  //we can pick a random goal state
  // ... or set specific values
  cout << EndPoint << endl;
  goal->setXYZ(EndPoint.x, EndPoint.y, EndPoint.z);
  goal->rotation().setAxisAngle(0, 0, 0, 1);

  ss.setStartAndGoalStates(start, goal);

  checkStart = StartPoint;
  checkEnd = EndPoint;

  startValid = isStateValid_withRadius(start());
  endValid = isStateValid_withRadius(goal());
  cout << "Start point: " << endl << StartPoint << endl;
  cout << "End point: " << endl << EndPoint << endl;

  if (!startValid || !endValid)
  {
      if (!startValid) ROS_WARN("Invalid start point.");
      if (!endValid) ROS_WARN("Invalid end point.");
      return -1;
  }

  while(plan_flag)
  {
      cout << "In while" << endl;
      dt += 0.3;
      solved = ss.solve(dt);
      cout << "After solve" << endl;
      if (dt > 10) break;
      if (solved)
      {
          path = &ss.getSolutionPath();
          pt_num = path->getStateCount();
          ob::State *end_point = path->getState(pt_num-1);
          plan_flag = !(abs(end_point->as<ob::SE3StateSpace::StateType>()->getX() - EndPoint.x) < 0.05 && abs(end_point->as<ob::SE3StateSpace::StateType>()->getY() - EndPoint.y) < 0.05 &&
              abs(end_point->as<ob::SE3StateSpace::StateType>()->getZ() - EndPoint.z) < 0.05);
          cout << "Start: " << endl << StartPointPose << endl;
          cout << "End: " << endl << EndPointPose << endl;
      }
  }
  if (plan_flag)
  {
      ROS_WARN("Path plan failed!");
      return -1;
  }
  else
  {
    ROS_INFO("Path planned.");
    if (genTrajectoryFlag)
    {
      waypointArray.waypoints.clear();
      posearray.poses.clear();

      posearray.header.frame_id="world";
      posearray.header.stamp = ros::Time::now();
      geometry_msgs::Pose tempPose;
      pt_num = path->getStateCount();
      ROS_INFO("Created %d points.", (int)pt_num);
      for (int i=0;i<pt_num;i++)
      {
        ob::State *point = path->getState(i);
        tempPose.position.x = point->as<ob::SE3StateSpace::StateType>()->getX();
        tempPose.position.y = point->as<ob::SE3StateSpace::StateType>()->getY();
        tempPose.position.z = point->as<ob::SE3StateSpace::StateType>()->getZ();
        tempPose.orientation.z = StartPointPose.orientation.z;

        waypointArray.waypoints.push_back(tempPose);
        posearray.poses.push_back(tempPose);
      }
      return path->length();
    }
    else
    {
      // Trajectory does not need to be generated but fill posearray 
      // anyway so UGV service can access and publish poses afterwards

      posearray.poses.clear();

      posearray.header.frame_id="world";
      posearray.header.stamp = ros::Time::now();
      geometry_msgs::Pose tempPose;
      pt_num = path->getStateCount();
      ROS_INFO("Created %d points.", (int)pt_num);
      for (int i=0;i<pt_num;i++)
      {
        ob::State *point = path->getState(i);
        tempPose.position.x = point->as<ob::SE3StateSpace::StateType>()->getX();
        tempPose.position.y = point->as<ob::SE3StateSpace::StateType>()->getY();
        tempPose.position.z = point->as<ob::SE3StateSpace::StateType>()->getZ();
        tempPose.orientation.z = StartPointPose.orientation.z;

        posearray.poses.push_back(tempPose);
      }
      return path->length();
    }
  }
}

bool MissionPlanner::assess_path(mav_path_trajectory::AssessPath3D::Request &req, mav_path_trajectory::AssessPath3D::Response &res)
{
    geometry_msgs::Pose start, goal;
    start.position.x = req.xs;
    start.position.y = req.ys;
    start.position.z = req.zs;
    goal.position.x = req.xg;
    goal.position.y = req.yg;
    goal.position.z = req.zg;
    if (req.useUgvFlag == false) res.cost = plan(start, goal, false);
    else if (req.useUgvFlag == true) res.cost = plan(start, goal, false, true, 0.0, 0.3);
    if (res.cost < 0.0) res.cost = 0.0;
    return true;
}

bool MissionPlanner::GetPath(mav_path_trajectory::GetPath::Request &req,
  mav_path_trajectory::GetPath::Response &res)
{
  geometry_msgs::Pose start, goal;
  start.position.x = req.start.x;
  start.position.y = req.start.y;
  start.position.z = req.start.z;
  goal.position.x = req.end.x;
  goal.position.y = req.end.y;
  goal.position.z = req.end.z;

  if (req.customZBoundaries == false) res.cost = plan(start, goal, false);
  else if (req.customZBoundaries == true) res.cost = plan(start, goal, false, 
    true, req.start.z, req.end.z);

  res.path = posearray;

  return true;

}

bool MissionPlanner::SendCircleRadiusCallback(mav_path_trajectory::WaypointSrv::Request &req, 
  mav_path_trajectory::WaypointSrv::Response &res)
{
  //This is experimental
  // We get circle radius through message to test circle discretisation
  float r = req.start.position.x;
  float angleStep;
  int n = int(32.0*r); 
  angleStep = 2*M_PI/float(n);
  if (n<3) n=3;
  // Marker is a big message and it requires some attention while constructing
  visualization_msgs::Marker tempMarker;
  tempMarker.type = visualization_msgs::Marker::SPHERE_LIST;
  tempMarker.action = visualization_msgs::Marker::ADD;
  tempMarker.header.stamp = ros::Time::now();
  tempMarker.header.frame_id = std::string("world");
  tempMarker.ns = std::string("circle");
  tempMarker.id = 0;
  tempMarker.scale.x = 0.07;
  tempMarker.scale.y = 0.07;
  tempMarker.scale.z = 0.07;
  tempMarker.color.r = 1.0;
  tempMarker.color.a = 1.0;
  tempMarker.pose.position.x = req.goal.position.x;
  tempMarker.pose.position.y = req.goal.position.y;
  tempMarker.lifetime = ros::Duration();
  tempMarker.pose.orientation.w = 1.0;
  geometry_msgs::Point tempPoint;
  std_msgs::ColorRGBA tempColor;

  // There are two special fields in marker, markers and colors. These are 
  // fields to visualize array of markers so temp variables are created
  // and pushed on array vector. First we add center point to array
  tempPoint.x = req.goal.position.x;
  tempPoint.y = req.goal.position.y;
  tempColor.r = 1.0;
  tempColor.g = 1.0;
  tempColor.b = 1.0;
  tempColor.a = 1.0;
  tempMarker.points.push_back(tempPoint);
  tempMarker.colors.push_back(tempColor);

  // Adding circle points to array.
  for(int i=0; i<n; i++)
  {
    tempPoint.x = r*cos(float(i)*angleStep) + req.goal.position.x;
    tempPoint.y = r*sin(float(i)*angleStep) + req.goal.position.y;
    tempColor.r = 1.0;
    tempColor.g = 1.0;
    tempColor.b = 0.7;
    tempColor.a = 1.0;
    tempMarker.header.stamp = ros::Time::now();
    tempMarker.points.push_back(tempPoint);
    tempMarker.colors.push_back(tempColor);
  }
  //markerPub.publish(tempMarker);
  GetPointsMethod1(req.start, req.goal);
  
}

void MissionPlanner::GetPointsMethod1(geometry_msgs::Pose point1, 
            geometry_msgs::Pose point2)
{
  
  //This is still experimental
  
  // First method creates the line between two, than midpoint is computed
  // and z-coordinate is set to 0.01(just above ground). Then we create a
  // circle with variable radius to find valid points. When a valid point
  // is found we try to find path with RRT, and we remember that point. If 
  // another point is found it will be discarded if a straight line between
  // remembered point and new point hits no obstacles. When we find valid
  // point in second area and RRT returns valid path the procedure is over.

  // Create an SE3 state space
    ob::StateSpacePtr space(new ob::SE3StateSpace());

    //set lower and upper bounds
    ob::RealVectorBounds bounds(3);
  bounds.setLow(0, -40);
    bounds.setHigh(0, 40);
    bounds.setLow(1, -40);
    bounds.setHigh(1, 40);
    bounds.setLow(2, 0.1);
    bounds.setHigh(2, 0.1);
    space->as<ob::SE3StateSpace>()->setBounds(bounds);

    //create a simple setup object
    og::SimpleSetup ss(space);
    ss.setStateValidityChecker(boost::bind(
        &MissionPlanner::isStateValid, this, _1));
    space->as<ob::SE3StateSpace>()->setLongestValidSegmentFraction(
        0.05/space->as<ob::SE3StateSpace>()->getMaximumExtent());

    // (optionally) set planner
    ob::SpaceInformationPtr si;
    si = ss.getSpaceInformation();
    space_ = si->getStateSpace();
    //ob::OptimizationObjective optimizationObjective(si);

    ob::PlannerPtr planner(new og::RRTstar(si));
    planner->as<og::RRTstar>()->setPrune(true);
    planner->as<og::RRTstar>()->setGoalBias(0.05);
    planner->as<og::RRTstar>()->setRange(0.5);
    planner->as<og::RRTstar>()->setPruneStatesImprovementThreshold(0.95);
    ss.setPlanner(planner);

    // Now that we have the planner all set we can check if point1 and point2
    // are valid in the given map.
    ob::ScopedState<ob::SE3StateSpace> point1Map(space);
    ob::ScopedState<ob::SE3StateSpace> point2Map(space);
    point1Map->setXYZ(point1.position.x, point1.position.y, point1.position.z);
    point1Map->rotation().setAxisAngle(0,0,0,1);
    point2Map->setXYZ(point2.position.x, point2.position.y, point2.position.z);
    point2Map->rotation().setAxisAngle(0,0,0,1);

    // If any of the user provided points is not valid, we return with error
    if (!isStateValid(point1Map()))
    {
        ROS_WARN("Invalid first point");
        return;
    }
    if (!isStateValid(point2Map()))
    {
        ROS_WARN("Invalid second point");
        return;
    }

}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "Mission_planner");
    ros::NodeHandle n;

    MissionPlanner mission_planner;
    mission_planner.run();
    return 0;
}

int MissionPlanner::get_Voxel_index(int x, int y, int z)
{ 
  //This function is deprecated
  int index = x * width * height + z * width + y;
  return index;
} 

bool MissionPlanner::emptyCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res)
{
  if (_octomap == NULL) return false;
  _octomap->writeBinary("/home/euroc/freestyle_online.bt");
  ROS_INFO("Map Rotated and Saved");
  return true;
}

int MissionPlanner::read_binvox(string filespec)
{
  //This function is deprecated
  ifstream *input = new ifstream(filespec.c_str(), ios::in | ios::binary);

  //
  // read header
  //
  string line;
  *input >> line;  // #binvox
  if (line.compare("#binvox") != 0) {
    cout << "Error: first line reads [" << line << "] instead of [#binvox]" << endl;
    delete input;
    return 0;
  }
  *input >> version;
  cout << "reading binvox version " << version << endl;

  depth = -1;
  int done = 0;
  while(input->good() && !done) {
    *input >> line;
    if (line.compare("data") == 0) done = 1;
    else if (line.compare("dim") == 0) {
      *input >> depth >> height >> width;
    }
    else if (line.compare("translate") == 0) {
      *input >> tx >> ty >> tz;
    }
    else if (line.compare("scale") == 0) {
      *input >> scale;
    }
    else {
      cout << "  unrecognized keyword [" << line << "], skipping" << endl;
      char c;
      do {  // skip until end of line
        c = input->get();
      } while(input->good() && (c != '\n'));

    }
  }
  if (!done) {
    cout << "  error reading header" << endl;
    return 0;
  }
  if (depth == -1) {
    cout << "  missing dimensions in header" << endl;
    return 0;
  }

  size = width * height * depth;
  voxels = new byte[size];
  if (!voxels) {
    cout << "  error allocating memory" << endl;
    return 0;
  }

  //
  // read voxel data
  //
  byte value;
  byte count;
  int index = 0;
  int end_index = 0;
  int nr_voxels = 0;
  
  input->unsetf(ios::skipws);  // need to read every byte now (!)
  *input >> value;  // read the linefeed char

  while((end_index < size) && input->good()) {
    *input >> value >> count;

    if (input->good()) {
      end_index = index + count;
      if (end_index > size) return 0;
      for(int i=index; i < end_index; i++) voxels[i] = value;
      
      if (value) nr_voxels += count;
      index = end_index;
    }  // if file still ok
    
  }  // while

  input->close();
  cout << "  read " << nr_voxels << " voxels" << endl;
  cout << "  width="<< width << " height=" << height << endl;

  return 1;

}
