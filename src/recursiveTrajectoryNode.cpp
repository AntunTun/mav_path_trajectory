#include <mav_path_trajectory/trajectoryPlanning2.h>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "trajectoryPlanning2");
    trajectoryPlanning trajectory;
    trajectory.run();
    return 0;
}
