#!/usr/bin/env python
import sys, os
import rospy
from mav_path_trajectory.msg import TrajectorySampled
from geometry_msgs.msg import PoseStamped
from mav_path_trajectory.msg import WaypointArray
import time
import math

# Matlab plot and numpy
import matplotlib.pyplot as plt
import numpy as np

class plotTrajectory():

	def __init__(self):
		# Subscribes to trajectory and stores it in message below
		self.trajectorySampledSub = rospy.Subscriber(
			"trajectory", TrajectorySampled, 
			self.trajectorySampledCallback)
		self.trajectorySampled = TrajectorySampled()

		# Set trajectory sampling frequency
		self.f = rospy.get_param('~frequency', 100)
		self.f = float(self.f)
		self.Ts = 1.0/self.f

		# Create variables to plot
		self.posx = []
		self.posy = []
		self.posz = []
		self.velx = []
		self.vely = []
		self.velz = []
		self.accx = []
		self.accy = []
		self.accz = []
		self.jerkx = []
		self.jerky = []
		self.jerkz = []
		self.splitx = []
		self.splity = []
		self.splitz = []
		self.time = []

		self.plotFlag = False

	def run(self):
		r = rospy.Rate(1)

		while not rospy.is_shutdown():

			if self.plotFlag:
				self.plotFlag = False

				# Position
				plt.figure(1)
				plt.subplot(311)
				plt.plot(self.time, self.posx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m]')

				plt.subplot(312)
				plt.plot(self.time, self.posy, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m]')

				plt.subplot(313)
				plt.plot(self.time, self.posz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m]')

				# Speed
				plt.figure(2)
				plt.subplot(311)
				plt.plot(self.time, self.velx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m/s]')

				plt.subplot(312)
				plt.plot(self.time, self.vely, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m/s]')

				plt.subplot(313)
				plt.plot(self.time, self.velz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m/s]')

				# Acc
				plt.figure(3)
				plt.subplot(311)
				plt.plot(self.time, self.accx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m/s^2]')

				plt.subplot(312)
				plt.plot(self.time, self.accy, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m/s^2]')

				plt.subplot(313)
				plt.plot(self.time, self.accz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m/s^2]')

				# Jerk
				plt.figure(4)
				plt.subplot(311)
				plt.plot(self.time, self.jerkx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m/s^3]')

				plt.subplot(312)
				plt.plot(self.time, self.jerky, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m/s^3]')

				plt.subplot(313)
				plt.plot(self.time, self.jerkz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m/s^3]')

				# Jerk
				plt.figure(5)
				plt.subplot(311)
				plt.plot(self.time, self.splitx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m/s^4]')

				plt.subplot(312)
				plt.plot(self.time, self.splity, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m/s^4]')

				plt.subplot(313)
				plt.plot(self.time, self.splitz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m/s^4]')

				# x-y plot
				plt.figure(6)
				plt.plot(self.posx, self.posy, 'b')
				plt.grid(True)
				plt.xlabel('x[m]')
				plt.ylabel('y[m]')

				plt.show()

				self.clearPlotVariables()

			r.sleep()

	def trajectorySampledCallback(self, data):
		self.trajectorySampled = data
		print len(self.trajectorySampled.position)

		f = 0.001
		for i in range(len(self.trajectorySampled.position)):
			self.posx.append(float(f + self.trajectorySampled.position[i].x))
			self.posy.append(float(f + self.trajectorySampled.position[i].y))
			self.posz.append(float(f + self.trajectorySampled.position[i].z))
			self.velx.append(float(f + self.trajectorySampled.speed[i].x))
			self.vely.append(float(f + self.trajectorySampled.speed[i].y))
			self.velz.append(float(f + self.trajectorySampled.speed[i].z))
			self.accx.append(float(f + self.trajectorySampled.acceleration[i].x))
			self.accy.append(float(f + self.trajectorySampled.acceleration[i].y))
			self.accz.append(float(f + self.trajectorySampled.acceleration[i].z))
			self.jerkx.append(float(f + self.trajectorySampled.jerk[i].x))
			self.jerky.append(float(f + self.trajectorySampled.jerk[i].y))
			self.jerkz.append(float(f + self.trajectorySampled.jerk[i].z))
			self.splitx.append(float(f + self.trajectorySampled.split[i].x))
			self.splity.append(float(f + self.trajectorySampled.split[i].y))
			self.splitz.append(float(f + self.trajectorySampled.split[i].z))

		timeEnd = len(self.trajectorySampled.position)*self.Ts
		self.time = np.arange(0, (timeEnd-self.Ts/2.0), self.Ts)
		print len(self.time)
		for i in range(len(self.time)):
			if math.isnan(self.time[i]):
				print "NAN"

		self.plotFlag = True

	def clearPlotVariables(self):
		self.posx = []
		self.posy = []
		self.posz = []
		self.velx = []
		self.vely = []
		self.velz = []
		self.accx = []
		self.accy = []
		self.accz = []
		self.jerkx = []
		self.jerky = []
		self.jerkz = []
		self.splitx = []
		self.splity = []
		self.splitz = []
		self.time = []

if __name__=="__main__":
	rospy.init_node('plotTrajectory')
	a = plotTrajectory()
	a.run()