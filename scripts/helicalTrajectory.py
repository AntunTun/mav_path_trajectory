#!/usr/bin/env python
import sys, os
import time
import math
import copy

import rospy
from mav_path_trajectory.msg import WaypointArray
from visualization_msgs.msg import Marker
from nav_msgs.msg import Path
from geometry_msgs.msg import Pose, Point
from std_msgs.msg import ColorRGBA
from mav_path_trajectory.srv import SendHelixParams
from mav_path_trajectory.srv import WaypointArraySrv

class HelicalTrajectory():

    def __init__(self):
        # Ros publisher for markers
        self.markerPub = rospy.Publisher("helical_trajectory/marker", 
            Marker, queue_size=1)
        self.marker = Marker()

        # Create service to receive helical trajectory params
        self.helixParamsService = rospy.Service('helix_params', SendHelixParams, 
            self.helixParamsServiceCallback)

        # Waypoints service
        rospy.wait_for_service("positionWaypointArray", timeout=30)
        self.waypointArrayService = rospy.ServiceProxy("positionWaypointArray", 
            WaypointArraySrv)

    def run(self):
        rospy.spin()

    def helixParamsServiceCallback(self, req):
        r = req.r
        angleStep = req.angleStep
        x0 = req.x0
        y0 = req.y0
        z0 = req.z0
        zf = req.zf
        deltaZ = req.deltaZ

        # Create waypoint array and pose messages
        wp = WaypointArray()
        tempPose = Pose()
        n = int((zf-z0)/deltaZ)

        for i in range(n+1):
            x = r*math.cos(float(i)*angleStep) + x0
            y = r*math.sin(float(i)*angleStep) + y0
            z = z0 + float(i)*deltaZ
            tempPose.position.x = x
            tempPose.position.y = y
            tempPose.position.z = z
            tempPose.orientation.w = 1.0
            wp.waypoints.append(copy.deepcopy(tempPose))


        # Create helix for visualization in rviz
        tempMarker = Marker()
        tempMarker.type = Marker.SPHERE_LIST
        tempMarker.action = Marker.ADD
        tempMarker.header.stamp = rospy.Time.now()
        tempMarker.header.frame_id = "map"
        tempMarker.ns = "helix"
        tempMarker.id = 0
        tempMarker.scale.x = 0.07
        tempMarker.scale.y = 0.07
        tempMarker.scale.z = 0.07
        tempMarker.color.r = 1.0
        tempMarker.color.a = 1.0
        tempMarker.lifetime = rospy.Duration()
        tempMarker.pose.orientation.w = 1.0

        tempColor = ColorRGBA()
        tempColor.r = 1.0
        tempColor.a = 1.0

        tempPoint = Point()

        for i in range(n+1):
            tempPoint.x = wp.waypoints[i].position.x
            tempPoint.y = wp.waypoints[i].position.y
            tempPoint.z = wp.waypoints[i].position.z
            tempMarker.points.append(copy.deepcopy(tempPoint))
            tempMarker.colors.append(copy.deepcopy(tempColor))


        self.markerPub.publish(tempMarker)
        self.waypointArrayService(wp)

        return None



if __name__=="__main__":
    rospy.init_node('HelicalTrajectory')
    a = HelicalTrajectory()
    a.run()