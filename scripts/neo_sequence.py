#!/usr/bin/env python
import sys, os
import rospy
import copy
import math
import time

from geometry_msgs.msg import Pose, Point
from mav_path_trajectory.msg import WaypointArray
from mav_path_trajectory.srv import WaypointArraySrv
from trajectory_msgs.msg import MultiDOFJointTrajectory

class SequenceFollowing():

    def __init__(self):
        self.waypointArray1 = WaypointArray()
        self.waypointArray2 = WaypointArray()
        self.startPose = Pose()
        self.startPose.orientation.w = 1.0
        self.startPose.position = copy.deepcopy(Point(0,0,1))
        self.endPose = Pose()
        self.endPose.orientation.w = 1.0
        self.endPose.position = copy.deepcopy(Point(1,1,1))
        # One way
        self.waypointArray1.waypoints.append(self.startPose)
        self.waypointArray1.waypoints.append(self.endPose)
        # Other way
        self.waypointArray2.waypoints.append(self.endPose)
        self.waypointArray2.waypoints.append(self.startPose)

        # wait for waypoint array service
        # Services for requesting trajectory interpolation
        rospy.wait_for_service("positionWaypointArray", timeout=30)
        self.waypointArrayService = rospy.ServiceProxy("positionWaypointArray", 
            WaypointArraySrv)
        #self.waypointArray.waypoints.append(copy.deepcopy(self.tempPose))
        #self.tempPose.position = Point(0,0,1)

        # We have to subscribe to current_reference in order to keep track
        # with current position. The sequences will have to track that position
        # to be able to switch to next state
        rospy.Subscriber("/euroc3/command/current_reference", 
            MultiDOFJointTrajectory, self.CurrentReferenceCallback)
        self.currentPosition = Point(0,0,0)
        self.currentlyExecutingFlag = "diff"


    def run(self):
        sleepTime = 3
        r = rospy.Rate(30)
        eps = 0.001
        while not rospy.is_shutdown():
            diff1 = math.sqrt(pow(self.startPose.position.x - self.currentPosition.x,2) 
                + pow(self.startPose.position.y - self.currentPosition.y,2) 
                + pow(self.startPose.position.z - self.currentPosition.z,2))
            diff2 = math.sqrt(pow(self.endPose.position.x - self.currentPosition.x,2) 
                + pow(self.endPose.position.y - self.currentPosition.y,2) 
                + pow(self.endPose.position.z - self.currentPosition.z,2))

            #print diff1, diff2

            if (abs(diff1) < eps and self.currentlyExecutingFlag != "diff1"):
                print "diff1"
                self.currentlyExecutingFlag = "diff1"
                # We have to request for waypoint1 to go, start to end. One of
                # These two will take effect as soon as reference reaches start
                # or end point
                self.waypointArrayService(self.waypointArray1)
                time.sleep(sleepTime)
                
            elif (abs(diff2) < eps and self.currentlyExecutingFlag != "diff2"):
                print "diff2"
                self.currentlyExecutingFlag = "diff2"
                # Request to go from end to start
                self.waypointArrayService(self.waypointArray2)
                time.sleep(sleepTime)
            r.sleep()

    def CurrentReferenceCallback(self, data):
        #print data.points[0].transforms[0]
        self.currentPosition.x = data.points[0].transforms[0].translation.x
        self.currentPosition.y = data.points[0].transforms[0].translation.y
        self.currentPosition.z = data.points[0].transforms[0].translation.z


if __name__=="__main__":
    rospy.init_node("pose_publisher")
    klasa = SequenceFollowing()
    klasa.run()