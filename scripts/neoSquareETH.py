#!/usr/bin/env python
import sys, os
import rospy
import copy
import math
import time
import rospkg

from geometry_msgs.msg import Point

class CreateSquareTrajectoryFile():

	def __init__(self):
		xOffset = -0.18
		yOffset = -1.12
		zOffset = 0.0
		dt = 1
		t = 0

		tempArray = []
		tempArray.append(Point(0.0, 0.0, 1.0))
		tempArray.append(Point(0.5, 0.0, 1.0))
		tempArray.append(Point(1.0, 0.0, 1.0))
		tempArray.append(Point(1.0, 0.5, 1.0))
		tempArray.append(Point(1.0, 1.0, 1.0))
		tempArray.append(Point(0.5, 1.0, 1.0))
		tempArray.append(Point(0.0, 1.0, 1.0))
		tempArray.append(Point(0.0, 0.5, 1.0))
		tempArray.append(Point(0.0, 0.0, 1.0))

		rospack = rospkg.RosPack()
		filename = rospack.get_path('mav_path_trajectory') + \
			'/resource/squareTrajectoryFile.txt'

		open(filename, 'w').close()
		f = open(filename, 'w')
		#tempStr = format(tempArray[0].x, '.2f')
		#f.write(tempStr)

		for i in range(len(tempArray)):
			print i
			tempArray[i].x = tempArray[i].x + xOffset
			tempArray[i].y = tempArray[i].y + yOffset
			tempArray[i].z = tempArray[i].z + zOffset
			tempStr = str(format(t, '3d')) + ' ' + \
			str(format(tempArray[i].x, '.2f')) + ' ' + \
			str(format(tempArray[i].y, '.2f')) + ' ' + \
			str(format(tempArray[i].z, '.2f')) + ' 0.00\n'
			f.write(tempStr)
			t = t+dt

		f.close()

if __name__ == "__main__":
	rospy.init_node("square_trajectory_to_txt")
	c = CreateSquareTrajectoryFile()