#!/usr/bin/env python
import sys, os, copy
import math, time
from threading import Thread

from PyQt4 import QtCore, QtGui, uic
import rospy, rospkg
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped
from nav_msgs.msg import Path

global recordingFlag
recordingFlag = "Stop"

class RecordPathGUI():
	# This is a GUI class

	def __init__(self):
		# First get path and create window object
		app = QtGui.QApplication(sys.argv)
		rospack = rospkg.RosPack()
		path = rospack.get_path('mav_path_trajectory')
		self.window = uic.loadUi(path + "/resource/RecordPath.ui")
		self.window.show()

		# Set up button callbacks
		self.window.pushButtonStartRecording.clicked.connect(
			self.pushButtonStartRecordingCallback)
		self.window.pushButtonStopRecording.clicked.connect(
			self.pushButtonStopRecordingCallback)
		self.window.pushButtonClearData.clicked.connect(
			self.pushButtonClearDataCallback)
		sys.exit(app.exec_())

		
	def pushButtonStartRecordingCallback(self):
		global recordingFlag
		recordingFlag = "Start"

	def pushButtonStopRecordingCallback(self):
		global recordingFlag
		recordingFlag = "Stop"

	def pushButtonClearDataCallback(self):
		global recordingFlag
		recordingFlag = "Clear"


class RecordPathROS():
	# This is ROS class that checks GUI state and acts accordingly

	def __init__(self):
		# Initialize GUI class
		self.path = Path()
		tempPoseStamped = PoseStamped()
		self.path.poses.append(tempPoseStamped)
		self.currentPose = PoseStamped()
		self.oldPose = PoseStamped()
		self.positionTolerance = 0.03
		self.poseSub = rospy.Subscriber("vrpn_client/raw_transform", TransformStamped, 
			self.poseCallback, queue_size=1)
		self.pathPub = rospy.Publisher("recorded_path", Path, queue_size=1)
		self.run()
		
		
	def run(self):
		r = rospy.Rate(30)

		while not rospy.is_shutdown():
			dx = self.currentPose.pose.position.x - self.oldPose.pose.position.x
			dy = self.currentPose.pose.position.y - self.oldPose.pose.position.y
			dz = self.currentPose.pose.position.z - self.oldPose.pose.position.z
			dist = math.sqrt(dx**2 + dy**2 + dz**2)

			if dist>self.positionTolerance:
				self.oldPose = copy.deepcopy(self.currentPose)

				global recordingFlag
				if recordingFlag == "Start":
					self.path.poses.append(copy.deepcopy(self.oldPose))

			if recordingFlag == "Clear":
				self.path = []
				self.path = copy.deepcopy(Path())
				self.path.poses.append(copy.deepcopy(self.oldPose))
				recordingFlag = "Stop"


			self.path.header.stamp = rospy.Time()
			self.path.header.frame_id = "world"
			self.pathPub.publish(self.path)
			r.sleep()

	def poseCallback(self, msg):
		self.currentPose.pose.position.x = msg.transform.translation.x
		self.currentPose.pose.position.y = msg.transform.translation.y
		self.currentPose.pose.position.z = msg.transform.translation.z
		#print self.currentPose


if __name__ == "__main__":
	rospy.init_node("RecordPathGUI")
	threadROS = Thread(target=RecordPathROS)
	threadROS.start()
	threadGUI = Thread(target=RecordPathGUI)
	threadGUI.start()
	threadROS.join()
	threadGUI.join()