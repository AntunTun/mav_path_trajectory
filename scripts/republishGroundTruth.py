#!/usr/bin/env python
import sys, os
import time
import math
import copy

import rospy
from geometry_msgs.msg import PoseStamped, TransformStamped
from std_msgs.msg import Bool

class RepublishGroundTruth():

	def __init__(self):
		rospy.Subscriber("vrpn_client/raw_transform", TransformStamped, 
			self.groundTruthCallback)
		self.groundTruthPub = rospy.Publisher("/pose", PoseStamped, 
			queue_size=1)
		self.messageCounter = 0
		self.startPublishingFlag = False

		rospy.Subscriber("/set_ground_truth_publishing", Bool, 
			self.setGroundTruthPublishing)

		rospy.spin()

	def groundTruthCallback(self, msg):
		tempPoseStamped = PoseStamped()
		tempPoseStamped.header = msg.header
		tempPoseStamped.header.stamp = rospy.Time.now()# + rospy.Duration(1.3)
		tempPoseStamped.pose.position.x = msg.transform.translation.x
		tempPoseStamped.pose.position.y = msg.transform.translation.y
		tempPoseStamped.pose.position.z = msg.transform.translation.z
		tempPoseStamped.pose.orientation.x = msg.transform.rotation.x
		tempPoseStamped.pose.orientation.y = msg.transform.rotation.y
		tempPoseStamped.pose.orientation.z = msg.transform.rotation.z
		tempPoseStamped.pose.orientation.w = msg.transform.rotation.w

		self.messageCounter = self.messageCounter + 1

		if (self.startPublishingFlag == True and self.messageCounter < 1000):
			self.groundTruthPub.publish(tempPoseStamped)


	def setGroundTruthPublishing(self, msg):
		self.startPublishingFlag = msg.data
		self.messageCounter = 0


if __name__=="__main__":
	rospy.init_node("RepublishGroundTruth")
	c = RepublishGroundTruth()