#!/usr/bin/env python
import sys, os
import rospy
from geometry_msgs.msg import PoseStamped
import time
import math
from trajectory_msgs.msg import MultiDOFJointTrajectory
from std_msgs.msg import Empty

# Matlab plot and numpy
import matplotlib.pyplot as plt
import numpy as np

class plotMultiDOFJointTrajectory():

	def __init__(self):
		# Subscribes to trajectory and stores it in message below
		self.MultiDOFJointTrajectorySub = rospy.Subscriber(
			"command/trajectory", MultiDOFJointTrajectory, 
			self.multiDOFJointTrajectoryCallback)
		self.trajectory = MultiDOFJointTrajectory()
		rospy.Subscriber("clearPlot", Empty, self.clearPlotCallback)

		# Set trajectory sampling frequency
		self.f = rospy.get_param('~frequency', 100)
		self.f = float(self.f)
		self.Ts = 1.0/self.f

		# Create variables to plot
		self.posx = []
		self.posy = []
		self.posz = []
		self.velx = []
		self.vely = []
		self.velz = []
		self.accx = []
		self.accy = []
		self.accz = []
		self.yaw = []
		self.time = []

		self.plotFlag = False

	def multiDOFJointTrajectoryCallback(self, data):
		self.trajectory = data
		print "Points length: ", len(self.trajectory.points)

		f = 0.001
		for i in range(len(self.trajectory.points)):
			self.posx.append(float(f + self.trajectory.points[i].transforms[0].translation.x))
			self.posy.append(float(f + self.trajectory.points[i].transforms[0].translation.y))
			self.posz.append(float(f + self.trajectory.points[i].transforms[0].translation.z))
			self.velx.append(float(f + self.trajectory.points[i].velocities[0].linear.x))
			self.vely.append(float(f + self.trajectory.points[i].velocities[0].linear.y))
			self.velz.append(float(f + self.trajectory.points[i].velocities[0].linear.z))
			self.accx.append(float(f + self.trajectory.points[i].accelerations[0].linear.x))
			self.accy.append(float(f + self.trajectory.points[i].accelerations[0].linear.y))
			self.accz.append(float(f + self.trajectory.points[i].accelerations[0].linear.z))
			q0 = self.trajectory.points[i].transforms[0].rotation.w
			q1 = 0
			q2 = 0
			q3 = self.trajectory.points[i].transforms[0].rotation.z
			self.yaw.append(float(f + 
				math.atan2(2.0*(q0*q3 + q1*q2), 1.0 - 2.0*(q2*q2 + q3*q3)))*180.0/3.14)
			self.time.append(len(self.time)*0.01)

		#timeEnd = len(self.trajectory.points)*self.Ts
		#tempTime = np.arange(0, (timeEnd-self.Ts/2.0), self.Ts)
		#print "Time length: ", len(tempTime)
		#for i in range(len(tempTime)):
		#	if math.isnan(tempTime[i]):
		#		print "NAN"

		#self.time.extend(tempTime)

		print "Plot length: ", len(self.posx)
		print "Plot time length", len(self.time)
		self.plotFlag = True

	def clearPlotCallback(self, data):
		self.posx = []
		self.posy = []
		self.posz = []
		self.velx = []
		self.vely = []
		self.velz = []
		self.accx = []
		self.accy = []
		self.accz = []
		self.yaw = []
		self.time = []

	def run(self):
		r = rospy.Rate(1)

		while not rospy.is_shutdown():
			if self.plotFlag:
				self.plotFlag = False
				# Position
				fig = plt.figure(1)
				plt.close(fig)
				fig = plt.figure(1)
				plt.subplot(311)
				plt.plot(self.time, self.posx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m]')

				plt.subplot(312)
				plt.plot(self.time, self.posy, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m]')

				plt.subplot(313)
				plt.plot(self.time, self.posz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m]')

				# Speed
				fig = plt.figure(2)
				plt.close(fig)
				fig = plt.figure(2)
				plt.subplot(311)
				plt.plot(self.time, self.velx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m/s]')

				plt.subplot(312)
				plt.plot(self.time, self.vely, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m/s]')

				plt.subplot(313)
				plt.plot(self.time, self.velz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m/s]')

				# Acc
				fig = plt.figure(3)
				plt.close(fig)
				fig = plt.figure(3)
				plt.subplot(311)
				plt.plot(self.time, self.accx, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('x[m/s^2]')

				plt.subplot(312)
				plt.plot(self.time, self.accy, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('y[m/s^2]')

				plt.subplot(313)
				plt.plot(self.time, self.accz, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('z[m/s^2]')

				# Yaw
				fig = plt.figure(4)
				plt.close(fig)
				fig = plt.figure(4)
				plt.plot(self.time, self.yaw, 'b')
				plt.grid(True)
				plt.xlabel('time[s]')
				plt.ylabel('yaw[deg]')


				plt.show(block=False)
				print "after show"
				

			r.sleep()

if __name__=="__main__":
	rospy.init_node('plotMultiDOFJointTrajectory')
	a = plotMultiDOFJointTrajectory()
	a.run()