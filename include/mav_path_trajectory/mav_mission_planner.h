#ifndef _MISSION_PLANNER_H
#define _MISSION_PLANNER_H

#include "ros/ros.h"
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Point.h>
#include <std_msgs/ColorRGBA.h>
#include <mav_path_trajectory/AssessPath3D.h>
#include <mav_path_trajectory/IsPointValid.h>
#include <std_srvs/Empty.h>
//#include <mav_msgs/CommandTrajectory.h>
#include <mav_path_trajectory/Waypoint.h>
#include <mav_path_trajectory/WaypointArray.h>
#include <mav_path_trajectory/TrajectorySampled.h>
#include <mav_path_trajectory/WaypointSrv.h>
#include <geometry_msgs/PoseArray.h>
#include <mav_path_trajectory/GetLiftPoints.h>
#include <mav_path_trajectory/WaypointArraySrv.h>
#include <nav_msgs/Path.h>
#include <mav_path_trajectory/GetTakeoffAndLandPoints.h>
#include <mav_path_trajectory/GetPath.h>

#include <mav_path_trajectory/trajectoryPlanning2.h>

#include <ompl/base/spaces/SE3StateSpace.h>
#include <ompl/base/spaces/RealVectorBounds.h>
#include <ompl/base/State.h>
#include <ompl/base/ScopedState.h>
#include <ompl/base/SpaceInformation.h>
#include <ompl/base/PlannerStatus.h>
#include <ompl/geometric/SimpleSetup.h>
#include <ompl/geometric/planners/rrt/RRTstar.h>
#include <ompl/geometric/PathGeometric.h>
#include <ompl/base/OptimizationObjective.h>

#include <iostream>
#include <vector>
#include <math.h>
#include <ros/package.h>

#include <octomap/octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap/OcTree.h>
#include <octomap_msgs/Octomap.h>

#include <tf/transform_broadcaster.h>

#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <sstream>

// Neo hexacopter adaptation
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <trajectory_msgs/MultiDOFJointTrajectoryPoint.h>
#include <geometry_msgs/Transform.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>


// Testing to find closest points on the map for UAV takeoff
#include <mav_path_trajectory/SendPose.h>

//Joystick control
#include <sensor_msgs/Joy.h>

#include <nav_msgs/Odometry.h>

using namespace std;
using namespace octomap;

namespace ob = ompl::base;
namespace og = ompl::geometric;

typedef unsigned char byte;


class MissionPlanner{
    private:
        double plan(geometry_msgs::Pose StartPointPose, 
            geometry_msgs::Pose EndPointPose, bool genTrajectoryFlag, 
            bool useCustomConstraintsAxisZFlag = false, double zLow = 0.0,
            double zHigh = 1.0);
        bool isStateValid(const ob::State *state);
        bool isStateValid(const ob::State *state, int depth);
	bool isStateValid_withRadius(const ob::State *state); //This function should have option to search for radius around vehicle
	bool isStateValid_withRadius(const ob::State *state, int depth, int radius); //Overloaded funciton for radius search
	
        bool assess_path(mav_path_trajectory::AssessPath3D::Request &req, mav_path_trajectory::AssessPath3D::Response &res);
        bool WaypointPositionCallback(mav_path_trajectory::WaypointSrv::Request &req, mav_path_trajectory::WaypointSrv::Response &res);
        bool WaypointOrientationCallback(mav_path_trajectory::WaypointSrv::Request &req, mav_path_trajectory::WaypointSrv::Response &res);
        bool collisionCheck(mav_path_trajectory::TrajectorySampled generatedTrajectory);
        bool taylor(mav_path_trajectory::TrajectorySampled generatedTrajectory);
        bool get_lift_points(mav_path_trajectory::GetLiftPoints::Request &req, mav_path_trajectory::GetLiftPoints::Response &res);
        bool PlanTakeoffAndLandPoints(mav_path_trajectory::GetTakeoffAndLandPoints::Request &req, 
            mav_path_trajectory::GetTakeoffAndLandPoints::Response &res);
        bool GetTakeoffAndLandPoints(mav_path_trajectory::GetTakeoffAndLandPoints::Request &req, 
            mav_path_trajectory::GetTakeoffAndLandPoints::Response &res);
        bool GetPath(mav_path_trajectory::GetPath::Request &req, mav_path_trajectory::GetPath::Response &res);
        void octomapCallback(const octomap_msgs::Octomap::ConstPtr& msg);

        void mapReceivedCallback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
        bool is2DPointValid(geometry_msgs::PoseStamped &pose, double ugvOffsetX, 
            double ugvOffsetY);
	//Overloaded function depending if one uses simulation or experiment
	void poseCallback(const nav_msgs::Odometry::ConstPtr& msg);
	void poseCallback(const geometry_msgs::PoseWithCovarianceStamped::ConstPtr& msg);
	
	//Point check service depricated (used for testing purposes only
	bool pointCheckCallback(mav_path_trajectory::IsPointValid::Request &req, mav_path_trajectory::IsPointValid::Response &res);
	
	//Save octomap service
	bool emptyCallback(std_srvs::Empty::Request &req, std_srvs::Empty::Response &res);
	 
	//Joystick callbacks
	void joyCallback(const sensor_msgs::JoyConstPtr& msg);
	bool isPoint3dValid(point3d query); //This funciton is used to directly check Octomap occupancy
	void CalculateNextJoyWaypoint();
	

        octomap::OcTree* _octomap; //This variable holds octomap information and updates on octomapCallback
        int version;
        int depth, height, width;
        int size;
	int octomap_depth;
        byte *voxels;
        float tx, ty, tz;
        float scale;
        double  rrtMapMaxX, rrtMapMinX, rrtMapMaxY, rrtMapMinY, rrtMapMaxZ, 
                rrtMapMinZ;

        bool mapReadFlag;
        int rate, trajectoryRate;
    
        int read_binvox(string filespec);// __attribute__ ((deprecated));
        //Function should be replaced when switching to Octomap
        int get_Voxel_index(int x, int y, int z);// __attribute__ ((deprecated));
        //Function should be replaced when switching to Octomap
        
        double wp_x_ref, wp_y_ref, wp_z_ref, minSegmentLength, maxTrajectoryOffset, planningTimeOut, stepLength;
        double maxQuadrotorSpeed, maxQuadrotorAcceleration, distanceFromObstacle;
	//Joystick related variables
	bool joystickEnabled;

	double current_pose_x,current_pose_y, current_pose_z, current_imu_roll, 
        current_imu_pitch, current_imu_yaw, joy_velocityREF,joy_headingREF,joy_slideREF, 
        joy_headingREFScale, joy_velocityREFScale;

        ob::StateSpacePtr space_;
        og::PathGeometric *path;

        ros::NodeHandle n, nhPrivate;

        ros::ServiceServer assessPathService, waypointPositionService,
	pointCheckService,emptyService,
	waypointOrientationService, getLiftPointsService, positionCheckService, 
	getTakeoffAndLandPointsService, getPathService, planTakeoffAndLandPointsService;
	geometry_msgs::Twist horizontClearance(double R0,double Rmax,double fimin,double fimax,double vmax);
	double hclear_vmax,hclear_vmin, hclear_rmax, hclear_rmin, hclear_fimax, hclear_fimin, hclear_gain;
	string uav_type;

        ros::Publisher trajectoryPub, pointPub;
        ros::Publisher pathPub;

        ros::Subscriber octomapSub, joySub, poseSub, sub2DMap;
	
        trajectoryPlanning trajectoryPlanner;

        geometry_msgs::Vector3 checkStart, checkEnd;

        mav_path_trajectory::WaypointArray waypointArray;

        std::vector<geometry_msgs::Pose>   takeoffPoints, landPoints;
        std::vector<float> obstacleHeight;
        geometry_msgs::Pose grabPackageTakeoffPoint;
        double ugv2DOffset, ugvTakeoffLandHeight;

        double* map2D;

        int map2DWidth;
        int map2DHeight;

        double map2DoriginX;
        double map2DoriginY;

        std::string map2Dframeid;

        float map2DResolution;

        std::size_t pt_num;
        geometry_msgs::PoseArray posearray; //za vizualizaciju u rviz-u
        geometry_msgs::Pose takeoffAndLAndPointGoal;

        // NEO trajectory adaptation: new publisher
        ros::Publisher neoTrajectoryPub;
        // Service for trajectory request, without need to plan
        ros::ServiceServer waypointOrientationServiceDirectTrajectory;
        ros::ServiceServer sendCircleRadius;
        bool WaypointArrayPositionCallback(mav_path_trajectory::WaypointArraySrv::Request &req, 
            mav_path_trajectory::WaypointArraySrv::Response &res);
        bool SendCircleRadiusCallback(mav_path_trajectory::WaypointSrv::Request &req, 
            mav_path_trajectory::WaypointSrv::Response &res);
        ros::Publisher markerArrayPub, markerPub, textPub;

        void GetPointsMethod1(geometry_msgs::Pose point1, 
            geometry_msgs::Pose point2);

        //void VisualizePointsInRVIZ(geometry_msgs::Pose point1, 
        //    geometry_msgs::Pose point2, );
    

    public:
        MissionPlanner(void);
        MissionPlanner(string filespec)__attribute__ ((deprecated));
        //Function should be replaced when switching to Octomap
        void run(void);
};

#endif