/******************************************************************************
File name: trajectoryPlanning.h
Description: Planning trajectory for arducopter quadcopter with MoveIt! 
Author: Marko Car, Antun Ivanovic
******************************************************************************/

#include <sstream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <time.h>
#include <eigen3/Eigen/Eigen>

// Ros includes
#include "ros/ros.h"
#include <ros/package.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float64.h>

#include <mav_path_trajectory/TrajectorySampled.h>
#include <mav_path_trajectory/WaypointArray.h>

using namespace std;

typedef struct DerivativeConditions
{
    double speed;
    double acceleration;
    double jerk;
    double split;
} DerivativeConditions;

typedef struct DerivativeConditionsStartEnd
{
    DerivativeConditions start;
    DerivativeConditions end;
} DerivativeConditionsStartEnd;

typedef struct TrajectoryDerivativeConditions
{
    DerivativeConditionsStartEnd x;
    DerivativeConditionsStartEnd y;
    DerivativeConditionsStartEnd z;
    DerivativeConditionsStartEnd yaw;
} TrajectoryDerivativeConditions;

// This message describes one segment of the trajectory. It has segment number
// as an ID, bool isFixedFlag which tells if that segment needs further
// optimization or not and trajectorySampled which contains sampled trajectory
// data.
typedef struct TrajectorySegment
{
    mav_path_trajectory::TrajectorySampled trajectorySampled;
    int segmentNumber;
    bool isFixedFlag;
} TrajectorySegment;

class trajectoryPlanning
{
public:
    trajectoryPlanning();
    ~trajectoryPlanning();
    void run();
    mav_path_trajectory::TrajectorySampled plan(mav_path_trajectory::WaypointArray wp);
    void setTrajectorySamplingFrequency(int freq);
    void setArducopterMaxSpeed(double speed);
    void setArducopterMaxAcc(double acc);


private:    
    // Node handles for topics and params
    ros::NodeHandle nhParams;
    ros::NodeHandle nhTopics;

    // Publishers and subscribers
    ros::Publisher trajectorySampledPub;
    ros::Subscriber waypointSub;

    // Quadcopter params
    double maxSpeed, maxAcc, maxYawSpeed, maxYawAcc;
    int trajectorySamplingFrequency;

    // Trajectory params
    double trajectoryAccScalerZ, trajectorySpeedScalerZ;

    // Functions for calculating spline coefficients for polynomial orders
    // 3, 4, 5, 6. There are two functions for polynomial coefficients with
    // even order of polynomial because a condition can be set either at the 
    // beginning or at the end of the polynomial. 
    void getSplineCoefficientsOrder3(double X0, double Xf, double V0, 
        double Vf, double Tf, std::vector<double> &coefficients);
    void getSplineCoefficientsOrder4AccStart(double X0, double Xf, 
        double V0, double Vf, double A0, double Tf, 
        std::vector<double> &coefficients);
    void getSplineCoefficientsOrder4AccEnd(double X0, double Xf, 
        double V0, double Vf, double Af, double Tf, 
        std::vector<double> &coefficients);
    void getSplineCoefficientsOrder5(double X0, double Xf, double V0, 
        double Vf, double A0, double Af, double Tf, 
        std::vector<double> &coefficients);
    void getSplineCoefficientsOrder6JerkStart(
        double X0, double Xf, double V0, double Vf, double A0, double Af,
        double J0, double Tf, std::vector<double> &coefficients);
    void getSplineCoefficientsOrder6JerkEnd(
        double X0, double Xf, double V0, double Vf, double A0, double Af, 
        double Jf, double Tf, std::vector<double> &coefficients);
    void getSplineCoefficientsOrder7JerkSplitStart(
        double X0, double Xf, double V0, double Vf, double A0, double Af,
        double J0, double S0, double Tf, std::vector<double> &coefficients);
    void getSplineCoefficientsOrder7JerkSplitEnd(
        double X0, double Xf, double V0, double Vf, double A0, double Af,
        double Jf, double Sf, double Tf, std::vector<double> &coefficients);


    // Returns polynomial value for 3rd, 4th, 5th and 6th order polynomials
    // Older functions returning values for specific order polynomial
    void calculatePolynomialValueOrder3(std::vector<double> B, double t, 
        double &position, double &speed, double &acceleration);
    void calculatePolynomialValueOrder4(std::vector<double> B, double t, 
        double &position, double &speed, double &acceleration);
    void calculatePolynomialValueOrder5(std::vector<double> B, double t, 
        double &position, double &speed, double &acceleration, double &jerk, 
        double &split);
    void calculatePolynomialValueOrder6(std::vector<double> B, double t, 
        double &position, double &speed, double &acceleration, double &jerk, 
        double &split);
    void calculatePolynomialValueOrder7(std::vector<double> B, double t, 
        double &position, double &speed, double &acceleration, double &jerk, 
        double &split);

    
    // Finding max speed in trajectory as well as acceleration functions
    double getMaxTrajectorySpeed(mav_path_trajectory::TrajectorySampled trajectory, 
        int &index);
    double getMaxTrajectoryAcceleration(
        mav_path_trajectory::TrajectorySampled trajectory, int &index);
    double calculateTheoreticalMaxSpeed(double V0, double trajectoryLength, 
        double maxAcc);
    double getMaxTrajectoryYawSpeed(mav_path_trajectory::TrajectorySampled trajectory, 
        int &index);
    double getMaxTrajectoryYawAcc(mav_path_trajectory::TrajectorySampled trajectory, 
        int &index);

    // Trajectory sampling functions
    // More convinient multiple waypoint sampling function, will need yaw
    void sampleMultipleWaypointTrajectory(
        std::vector< std::vector<double> > coefficientsX, 
        std::vector< std::vector<double> > coefficientsY, 
        std::vector< std::vector<double> > coefficientsZ, 
        std::vector< std::vector<double> > coefficientsYAW, std::vector<double> t,
        mav_path_trajectory::TrajectorySampled &trajectory, int sampleFrequency, 
        std::vector<int> &endIndexes);
    // Function to empty sampled trajectory
    void clearTrajectorySampled(mav_path_trajectory::TrajectorySampled &trajectory);
    // Utility function to add zero speed and acceleration at the end of trajectory
    // to ensure full stop
    void addZeroSpeedAndAccAtTrajectoryEnd(
        mav_path_trajectory::TrajectorySampled &trajectorySampled);


    // Waypoint callback and TWO waypoints trajectory functions used for testing
    // and generating trajectory through two waypoints only because matrix-based
    // methods don't work with less than three waypoints
    void waypointCallback(const mav_path_trajectory::WaypointArray &msg);
    mav_path_trajectory::TrajectorySampled twoWaypointsTrajectoryOrder5(
        mav_path_trajectory::WaypointArray waypointArray);


    


    // Getting max speed and acc on a segment of trajectory rather tgan on
    // full trajectory
    double getMaxTrajectorySpeedInterval(
        mav_path_trajectory::TrajectorySampled trajectory, int start, int end);
    double getMaxTrajectoryAccelerationInterval(
        mav_path_trajectory::TrajectorySampled trajectory, int start, int end);

    // Ho cook 56 related functions
    mav_path_trajectory::TrajectorySampled hoCook56(
        mav_path_trajectory::WaypointArray waypointArray, 
        TrajectoryDerivativeConditions initialConditions, 
        int &scalingSegmentID);
    void createMatrixMForHoCook56(Eigen::MatrixXd &M, std::vector<double> t, 
        int m);
    void createMatrixAForHoCook56(Eigen::MatrixXd &A, std::vector<double> x, 
        std::vector<double> t, int m, DerivativeConditionsStartEnd conditions);
    void calculateCoefficientsHoCook56(
        std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
        std::vector<double> x, std::vector<double> t, 
        DerivativeConditionsStartEnd conditions);

    // Ho cook 57 related functions
    mav_path_trajectory::TrajectorySampled hoCook57(
        mav_path_trajectory::WaypointArray waypointArray, 
        TrajectoryDerivativeConditions initialConditions, 
        int &scalingSegmentID);
    void createMatrixMForHoCook57(Eigen::MatrixXd &M, std::vector<double> t, 
        int m);
    void calculateCoefficientsHoCook57(
        std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
        std::vector<double> x, std::vector<double> t, 
        DerivativeConditionsStartEnd conditions);
    void createMatrixAForHoCook57(Eigen::MatrixXd &A, std::vector<double> x, 
        std::vector<double> t, int m, DerivativeConditionsStartEnd conditions);


    mav_path_trajectory::TrajectorySampled optimizedPlan(
        mav_path_trajectory::WaypointArray wp);
    void getFirstAndLastAvaliableSegment(
        std::vector<TrajectorySegment> trajectorySegments, int &firstSegment, 
        int &lastSegment);
    void setTrajectoryDerivativeConditions(std::vector<TrajectorySegment> 
        trajectorySegments, int firstSegment, int lastSegment, 
        TrajectoryDerivativeConditions &conditions);
    void fixateAndSampleTrajectorySegments(
    std::vector<TrajectorySegment> &trajectorySegments, 
    mav_path_trajectory::TrajectorySampled trajectory, int firstSegment, int lastSegment,
    int scalingSegmentID);
};

void printConditions(TrajectoryDerivativeConditions c);