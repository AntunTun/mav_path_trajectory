/******************************************************************************
File name: trajectoryPlanning.h
Description: Planning trajectory for arducopter quadcopter with MoveIt! 
Author: Marko Car, Antun Ivanovic
******************************************************************************/

#include <sstream>
#include <fstream>
#include <vector>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <iostream>
#include <stdio.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>
#include <time.h>
#include <eigen3/Eigen/Eigen>

// Ros includes
#include "ros/ros.h"
#include <ros/package.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float64.h>

#include <mav_path_trajectory/TrajectorySampled.h>
#include <mav_path_trajectory/WaypointArray.h>

using namespace std;

class trajectoryPlanning
{
public:
	trajectoryPlanning();
	~trajectoryPlanning();
	void run();
	mav_path_trajectory::TrajectorySampled plan(mav_path_trajectory::WaypointArray wp);
	void setTrajectorySamplingFrequency(int freq);
	void setArducopterMaxSpeed(double speed);
	void setArducopterMaxAcc(double acc);


private: 	
 	// Node handles for topics and params
 	ros::NodeHandle nhParams;
 	ros::NodeHandle nhTopics;

 	// Publishers and subscribers
 	ros::Publisher trajectorySampledPub;
 	ros::Subscriber waypointSub;

 	// Quadcopter params
 	double maxSpeed, maxAcc, maxYawSpeed, maxYawAcc;
 	int trajectorySamplingFrequency;

 	// Spline coefficients and polynomial values functions, first one is for
 	// order 5 polynomials
 	void getSplineCoefficientsThroughTwoPoints(double X0, double Xf, double V0, 
 		double Vf, double A0, double Af, double Tf, 
 		std::vector<double> &coefficients);
 	void getSplineCoefficientsOrder3(double X0, double Xf, double V0, 
 		double Vf, double Tf, std::vector<double> &coefficients);
 	void getSplineCoefficientsOrder4AccStart(double X0, double Xf, 
 		double V0, double Vf, double A0, double Tf, 
 		std::vector<double> &coefficients);
 	void getSplineCoefficientsOrder4AccEnd(double X0, double Xf, 
 		double V0, double Vf, double Af, double Tf, 
 		std::vector<double> &coefficients);
 	void getSplineCoefficientsOrder6JerkStart(
		double X0, double Xf, double V0, double Vf, double A0, double Af,
		double J0, double Tf, std::vector<double> &coefficients);
 	void getSplineCoefficientsOrder6JerkEnd(
		double X0, double Xf, double V0, double Vf, double A0, double Af, 
		double Jf, double Tf, std::vector<double> &coefficients);
 	// Returns polynomial value for 3rd, 4th, 5th and 6th order polynomials
 	double calculatePositionPolynomialValue(std::vector<double> a, double t);
 	double calculateSpeedPolynomialValue(std::vector<double> a, double t);
 	double calculateAccelerationPolynomialValue(std::vector<double> a, double t);
 	// Older functions returning values for specific order polynomial
 	void calculatePolynomialValueOrder5(std::vector<double> B, double t, 
 		double &position, double &speed, double &acceleration);
 	void calculatePolynomialValueOrder4(std::vector<double> B, double t, 
 		double &position, double &speed, double &acceleration);
 	void calculatePolynomialValueOrder3(std::vector<double> B, double t, 
 		double &position, double &speed, double &acceleration);
 	void calculatePolynomialValueOrder6(std::vector<double> B, double t, 
 		double &position, double &speed, double &acceleration);

 	// Waypoint callback and TWO waypoints trajectory functions used for testing
 	// and generating trajectory through two waypoints only because matrix-based
 	// methods don't work with less than three waypoints
 	void waypointCallback(const mav_path_trajectory::WaypointArray &msg);
 	void twoWaypointsTrajectoryOrder3(mav_path_trajectory::WaypointArray waypointArray);
 	mav_path_trajectory::TrajectorySampled twoWaypointsTrajectoryOrder5(mav_path_trajectory::WaypointArray waypointArray);
 	//void twoWaypointsTrajectoryOrder4AccStart(
 	//	mav_path_trajectory::WaypointArray waypointArray);
 	//void twoWaypointsTrajectoryOrder4AccEnd(
 	//	mav_path_trajectory::WaypointArray waypointArray);


 	// Trajectory manipulation functions
 	// Older sampling function for two waypoint trajectory
 	void sampleTrajectoryBetweenTwoPoints(std::vector<double> coefficientsX, 
 		std::vector<double> coefficientsY, std::vector<double> coefficientsZ,
 		std::vector<double> coefficientsYaw, 
 		mav_path_trajectory::TrajectorySampled &trajectory, double trajectoryDuration, 
		int sampleFrequency);
 	// More convinient multiple waypoint sampling function, will need yaw
 	void sampleMultipleWaypointTrajectory(
	 	std::vector< std::vector<double> > coefficientsX, 
		std::vector< std::vector<double> > coefficientsY, 
		std::vector< std::vector<double> > coefficientsZ, 
		std::vector< std::vector<double> > coefficientsYAW, std::vector<double> t,
		mav_path_trajectory::TrajectorySampled &trajectory, int sampleFrequency, 
		std::vector<int> &endIndexes);
 	// Function to empty sampled trajectory
 	void clearTrajectorySampled(mav_path_trajectory::TrajectorySampled &trajectory);

 	// Finding max speed in trajectory as well as acceleration functions
 	double getMaxTrajectorySpeed(mav_path_trajectory::TrajectorySampled trajectory, 
 		int &index);
 	double getMaxTrajectoryAcceleration(
 		mav_path_trajectory::TrajectorySampled trajectory, int &index);
 	double calculateTheoreticalMaxSpeed(double V0, double trajectoryLength, 
 		double maxAcc);
 	double getMaxTrajectoryYawSpeed(mav_path_trajectory::TrajectorySampled trajectory, 
 		int &index);
 	double getMaxTrajectoryYawAcc(mav_path_trajectory::TrajectorySampled trajectory, 
 		int &index);


 	
 	// Ho-Cook related functions, 3rd and 4th order polynomials
 	//void hoCook34(mav_path_trajectory::WaypointArray waypointArray);
 	//void calculateCoefficientsHoCook34(
	//	std::vector< std::vector<double> > &coefficientsX, 
	//	std::vector< std::vector<double> > &coefficientsY, 
	//	std::vector< std::vector<double> > &coefficientsZ, Eigen::MatrixXd xd, 
	//	Eigen::MatrixXd yd, Eigen::MatrixXd zd, 
	//	mav_path_trajectory::WaypointArray waypointArray, std::vector<double> t);

 	
 	// Getting max speed and acc on a segment of trajectory rather tgan on
 	// full trajectory
 	double getMaxTrajectorySpeedInterval(
		mav_path_trajectory::TrajectorySampled trajectory, int start, int end);
 	double getMaxTrajectoryAccelerationInterval(
		mav_path_trajectory::TrajectorySampled trajectory, int start, int end);

 	// Ho-Cook 45 functions
 	//void hoCook45(mav_path_trajectory::WaypointArray waypointArray);
 	//void createMatrixMForHoCook45(Eigen::MatrixXd &M, std::vector<double> t,
 	//	int m);
 	//void createMatrixAForHoCook45(Eigen::MatrixXd &A, std::vector<double> x, 
 	//	std::vector<double> t, int m);
 	//void calculateCoefficientsHoCook45(
 	//	std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
	//	std::vector<double> x, std::vector<double> t);


 	// Ho cook 56
 	mav_path_trajectory::TrajectorySampled hoCook56(mav_path_trajectory::WaypointArray waypointArray);
 	void createMatrixMForHoCook56(Eigen::MatrixXd &M, std::vector<double> t, 
 		int m);
 	void createMatrixAForHoCook56(Eigen::MatrixXd &A, std::vector<double> x, 
 		std::vector<double> t, int m);
 	void calculateCoefficientsHoCook56(
 		std::vector< std::vector<double> > &coefficients, Eigen::MatrixXd D, 
		std::vector<double> x, std::vector<double> t);

 	
 	// Utility function to add zero speed and acceleration at the end of trajectory
 	// to ensure full stop
 	void addZeroSpeedAndAccAtTrajectoryEnd(
 		mav_path_trajectory::TrajectorySampled &trajectorySampled);
};