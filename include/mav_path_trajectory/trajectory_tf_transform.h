#ifndef _TRAJECTORY_TRANSFORMER_H
#define _TRAJECTORY_TRANSFORMER_H

#include "ros/ros.h"
#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <trajectory_msgs/MultiDOFJointTrajectory.h>
#include <trajectory_msgs/MultiDOFJointTrajectoryPoint.h>
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/PoseStamped.h>

using namespace std;


typedef unsigned char byte;


class TrajectoryTransformer{
    private:
        ros::NodeHandle nhPrivate;

        ros::Publisher transformed_trajectory, transformedPosePub;
        ros::Subscriber trajectorySub, transformSub, poseSub;
	void trajectoryCallback(const trajectory_msgs::MultiDOFJointTrajectory::ConstPtr& msg);
	void transformCallback(const geometry_msgs::TransformStamped::ConstPtr& msg);
	void poseCallback(const geometry_msgs::PoseStamped msg);
	bool transform_received;
	tf::TransformListener tf_listener;
	tf::Transform transform;
	tf::Quaternion odom2viconYaw;

    public:
        TrajectoryTransformer(void);
        void run(void);
};

#endif